//******************************************************************************
//
//   Fdir.h       File Directory utilites
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __Fdir_H__
   #define __Fdir_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __FdirDef_H__
   #include "FdirDef.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

// TFdirHandle == FDIR_INVALID (0) when function failed

void FdirInit( void);
// Initialisation

void FdirFormat( void);
// Formatting

TFdirHandle FdirCreate( const char *Name);
// Create directory entry with <Name>

void FdirRename( TFdirHandle Handle, const char *Name);
// Rename with <Name> at <Handle>

void FdirDelete( TFdirHandle Handle);
// Delete directory entry

TFdirHandle FdirSearch( const char *Name);
// Search directory entry for <Name>

void FdirLoad( TFdirHandle Handle, TFdirInfo *Item);
// Get directory <Item> by <Handle>

void FdirSave( TFdirHandle Handle, TFdirInfo *Item);
// Save directory entry <Item> at <Handle>

void FdirGetName( TFdirHandle Handle, char *Name);
// Fills <Name> by <Handle>

int FdirGetSize( TFdirHandle Handle);
// Returns file size by <Handle>

#ifdef __cplusplus
}
#endif

#endif
