//*****************************************************************************
//
//    Lm2068e.h      LM2068E / S1D13700 graphic controller services
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#ifndef __Lm2068e_H__
   #define __Lm2068e_H__

#ifndef __Gpu_H__
   #include "Gpu.h"                    // init & flush
#endif

// GpuGoto range :
#define GPU_MAX_ROW       240          // rows count
#define GPU_MAX_COLUMN    40           // column bytes

//-----------------------------------------------------------------------------
// Extended GPU functions
//-----------------------------------------------------------------------------

void GpuOn( void);
// Display On

void GpuOff( void);
// Display Off

void GpuClear( void);
// Clear display

void GpuGoto( int Row, int Column);
// Set coordinate to <Row>,<Column>

void GpuWrite( unsigned Pattern);
// Write <Pattern> to current coordinate (increment <Column>)

void GpuDone( void);
// Write done

#ifndef GPU_DISABLE_BUFFER
void GpuInitBuffer( void);
// Inicializace bufferu
#endif

#endif
