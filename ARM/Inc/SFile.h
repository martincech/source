//*****************************************************************************
//
//    SFile.h       Secure file
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __SFile_H__
   #define __SFile_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif


TYesNo SFileReset( void);
// Prepare file for write

TYesNo SFileAppend( void *Data);
// Append record with <Data>

TYesNo SFileRead( int Index, void *Data);
// Read <Data> from <Index>. Returns false on EOF

#endif
