//*****************************************************************************
//
//    Iic.h  -  I2C controller services
//    Version 1.0 (c) VymOs
//
//*****************************************************************************

#ifndef __Iic_H__
   #define __Iic_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void IicInit( void);
// Interface initialisation

void IicStart( void);
// Sends start sequence

void IicStop( void);
// Sends stop sequence

TYesNo IicSend( byte value);
// Sends <value>, returns YES if ACK

byte IicReceive( TYesNo ack);
// Returns received byte, sends confirmation <ack>.
// <ack> = YES sends ACK, NO sends NOT ACK

TYesNo IicWrite( void *Buffer, int Size);
// Sends <Size> bytes from <Buffer>

void IicRead( void *Buffer, int Size);
// Receives of <Size> bytes into <Buffer>

#endif
