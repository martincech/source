//*****************************************************************************
//
//    Histogram.h   -  Histogram calculations
//    Version 1.0, (c) VymOs
//
//*****************************************************************************

#ifndef __Histogram_H__
   #define __Histogram_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

typedef struct {
   THistogramValue Average;                 // Average value (true histogram center)
   THistogramValue Center;                  // Histogram center (updated for display)
   THistogramValue Step;                    // Histogram step
   THistogramCount Slot[ HISTOGRAM_SLOTS];
} THistogram;

void HistogramClear( THistogram *Histogram);
// Clear data

void HistogramAdd( THistogram *Histogram, THistogramValue Value);
// Add <Value>

int HistogramGetSlot( THistogram *Histogram, THistogramValue Value);
// Find slot index by <Value>

void HistogramSetCenter( THistogram *Histogram, THistogramValue Center);
// Set histogram <Center>

void HistogramSetRange( THistogram *Histogram, int Range);
// Set histogram step by <Range> [%]

void HistogramSetStep( THistogram *Histogram, THistogramValue Step);
// Set histogram <Step>

TYesNo HistogramEmpty( THistogram *Histogram);
// Returns YES if empty

THistogramValue HistogramMaximum( THistogram *Histogram);
// Get maximal slot value

int HistogramNormalize( THistogram *Histogram, THistogramValue MaxValue, int SlotIndex);
// Returns histogram value normalized by <MaxValue>

THistogramValue HistogramGetValue( THistogram *Histogram, int SlotIndex);
// Returns column value by <SlotIndex>

#endif
