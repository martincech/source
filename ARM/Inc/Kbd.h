//*****************************************************************************
//
//    Kbd.h -  Keyboard services
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Kbd_H__
   #define __Kbd_H__

#ifndef __Hardware_H__
   #include "Hardware.h"               // kody klaves
#endif

extern word _kbd_counter;              // citac delky

// Tato funkce se zaradi do interruptu systemoveho casovace :
#define KbdTrigger()   if( _kbd_counter){ \
                          --_kbd_counter; \
                       }                  \

//------------------------------------------------------------------------------
//  Funkce
//------------------------------------------------------------------------------

void KbdInit( void);
// Inicializace

int KbdPowerUpKey( void);
// Vrati klavesu, ktera je drzena po zapnuti nebo K_RELEASED

void KbdPowerUpRelease( void);
// Ceka na pusteni klavesy, drzene po zapnuti

int KbdGet( void);
// Testuje stisknuti klavesy, vraci klavesu nebo
// K_IDLE neni-li stisknuto nic, volat periodicky

void KbdDisable( void);
// Zakaze generovani klaves, ceka na uvolneni

//------------ funkce standardniho vstupu :

TYesNo kbhit( void);
// Vraci YES, bylo-li neco stisknuto

int getch( void);
// Ceka na stisknuti klavesy, vrati ji

#endif
