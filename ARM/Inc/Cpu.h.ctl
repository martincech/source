//*****************************************************************************
//
//    Cpu.h        CPU services
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Cpu_H__
   #define __Cpu_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// interrupt function attribute :
//#define __irq    __attribute__ ( (interrupt("IRQ")))
//#define __fiq    __attribute__ ( (interrupt("FIQ")))

#define __irq
#define __fiq

// interrupt handler signature :
typedef void TIrqHandler( void);

#define CheckTrigger( counter, expression)  \
                     if( counter){          \
                        if( !--(counter)){  \
                            expression;     \
                        }                   \
                     }                      \

//------------------------------------------------------------------------------
//   Functions
//------------------------------------------------------------------------------

void CpuInit( void);
// Cpu startup initialisation

void CpuInitIap( void);
// Cpu IAP initialisation

void CpuIrqAttach( native irq, native channel, TIrqHandler *handler);
// Install IRQ <handler> with <irq> priority on <channel>

void CpuIrqEnable( native channel);
// Enable IRQ on <channel>. Must not be used inside interrupt handler

void CpuIrqDisable( native channel);
// Disable IRQ on <channel>

void CpuIrqDone( void);
// Last operation in the interrupt handler

void EnableInts( void);
// Enable interrupts

void DisableInts( void);
// Disable interrupts

void StartWatchDog( void);
// Start watchdog

void WatchDog( void);
// Refresh Watchdog

#endif
