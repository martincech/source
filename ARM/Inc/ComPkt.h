//*****************************************************************************
//
//   ComPkt.h    COM packet interface
//   Version 1.0 (c) VymOs
//
//*****************************************************************************

#ifndef __ComPkt_H__
   #define __ComPkt_H__

#if   defined( __COM0__)
   #ifndef __Com0Pkt_H__
      #include "Com0Pkt.h"
   #endif

   #define ComRxPacket( cmd, arg)         Com0RxPacket( cmd, arg)
   #define ComTxPacket( cmd, arg)         Com0TxPacket( cmd, arg)
   #define ComTxBlockStart( size)         Com0TxBlockStart( size)
   #define ComTxBlockByte( b)             Com0TxBlockByte( b)
   #define ComTxBlockEnd()                Com0TxBlockEnd()
   #define ComRxData( buffer, size)       Com0RxData( buffer, size)

#elif defined( __COM1__)
   #ifndef __Com1Pkt_H__
      #include "Com1Pkt.h"
   #endif

   #define ComRxPacket( cmd, arg)         Com1RxPacket( cmd, arg)
   #define ComTxPacket( cmd, arg)         Com1TxPacket( cmd, arg)
   #define ComTxBlockStart( size)         Com1TxBlockStart( size)
   #define ComTxBlockByte( b)             Com1TxBlockByte( b)
   #define ComTxBlockEnd()                Com1TxBlockEnd()
   #define ComRxData( buffer, size)       Com1RxData( buffer, size)

#else
   #error "Unknown COM number"
#endif

#endif
