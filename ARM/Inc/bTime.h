//******************************************************************************
//                                                                            
//  bTime.c        Print time to buffer
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __bTime_H__
   #define __bTime_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __DtDef_H__
   #include "../DtDef.h"
#endif

void bTime( char *Buffer, TTimestamp Now);
// Display time only

void bDate( char *Buffer, TTimestamp Now);
// Display date only

void bDateTime( char *Buffer, TTimestamp Now);
// Display date and time

#endif
