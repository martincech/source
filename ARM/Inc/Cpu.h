//*****************************************************************************
//
//    Cpu.h        CPU services
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Cpu_H__
   #define __Cpu_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// interrupt function attribute :
#define __irq
#define __fiq    __attribute__ ( (interrupt("FIQ")))
#define __swi    __attribute__ ( (interrupt("SWI")))
#define __abort  __attribute__ ( (interrupt("ABORT")))
#define __undef  __attribute__ ( (interrupt("UNDEF")))

// interrupt handler signature :
typedef void TIrqHandler( void);

#define CheckTrigger( counter, expression)  \
                     if( counter){          \
                        if( !--(counter)){  \
                            expression;     \
                        }                   \
                     }                      \

#ifdef SLEEP_ENABLE
   #define CpuIdleMode()    SCB_PCON |= PCON_IDLE
   // Enter IDLE mode
#else
   #define CpuIdleMode()
#endif

//------------------------------------------------------------------------------
//   Functions
//------------------------------------------------------------------------------

void CpuInit( void);
// Cpu startup initialisation

void CpuInitIap( void);
// Cpu IAP initialisation

void CpuFiqAttach( int channel);
// Enable FIQ for <channel>

void CpuIrqAttach( int irq, int channel, TIrqHandler *handler);
// Install IRQ <handler> with <irq> priority on <channel>

void CpuIrqEnable( int channel);
// Enable IRQ on <channel>. Must not be used inside interrupt handler

void CpuIrqDisable( int channel);
// Disable IRQ on <channel>

void CpuIrqDone( void);
// Last operation in the interrupt handler

void EnableInts( void);
// Enable interrupts

void DisableInts( void);
// Disable interrupts

void StartWatchDog( void);
// Start watchdog

void WatchDog( void);
// Refresh Watchdog

void WatchDogNaked( void);
// Refresh Watchdog from interrupt

TYesNo WatchDogActive( void);
// Returns YES by watchdog reset

#endif
