//*****************************************************************************
//
//    Lpc.h        Philips LPC registers
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Lpc_H__
   #define __Lpc_H__

//------------------------------------------------------------------------------
//   Status register
//------------------------------------------------------------------------------

#define STATUS_MODE        0x1F
#define STATUS_THUMB       (1 << 5)    // T flag
#define STATUS_FIQ         (1 << 6)    // F flag
#define STATUS_IRQ         (1 << 7)    // I flag

//------------------------------------------------------------------------------
//   Memory map
//------------------------------------------------------------------------------

// MEMMAP :
#define MEMMAP_BOOTLOADER  0           // map vectors to bootloader
#define MEMMAP_FLASH       1           // map vectors to internal flash
#define MEMMAP_RAM         2           // map vectors to internal RAM

//------------------------------------------------------------------------------
//   PLL
//------------------------------------------------------------------------------

// PLLCON :
#define PLLCON_ENABLE      (1 << 0)    // enable PLL
#define PLLCON_CONNECT     (1 << 1)    // connect PLL

// PLLCFG :
#define PLLCFG_SET( msel, psel)   (((msel) & 0x1F) | (((psel) & 0x03) << 5))

// PLLFEED :
#define PLLFEED_1         0xAA         // feed sequence 1st word
#define PLLFEED_2         0x55         // feed sequence 1nd word

// PLLSTAT :
#define PLLSTAT_MSEL      0x001F       // MSEL mask       
#define PLLSTAT_PSEL      0x0060       // PSEL mask
#define PLLSTAT_ENABLE    (1 << 8)     // enable PLL
#define PLLSTAT_CONNECT   (1 << 9)     // connect PLL
#define PLLSTAT_LOCK      (1 << 10)    // PLL locked

//------------------------------------------------------------------------------
//   Power mode
//------------------------------------------------------------------------------

// PCON :
#define PCON_NORMAL        0           // cpu is runnning
#define PCON_IDLE          1           // idle mode - cpu off, peripherals on
#define PCON_POWER_DOWN    2           // power down mode - cpu & peripherals off

// PCONP :
#define PCONP_TIMER0       (1 <<  1)   // Timer 0
#define PCONP_TIMER1       (1 <<  2)   // Timer 1
#define PCONP_UART0        (1 <<  3)   // UART 0
#define PCONP_UART1        (1 <<  4)   // UART 1
#define PCONP_PWM0         (1 <<  5)   // PWM 0
#define PCONP_I2C0         (1 <<  7)   // I2C 0
#define PCONP_SPI0         (1 <<  8)   // SPI 0
#define PCONP_RTC          (1 <<  9)   // RTC
#define PCONP_SPI1         (1 << 10)   // SPI 1
#define PCONP_AD0          (1 << 12)   // ADC 0
#define PCONP_I2C1         (1 << 19)   // I2C 1
#define PCONP_AD1          (1 << 20)   // ADC 1

#define PCONP_I2C          PCONP_I20   // I2C
#define PCONP_SPI          PCONP_SPI0  // SPI

//------------------------------------------------------------------------------
//   VPB clock
//------------------------------------------------------------------------------

#define VPBDIV_QUARTER     0           // VPB clock = 1/4 cpu clock
#define VPBDIV_SAME        1           // VPB clock = cpu clock
#define VPBDIV_HALF        2           // VPB clock = 1/2 cpu clock

//------------------------------------------------------------------------------
//   MAM module
//------------------------------------------------------------------------------

// MAMCR :
#define MAMCR_DISABLED   0             // MAM disabled
#define MAMCR_PREFETCH   1             // sequential prefetch only
#define MAMCR_FULL       2             // prefetch & full cache

//------------------------------------------------------------------------------
//   EINT module
//------------------------------------------------------------------------------

// EXTINT :
#define EXTINT_EINT0     (1 << 0)      // input EINT0 int flag
#define EXTINT_EINT1     (1 << 1)      // input EINT1 int flag
#define EXTINT_EINT2     (1 << 2)      // input EINT2 int flag
#define EXTINT_EINT3     (1 << 3)      // input EINT3 int flag
#define EXTINT_EINT( input)  (1 << (input))

// EXTWAKE :
#define EXTWAKE_EINT0    (1 <<  0)     // input EINT0
#define EXTWAKE_EINT1    (1 <<  1)     // input EINT1
#define EXTWAKE_EINT2    (1 <<  2)     // input EINT2
#define EXTWAKE_EINT3    (1 <<  3)     // input EINT3
#define EXTWAKE_BOD      (1 << 14)     // BOD interrupt
#define EXTWAKE_RTC      (1 << 15)     // RTC interrupt
#define EXTWAKE_EINT( input) (1 << (input))

// EXTMODE :
#define EXTMODE_EINT0    (1 <<  0)     // input EINT0 edge sensitive
#define EXTMODE_EINT1    (1 <<  1)     // input EINT1 edge sensitive
#define EXTMODE_EINT2    (1 <<  2)     // input EINT2 edge sensitive
#define EXTMODE_EINT3    (1 <<  3)     // input EINT3 edge sensitive
#define EXTMODE_EINT( input)  (1 << (input))

// EXTPOLAR :
#define EXTPOLAR_EINT0   (1 << 0)      // input EINT0 high/rising edge active
#define EXTPOLAR_EINT1   (1 << 1)      // input EINT1 high/rising edge active
#define EXTPOLAR_EINT2   (1 << 2)      // input EINT2 high/rising edge active
#define EXTPOLAR_EINT3   (1 << 3)      // input EINT3 high/rising edge active
#define EXTPOLAR_EINT( input)  (1 << (input))

//------------------------------------------------------------------------------
//   VIC module
//------------------------------------------------------------------------------

// IRQ priority :
#define IRQ0       0                   // highest
#define IRQ1       1
#define IRQ2       2
#define IRQ3       3
#define IRQ4       4
#define IRQ5       5
#define IRQ6       6
#define IRQ7       7
#define IRQ8       8
#define IRQ9       9
#define IRQ10      10
#define IRQ11      11
#define IRQ12      12
#define IRQ13      13
#define IRQ14      14
#define IRQ15      15                  // lowest

// IRQ channels :

#define VIC_WDT         0              // Watchdog interrupt
#define VIC_SPARE       1              // reserved for SW interrupts only
#define VIC_DBG_RX      2              // Embedded ICE, DbgCommRx
#define VIX_DBG_TX      3              // Embedded ICE, DbgCommTx 
#define VIC_TIMER0      4              // Timer 0 Match 0-3, Capture 0-3
#define VIC_TIMER1      5              // Timer 1 Match 0-3, Capture 0-3
#define VIC_UART0       6              // UART0 RLS, THRE, RDA, CTI
#define VIC_UART1       7              // UART1 RLS, THRE, RDA, CTI, MSI
#define VIC_PWM0        8              // PWM0 Match 0-6
#define VIC_I2C0        9              // I2C 0 state change SI
#define VIC_SPI0        10             // SPI0 interrupt flag SPIF, MODF
#define VIC_SPI1        11             // SPI1 TXRIS, TXRIS, RTRIS, RORRIS
#define VIC_PLL         12             // PLL lock
#define VIC_RTC         13             // RTC counter RTCCIF / alarm RTCALF
#define VIC_EINT0       14             // External interrupt 0
#define VIC_EINT1       15             // External interrupt 1
#define VIC_EINT2       16             // External interrupt 2
#define VIC_EINT3       17             // External interrupt 3
#define VIC_AD0         18             // A/D COnverter 0
#define VIC_I2C1        19             // I2C 1 state change SI
#define VIC_BOD         20             // Brown out detection
#define VIC_AD1         21             // A/D COnverter 1

#define VIC_I2C         VIC_I2C0
#define VIC_SPI         VIC_SPI0

// VICVecCntl0-15 value :
#define VIC_CHANNEL_ENABLED (1 << 5)   // enable flag
#define VIC_SET_CHANNEL( channel)      ((channel) | VIC_CHANNEL_ENABLED)

// VIC Vectors :
#define VIC_VECTOR  (REG32(&VICVectAddr0))   // use VIC_VECTOR[ IRQx]
#define VIC_CONTROL (REG32(&VICVectCntl0))   // use VIC_CONTROL[ IRQx]

//------------------------------------------------------------------------------
//   Watchdog
//------------------------------------------------------------------------------

// WDMOD :
#define WDMOD_ENABLE     (1 << 0)      // enable watchdog
#define WDMOD_RESET      (1 << 1)      // enable reset (0-interrupt only)
#define WDMOD_FLAG       (1 << 2)      // timeout flag
#define WDMOD_INTERRUPT  (1 << 3)      // interrupt flag

// WDFEED sequence :
#define WDFEED_1    0xAA
#define WDFEED_2    0x55

//------------------------------------------------------------------------------
//   Timer
//------------------------------------------------------------------------------

// T0IR, T1IR :
#define IR_MR0            (1 << 0)     // Match channel 0
#define IR_MR1            (1 << 1)     // Match channel 1
#define IR_MR2            (1 << 2)     // Match channel 2
#define IR_MR3            (1 << 3)     // Match channel 3
#define IR_CR0            (1 << 4)     // Capture channel 0
#define IR_CR1            (1 << 5)     // Capture channel 1
#define IR_CR2            (1 << 6)     // Capture channel 2
#define IR_CR3            (1 << 7)     // Capture channel 3
// T0IR, T1IR based on channel :
#define IR_MR( channel)   (1 << (channel))
#define IR_CR( channel)   (1 << ((channel) + 4))

// T0TCR, T1TCR :
#define TCR_ENABLE        (1 << 0)     // Enable counter
#define TCR_RESET         (1 << 1)     // Reset counter

// T0MCR, T1MCR :
#define MCR_INTERRUPT0    (1 << 0)     // interrupt on MR0
#define MCR_RESET0        (1 << 1)     // reset counter on MR0
#define MCR_STOP0         (1 << 2)     // stop counter by MR0
#define MCR_INTERRUPT1    (1 << 3)     // interrupt on MR1
#define MCR_RESET1        (1 << 4)     // reset counter on MR1
#define MCR_STOP1         (1 << 5)     // stop counter on MR1
#define MCR_INTERRUPT2    (1 << 6)     // interrupt on MR2
#define MCR_RESET2        (1 << 7)     // reset counter on MR2
#define MCR_STOP2         (1 << 8)     // stop counter on MR2
#define MCR_INTERRUPT3    (1 << 9)     // interrupt on MR3
#define MCR_RESET3        (1 << 10)    // reset counter on MR3
#define MCR_STOP3         (1 << 11)    // stop counter on MR3
// T0MCR, T1MCR based on channel :
#define MCR_INTERRUPT( channel) (1 << ((channel) * 3))       // interrupt on <channel>
#define MCR_RESET( channel)     (1 << ((channel) * 3 + 1))   // reset on <channel>
#define MCR_STOP( channel)      (1 << ((channel) * 3 + 2))   // stop on <channel>

// T0CCR, T1CCR :
#define CCR_RISING0       (1 << 0)     // rising edge on CAP0.0, CAP1.0
#define CCR_FALLING0      (1 << 1)     // falling edge on CAP0.0, CAP1.0
#define CCR_INTERRUPT0    (1 << 2)     // interrupt on CAP0.0, CAP1.0 event
#define CCR_RISING1       (1 << 3)     // rising edge on CAP0.1, CAP1.1
#define CCR_FALLING1      (1 << 4)     // falling edge on CAP0.1, CAP1.1
#define CCR_INTERRUPT1    (1 << 5)     // interrupt on CAP0.1, CAP1.1 event
#define CCR_RISING2       (1 << 6)     // rising edge on CAP0.2, CAP1.2
#define CCR_FALLING2      (1 << 7)     // falling edge on CAP0.2, CAP1.2
#define CCR_INTERRUPT2    (1 << 8)     // interrupt on CAP0.2, CAP1.2 event
#define CCR_RISING3       (1 << 9)     // rising edge on CAP0.3, CAP1.3
#define CCR_FALLING3      (1 << 10)    // falling edge on CAP0.3, CAP1.3
#define CCR_INTERRUPT3    (1 << 11)    // interrupt on CAP0.3, CAP1.3 event
// ToCCR, T1CCR based on channel :
#define CCR_RISING( channel)    (1 << ((channel) * 3))     // rising edge on <channel>
#define CCR_FALLING( channel)   (1 << ((channel) * 3 + 1)) // falling edge on <channel>
#define CCR_INTERRUPT( channel) (1 << ((channel) * 3 + 2)) // interrupt on <channel> event

// T0EMR, T1EMR :
#define EMR_MAT0          (1 << 0)     // connect MAT0.0, MAT1.0 pin
#define EMR_MAT1          (1 << 1)     // connect MAT0.1, MAT1.1 pin
#define EMR_MAT2          (1 << 2)     // connect MAT0.2, MAT1.2 pin
#define EMR_MAT3          (1 << 3)     // connect MAT0.3, MAT1.3 pin
#define EMR_NOP0          (0 << 4)     // do nothing
#define EMR_CLEAR0        (1 << 4)     // clear MAT
#define EMR_SET0          (2 << 4)     // set MAT
#define EMR_TOGGLE0       (3 << 4)     // toggle MAT
#define EMR_NOP1          (0 << 6)     // do nothing
#define EMR_CLEAR1        (1 << 6)     // clear MAT
#define EMR_SET1          (2 << 6)     // set MAT
#define EMR_TOGGLE1       (3 << 6)     // toggle MAT
#define EMR_NOP2          (0 << 8)     // do nothing
#define EMR_CLEAR2        (1 << 8)     // clear MAT
#define EMR_SET2          (2 << 8)     // set MAT
#define EMR_TOGGLE2       (3 << 8)     // toggle MAT
#define EMR_NOP3          (0 << 10)    // do nothing
#define EMR_CLEAR3        (1 << 10)    // clear MAT
#define EMR_SET3          (2 << 10)    // set MAT
#define EMR_TOGGLE3       (3 << 10)    // toggle MAT
// ToEMR, T1EMR based on channel :
#define EMR_MAT( channel)     (1 << (channel))
#define EMR_MASK( channel)    (3 << ((channel) * 2 + 4))  // use : & ~EMR_MASK(ch) to set zeroes
#define EMR_NOP( channel)     (0 << ((channel) * 2 + 4))
#define EMR_CLEAR( channel)   (1 << ((channel) * 2 + 4))
#define EMR_SET( channel)     (2 << ((channel) * 2 + 4))
#define EMR_TOGGLE( channel)  (3 << ((channel) * 2 + 4))

//--------- LPC213x ------------------------------------------------------------

// T0CTCR, T1CTCR :
#define CTCR_TIMER              0      // Count rising edges on PCLK
#define CTCR_COUNT_RISING       1      // Rising edge on CAP input
#define CTCR_COUNT_FALLING      2      // Falling edge on CAP input
#define CTCR_COUNT_BOTH         3      // Both edges on CAP input 
#define CTCR_CAP0         (0 << 2)     // select CAP0.0, CAP1.0 input
#define CTCR_CAP1         (1 << 2)     // select CAP0.1, CAP1.1 input
#define CTCR_CAP2         (2 << 2)     // select CAP0.2, CAP1.2 input
#define CTCR_CAP3         (3 << 2)     // select CAP0.3, CAP1.3 input

//------------------------------------------------------------------------------
//   PWM
//------------------------------------------------------------------------------

// PWMIR :
#define PWMIR_PWMMR0      (1 <<  0)    // interrupt flag for channel PWM 0
#define PWMIR_PWMMR1      (1 <<  1)    // interrupt flag for channel PWM 1
#define PWMIR_PWMMR2      (1 <<  2)    // interrupt flag for channel PWM 2
#define PWMIR_PWMMR3      (1 <<  3)    // interrupt flag for channel PWM 3
#define PWMIR_PWMMR4      (1 <<  8)    // interrupt flag for channel PWM 4
#define PWMIR_PWMMR5      (1 <<  9)    // interrupt flag for channel PWM 5
#define PWMIR_PWMMR6      (1 << 10)    // interrupt flag for channel PWM 6
// PWMIR based on PWM match register :
#define PWMIR_PWMMR( pwm_reg)  (((pwm_reg) > 3) ? (1 << ((pwm_reg) + 4)) :\
                                                  (1 << (pwm_reg)))

// PWMTCR :
#define PWMTCR_ENABLE     (1 << 0)     // Enable timer counter& prescaler
#define PWMTCR_RESET      (1 << 1)     // Reset timer counter & prescaler
#define PWMTCR_PWM_ENABLE (1 << 3)     // Enable PWM mode

// PWMMCR :
#define PWMMCR_INTERRUPT0 (1 <<  0)    // PWMMR0 match generates interrupt
#define PWMMCR_RESET0     (1 <<  1)    // PWMMR0 match resets PWMTC
#define PWMMCR_STOP0      (1 <<  2)    // PWMMR0 match resets PWMTC
#define PWMMCR_INTERRUPT1 (1 <<  3)    // PWMMR1 match generates interrupt
#define PWMMCR_RESET1     (1 <<  4)    // PWMMR1 match resets PWMTC
#define PWMMCR_STOP1      (1 <<  5)    // PWMMR1 match resets PWMTC
#define PWMMCR_INTERRUPT2 (1 <<  6)    // PWMMR2 match generates interrupt
#define PWMMCR_RESET2     (1 <<  7)    // PWMMR2 match resets PWMTC
#define PWMMCR_STOP2      (1 <<  8)    // PWMMR2 match resets PWMTC
#define PWMMCR_INTERRUPT3 (1 <<  9)    // PWMMR3 match generates interrupt
#define PWMMCR_RESET3     (1 << 10)    // PWMMR3 match resets PWMTC
#define PWMMCR_STOP3      (1 << 11)    // PWMMR3 match resets PWMTC
#define PWMMCR_INTERRUPT4 (1 << 12)    // PWMMR4 match generates interrupt
#define PWMMCR_RESET4     (1 << 13)    // PWMMR4 match resets PWMTC
#define PWMMCR_STOP4      (1 << 14)    // PWMMR4 match resets PWMTC
#define PWMMCR_INTERRUPT5 (1 << 15)    // PWMMR5 match generates interrupt
#define PWMMCR_RESET5     (1 << 16)    // PWMMR5 match resets PWMTC
#define PWMMCR_STOP5      (1 << 17)    // PWMMR5 match resets PWMTC
#define PWMMCR_INTERRUPT6 (1 << 18)    // PWMMR6 match generates interrupt
#define PWMMCR_RESET6     (1 << 19)    // PWMMR6 match resets PWMTC
#define PWMMCR_STOP6      (1 << 20)    // PWMMR6 match resets PWMTC
// PWMMCR based on PWM match register :
#define PWMMCR_INTERRUPT( pwm_reg)   (1 << ((pwm_reg) * 3))
#define PWMMCR_RESET( pwm_reg)       (1 << ((pwm_reg) * 3 + 1))
#define PWMMCR_STOP( pwm_reg)        (1 << ((pwm_reg) * 3 + 2))

// PWMPCR :
#define PWMPCR_PWMSEL2    (1 <<  2)    // PWM2 double edge mode
#define PWMPCR_PWMSEL3    (1 <<  3)    // PWM3 double edge mode
#define PWMPCR_PWMSEL4    (1 <<  4)    // PWM4 double edge mode
#define PWMPCR_PWMSEL5    (1 <<  5)    // PWM5 double edge mode
#define PWMPCR_PWMSEL6    (1 <<  6)    // PWM6 double edge mode
#define PWMPCR_PWMENA1    (1 <<  9)    // PWM1 output enable
#define PWMPCR_PWMENA2    (1 << 10)    // PWM1 output enable
#define PWMPCR_PWMENA3    (1 << 11)    // PWM1 output enable
#define PWMPCR_PWMENA4    (1 << 12)    // PWM1 output enable
#define PWMPCR_PWMENA5    (1 << 13)    // PWM1 output enable
#define PWMPCR_PWMENA6    (1 << 14)    // PWM1 output enable
// PWMPCR based on PWM match register :
#define PWMPCR_PWMSEL( pwm_reg)      (1 << (pwm_reg))
#define PWMPCR_PWMENA( pwm_reg)      (1 << ((pwm_reg) + 8))

// PWMLER :
#define PWMLER_ENABLE0    (1 <<  0)    // Enable PWM Match 0 Latch
#define PWMLER_ENABLE1    (1 <<  1)    // Enable PWM Match 1 Latch
#define PWMLER_ENABLE2    (1 <<  2)    // Enable PWM Match 2 Latch
#define PWMLER_ENABLE3    (1 <<  3)    // Enable PWM Match 3 Latch
#define PWMLER_ENABLE4    (1 <<  4)    // Enable PWM Match 4 Latch
#define PWMLER_ENABLE5    (1 <<  5)    // Enable PWM Match 5 Latch
#define PWMLER_ENABLE6    (1 <<  6)    // Enable PWM Match 6 Latch
// PWMLER based on PWM match register :
#define PWMLER_ENABLE( pwm_reg)     (1 << (pwm_reg))

//------------------------------------------------------------------------------
//   UART
//------------------------------------------------------------------------------

// U0IER, U1IER :
#define IER_RBR           (1 << 0)     // Enable Receive Data Available interrupt
#define IER_THRE          (1 << 1)     // Enable Tx Holding Register Empty interrupt
#define IER_LSR           (1 << 2)     // Enable Rx line status interrupt
//--------- U1IER
#define IER_MSR           (1 << 3)     // Modem Status Interrupt enable

// U0IIR, U1IIR :
#define IIR_NO_INTERRUPT  (1 << 0)     // no pending interrupts
#define IIR_THRE          (1 << 1)     // Tx Holding Register Empty interrupt
#define IIR_RDA           (2 << 1)     // Rx Data Available
#define IIR_RLS           (3 << 1)     // Rx Line Status
#define IIR_CTI           (6 << 1)     // Character Timeout Indicator
#define IIR_MASK          (7 << 1)     // interrupt identification mask
#define IIR_FCR           (3 << 6)     // FIFO enable bits - U0FCR, U1FCR copy
// --------- U1IIR
#define IIR_MSR           (0 << 1)     // Modem status interrupt

// U0FCR, U1FCR :
#define FCR_ENABLE        (1 << 0)    // FIFO Enable
#define FCR_RX_RESET      (1 << 1)    // Reset Rx FIFO
#define FCR_TX_RESET      (1 << 2)    // Reset Tx FIFO
#define FCR_TRIGGER1      (0 << 6)    // Trigger level 1 character
#define FCR_TRIGGER4      (1 << 6)    // Trigger level 4 characters
#define FCR_TRIGGER8      (2 << 6)    // Trigger level 8 characters
#define FCR_TRIGGER14     (3 << 6)    // Trigger level 14 characters

// U0LCR, U1LCR :
#define LCR_CHAR5         (0 << 0)    // 5-bit character length
#define LCR_CHAR6         (1 << 0)    // 6-bit character length
#define LCR_CHAR7         (2 << 0)    // 7-bit character length
#define LCR_CHAR8         (3 << 0)    // 8-bit character length
#define LCR_STOP1         (0 << 2)    // 1 stop bit
#define LCR_STOP2         (1 << 2)    // 2 stop bits (1.5 for 5-bit)
#define LCR_NO_PARITY     (0 << 3)    // No Parity
#define LCR_ODD_PARITY    (1 << 3)    // Odd Parity
#define LCR_EVEN_PARITY   (3 << 3)    // Even Parity
#define LCR_MARK_PARITY   (5 << 3)    // stick "1" Parity
#define LCR_SPACE_PARITY  (7 << 3)    // stick "0" Parity
#define LCR_BREAK         (1 << 6)    // Enable Break transmission
#define LCR_DLAB          (1 << 7)    // Enable Divisor Latch Access

// U0LSR, U1LSR :
#define LSR_RDR            (1 << 0)    // Receive Data Ready
#define LSR_OE             (1 << 1)    // Overrun Error
#define LSR_PE             (1 << 2)    // Parity Error
#define LSR_FE             (1 << 3)    // Framing Error
#define LSR_BI             (1 << 4)    // Break Interrupt
#define LSR_THRE           (1 << 5)    // Transmitter Holding Register Empty
#define LSR_TEMT           (1 << 6)    // Transmitter Empty
#define LSR_RXFE           (1 << 7)    // Error in Rx FIFO
#define LSR_LINE_MASK      (0xF << 1)  // line errors

// U1MCR :
#define MCR_DTR            (1 << 0)    // DTR control
#define MCR_RTS            (1 << 1)    // RTS control
#define MCR_LOOPBACK       (1 << 4)    // Enable modem loopback mode

// U1MSR :
#define MSR_DCTS           (1 << 0)    // Delta Clear To Send
#define MSR_DDSR           (1 << 1)    // Delta Data Set Ready
#define MSR_TERI           (1 << 2)    // Trailing Edge Ring Indicator
#define MSR_DDCD           (1 << 3)    // Delta Data Carrier Detect
#define MSR_CTS            (1 << 4)    // Clear To Send state
#define MSR_DSR            (1 << 5)    // Data Set Ready state
#define MSR_RI             (1 << 6)    // Ring Indicator state
#define MSR_DCD            (1 << 7)    // Data Carrier Detect state

// U0ACR, U1ACR :
#define ACR_START          (1 << 0)    // autobaud running
#define ACR_MODE           (1 << 1)    // autobaud mode 0 / 1
#define ACR_AUTO_RESTART   (1 << 2)    // restart by timeout
#define ACR_ABEO_CLEAR     (1 << 8)    // end of autobaud clear interrupt
#define ACR_ABTO_CLEAR     (1 << 9)    // autobaud timeout clear interrupt

// U0FDR, U1FDR :
#define FDR_DIVADDVAL( v)  ((v) & 0x0F)
#define FDR_MULVAL( v)     (((v) & 0x0F) << 4)

// U0TER, U1TER :
#define TER_TXEN           (1 << 7)    // stop sending

//--------- LPC213x ------------------------------------------------------------
// U1TER :
#define TER_TXEN           (1 << 7)    // Enable Tx

//------------------------------------------------------------------------------
//   SPI
//------------------------------------------------------------------------------

// SPCR :
#define SPCR_CPHA          (1 << 3)    // Clock phase control
#define SPCR_POL           (1 << 4)    // Clock polarity control
#define SPCR_MSTR          (1 << 5)    // Master mode
#define SPCR_LSBF          (1 << 6)    // LSB first
#define SPCR_SPIE          (1 << 7)    // SPI interrupt enable

// SPSR :
#define SPSR_ABRT          (1 << 3)    // Slave abort
#define SPSR_MODF          (1 << 4)    // Mode fault error
#define SPSR_ROVR          (1 << 5)    // Read overrrun
#define SPSR_WCOL          (1 << 6)    // Write collision
#define SPSR_SPIF          (1 << 7)    // SPI transfer complete flag

// SPIINT :
#define SPINT_INT          (1 << 0)    // SPI interrupt flag

//------------------------------------------------------------------------------
//   I2C
//------------------------------------------------------------------------------

// I2CONSET :
#define I2CONSET_AA        (1 << 2)    // Assert acknowledge flag
#define I2CONSET_SI        (1 << 3)    // I2C interrupt flag
#define I2CONSET_STO       (1 << 4)    // STOP flag
#define I2CONSET_STA       (1 << 5)    // START flag
#define I2CONSET_I2EN      (1 << 6)    // I2C interface enable

// I2CONCLR :
#define I2CONCLR_AAC       (1 << 2)    // Assert Acknowledge Clear bit
#define I2CONCLR_SIC       (1 << 3)    // I2C interrupt clear bit
#define I2CONCLR_STAC      (1 << 5)    // Start flag clear bit
#define I2CONCLR_I2ENC     (1 << 6)    // I2C interface disable bit

// I2STAT :
#define I2STAT_BUS_ERROR      0x00     // bus error / illegal frame
#define I2STAT_IRELEVANT      0xF8     // status unknown (SI not set)
// master
#define I2STAT_START          0x08     // START condition transmitted
#define I2STAT_REP_START      0x10     // repeated START tranmitted
#define I2STAT_SLAW_ACK       0x18     // SLA+W transmitted, ACK received
#define I2STAT_SLAW_NAK       0x20     // SLA+W transmitted, NOT ACK received
#define I2STAT_TX_MDATA_ACK   0x28     // I2DAT transmitted, ACK received
#define I2STAT_TX_MDATA_NAK   0x30     // I2DAT transmitted, NOT ACK received
#define I2STAT_BUS_LOST       0x38     // Arbitration lost in SLA+R/W or data bytes
#define I2STAT_SLAR_ACK       0x40     // SLA+R transmitted, ACK received
#define I2STAT_SLAR_NAK       0x48     // SLA+R transmitted, NOT ACK received
#define I2STAT_RX_MDATA_ACK   0x50     // Data received, ACK returned
#define I2STAT_RX_MDATA_NAK   0x58     // Data received, NOT ACK returned
// slave
#define I2STAT_SLAW           0x60     // Own SLA+W received, ACK Tx
#define I2STAT_SLAW_LOST      0x68     // Arbitration lost as master, own SLA+W received
#define I2STAT_GENERAL        0x70     // General call address received, ACK Tx
#define I2STAT_GENERAL_LOST   0x78     // Arbitration lost as master, general address received
#define I2STAT_RX_SDATA_ACK   0x80     // Own SLA, Data received, ACK Tx
#define I2STAT_RX_SDATA_NAK   0x88     // Own SLA, Data received, NOT ACK Tx
#define I2STAT_RX_GDATA_ACK   0x90     // General call, Data received, ACK Tx
#define I2STAT_RX_GDATA_NAK   0x98     // General call, Data received, NOT ACK Tx
#define I2STAT_SLA_STOP       0xA0     // STOP or repeated START while still addressed as SLV
#define I2STAT_SLAR           0xA8     // own SLA+R received, ACK Tx
#define I2STAT_SLAR_LOST      0xB0     // Arbitration lost as master, own SLA+R received
#define I2STAT_TX_SDATA_ACK   0xB8     // I2DAT transmitted, ACK received
#define I2STAT_TX_SDATA_NAK   0xC0     // I2DAT transmitted, NOT ACK received
#define I2STAT_TX_SDATA_LAST  0xC8     // Last data I2DAT transmitted, ACK received

// I2ADR :
#define I2ADR_GC           (1 << 0)    // General Call bit
#define I2ADR_SET( a)      ((a) << 1)  // prepare Slave mode address for port write
#define I2ADR_GET( p)      ((p) >> 1)  // get Slave mode address from port value

//------------------------------------------------------------------------------
//   ADC
//------------------------------------------------------------------------------

// ADCR :
#define ADCR_SEL( Channel)    (1 << (Channel)) // Select input channel
#define ADCR_CLKDIV( ClkDiv)  ((ClkDiv) <<  8) // APB clock dividier - 8bits wide
#define ADCR_BURST            (1 << 16)        // Burst conversion
#define ADCR_CLKS( Clks)      ((Clks)   << 17) // Encoded conversion clocks/resolution 3bits wide
#define ADCR_10BITS           ADCR_CLKS(0)
#define ADCR_9BITS            ADCR_CLKS(1)
#define ADCR_8BITS            ADCR_CLKS(2)
#define ADCR_7BITS            ADCR_CLKS(3)
#define ADCR_6BITS            ADCR_CLKS(4)
#define ADCR_5BITS            ADCR_CLKS(5)
#define ADCR_4BITS            ADCR_CLKS(6)
#define ADCR_3BITS            ADCR_CLKS(7)
#define ADCR_PDN              (1 << 21)        // ADC Power On
#define ADCR_START( Source)   ((Source) << 24) // Encoded conversion start source
#define ADCR_STOP             ADCR_START(0)    // No start
#define ADCR_START_NOW        ADCR_START(1)    // Start now
#define ADCR_START_P016       ADCR_START(2)    // Start on port P0.16
#define ADCR_START_P022       ADCR_START(3)    // Start on port P0.22
#define ADCR_START_MAT01      ADCR_START(4)    // Start on MAT0.1
#define ADCR_START_MAT03      ADCR_START(5)    // Start on MAT0.3
#define ADCR_START_MAT10      ADCR_START(6)    // Start on MAT1.0
#define ADCR_START_MAT11      ADCR_START(7)    // Start on MAT1.1
#define ADCR_EDGE             (1 << 27)        // Start on falling edge (0-rising)

// ADGDR :
#define ADGDR_RESULT_MASK     1023
#define ADGDR_RESULT( Reg)    (((Reg) >> 6) & ADGDR_RESULT_MASK)   // get conversion value
#define ADGDR_CHANNEL_MASK       7
#define ADGDR_CHANNEL( Reg)   (((Reg) >> 24) & ADGDR_CHANNEL_MASK) // get conversion channel
#define ADGDR_OVERUN          (1 << 30)        // lost conversion
#define ADGDR_DONE            (1 << 31)        // conversion done

// ADGSR :
#define ADGSR_BURST            (1 << 16)        // Burst conversion
#define ADGSR_START( Source)   ((Source) << 24) // Encoded conversion start source
#define ADGSR_STOP             ADCR_START(0)    // No start
#define ADGSR_START_NOW        ADCR_START(1)    // Start now
#define ADGSR_START_P016       ADCR_START(2)    // Start on port P0.16
#define ADGSR_START_P022       ADCR_START(3)    // Start on port P0.22
#define ADGSR_START_MAT01      ADCR_START(4)    // Start on MAT0.1
#define ADGSR_START_MAT03      ADCR_START(5)    // Start on MAT0.3
#define ADGSR_START_MAT10      ADCR_START(6)    // Start on MAT1.0
#define ADGSR_START_MAT11      ADCR_START(7)    // Start on MAT1.1
#define ADGSR_EDGE             (1 << 27)        // Start on falling edge (0-rising)

// ADSTAT :
#define ADSTAT_DONE0           (1 << 0)         // Done channel 0
#define ADSTAT_DONE1           (1 << 1)         // Done channel 1
#define ADSTAT_DONE2           (1 << 2)         // Done channel 2
#define ADSTAT_DONE3           (1 << 3)         // Done channel 3
#define ADSTAT_DONE4           (1 << 4)         // Done channel 4
#define ADSTAT_DONE5           (1 << 5)         // Done channel 5
#define ADSTAT_DONE6           (1 << 6)         // Done channel 6
#define ADSTAT_DONE7           (1 << 7)         // Done channel 7
#define ADSTAT_OVERRUN0        (1 << 8)         // Overrun channel 0
#define ADSTAT_OVERRUN1        (1 << 9)         // Overrun channel 1
#define ADSTAT_OVERRUN2        (1 <<10)         // Overrun channel 2
#define ADSTAT_OVERRUN3        (1 <<11)         // Overrun channel 3
#define ADSTAT_OVERRUN4        (1 <<12)         // Overrun channel 4
#define ADSTAT_OVERRUN5        (1 <<13)         // Overrun channel 5
#define ADSTAT_OVERRUN6        (1 <<14)         // Overrun channel 6
#define ADSTAT_OVERRUN7        (1 <<15)         // Overrun channel 7
#define ADSTAT_ADINT           (1 <<16)         // Interrupt flag

//------------------------------------------------------------------------------
//   DAC
//------------------------------------------------------------------------------

#define DACR_VALUE_MASK     1023
#define DACR_VALUE( Value)  (((Value) & DACR_VALUE_MASK) << 6) // DAC output value
#define DACR_BIAS           (1 << 16)                          // 2.5us setting 350uA current

#endif
