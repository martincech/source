//******************************************************************************
//
//   Country.h     Country & locales utility
//   Version 1.0   (c) VymOs
//
//******************************************************************************

#ifndef __Country_H__
   #define __Country_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __CountryDef_H__
   #include "CountryDef.h"
#endif

void CountryLoad( int Country);
// Fill country data by <Country> code

int CountryGetLanguage( void);
// Returns language code

void CountrySetLanguage( int Language);
// Set <Language> and code page

int CountryGetCodePage( void);
// Returns code page

int CountryGetDateFormat( void);
// Returns date format enum

char CountryGetDateSeparator1( void);
// Returns first date separator character

char CountryGetDateSeparator2( void);
// Returns second date separator character

int CountryGetTimeFormat( void);
// Returns time format enum

char CountryGetTimeSeparator( void);
// Returns time separator character

int CountryGetDstType( void);
// Returns daylight saving type

#endif
