//*****************************************************************************
//
//   Com1Pkt.h    COM packet interface
//   Version 1.0 (c) Vymos
//
//*****************************************************************************

#ifndef __Com1Pkt_H__
   #define __Com1Pkt_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // zakladni datove typy
#endif

#define PACKET_DATA       (-1)    // specialni kod prikazu - datovy paket

//-----------------------------------------------------------------------------
// Kratky prikaz
//-----------------------------------------------------------------------------

TYesNo Com1RxPacket( int *cmd, int *arg);
// Prijme paket, vrati typ prikazu <cmd> a parametr prikazu <arg>.
// Vrati NO, pokud paket neprisel

void Com1TxPacket( int cmd, int arg);
// Vysle paket s kodem prikazu <cmd> a parametrem <arg>

//-----------------------------------------------------------------------------
// Datovy blok
//-----------------------------------------------------------------------------

void Com1TxBlockStart( int size);
// Vysle zahlavi paketu pro data o velikosti <size>

void Com1TxBlockByte( byte b);
// Vysle byte <b> bloku dat

void Com1TxBlockEnd( void);
// Uzavre datovy blok

TYesNo Com1RxData( void *buffer, int *size);
// Prijem datoveho bloku

#endif
