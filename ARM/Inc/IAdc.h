//*****************************************************************************
//
//    IAdc.h       Internal A/D convertor functions
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __IAdc_H__
   #define __IAdc_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#define IADC_RANGE  1024

void IAdcInit( void);
// Inicializace prevodniku

word IAdcRead( int Channel);
// Cteni vstupu <Channel> prevodniku

#endif
