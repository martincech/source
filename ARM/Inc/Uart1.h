//*****************************************************************************
//
//    Uart1.h - RS232 communication services
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#ifndef __Uart1_H__
   #define __Uart1_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __ComDef_H__
   #include "ComDef.h"
#endif

//-----------------------------------------------------------------------------
// Funkce
//-----------------------------------------------------------------------------

void Uart1Init( void);
// Communication initialisation

void Uart1Disconnect( void);
// Disconnect port pins

void Uart1Setup( unsigned Baud, unsigned Format, word Timeout);
// Set communication parameters <Format> is TComFormat enum

TYesNo Uart1TxBusy( void);
// Returns YES if transmitter is busy

void Uart1TxChar( byte Char);
// Transmit <Char> byte

TYesNo Uart1RxChar( byte *Char);
// Returns YES and received byte on <Char>,
// or NO on timeout

TYesNo Uart1RxWait( native Timeout);
// Waits for receive up to <Timeout> miliseconds

void Uart1FlushChars( void);
// Skips all Rx chars up to intercharacter timeout

TYesNo Uart1IsBreak( void);
// Testuje Tx break

#endif
