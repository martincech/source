//*****************************************************************************
//
//    ComDef.h      RS232 parameters
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#ifndef __ComDef_H__
   #define __ComDef_H__


// Format :
typedef enum {
   COM_8BITS,                // 8bit without parity
   COM_8BITS_EVEN,           // 8bit even parity
   COM_8BITS_ODD,            // 8bit odd parity
   COM_8BITS_MARK,           // 8bit constant parity - 2 stopbits
   COM_8BITS_SPACE,          // 8bit constant 0 parity
   COM_7BITS,                // 7bit without parity
   COM_7BITS_EVEN,           // 7bit even parity
   COM_7BITS_ODD,            // 7bit odd parity
   COM_7BITS_MARK,           // 7bit const parity - 2 stopbits
   COM_7BITS_SPACE,          // 7bit constant 0 parity
   _COM_FORMAT_COUNT
} TComFormat;

#endif
