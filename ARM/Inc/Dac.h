//*****************************************************************************
//
//    Dac.h        Internal D/A convertor functions
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Dac_H__
   #define __Dac_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#define DAC_RANGE 1024              // DAC value : 0..DAC_RANGE - 1
#define DAC_MAX   (DAC_RANGE - 1)   // DAC max. value

void DacInit( void);
// Inicializace prevodniku

void DacValue( word Value);
// Nastaveni hodnoty <Value>

#endif
