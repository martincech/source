//*****************************************************************************
//
//    Bcd.h - BCD utility
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Bcd_H__
   #define __Bcd_H__

#ifndef __Uni_H__
   #include "Uni.h"
#endif

//-----------------------------------------------------------------------------
// nibble operations
//-----------------------------------------------------------------------------

#define lnibble( x)     ((byte)(x) & 0x0F)
#define hnibble( x)     ((byte)(x) >> 4)

byte BcdGetDigit( int Value, int Order);
// returns BCD digit from <Order>

int BcdSetDigit( int Value, int Order, byte Digit);
// set BCD <Digit> at <Order>

int BcdGetWidth( int Number);
// Returns order of the binary <Number>

int BcdGetXWidth( int Number);
// Returns order of the hexadecimal <Number>

//-----------------------------------------------------------------------------
// bin to BCD
//-----------------------------------------------------------------------------

dword dbin2bcd( dword x);
// converts 0..99 999 999 to bcd

//-----------------------------------------------------------------------------
// BCD to bin
//-----------------------------------------------------------------------------

dword dbcd2bin( dword x);
// converts 0..99 999 999 to bin

//-----------------------------------------------------------------------------
// to character
//-----------------------------------------------------------------------------

byte nibble2dec( byte x);
// converts low nibble to '0'..'9'

byte nibble2hex( byte x);
// converts low nibble to '0'..'F'

//-----------------------------------------------------------------------------
// to digit
//-----------------------------------------------------------------------------

byte char2dec( byte ch);
// converts ASCII '0'..'9' to digit

byte char2hex( byte ch);
// converts ASCII '0'..'9' and 'A'..'F' to digit

#endif
