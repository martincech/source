//******************************************************************************
//                                                                            
//   DCallback.h    Display callback definitions
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __DCallback_H__
   #define __DCallback_H__


// action callback :
typedef void TAction( int Value);

#endif
