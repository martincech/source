//******************************************************************************
//                                                                            
//  DEnter.h       Display enter value
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __DEnter_H__
   #define __DEnter_H__

#ifndef __StrDef_H__
   #include "StrDef.h"
#endif

#ifndef __DtDef_H__
   #include "../DtDef.h"
#endif

#ifndef __DCallback_H__
   #include "DCallback.h"
#endif

#ifndef __DLabel_H__
   #include "DLabel.h"   // Center type
#endif

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

TYesNo DEnterNumber( int *InitialValue, int Width, int Decimals, int EditWidth, int x, int y);
// Display and edit <InitialValue>. 
// <Width> - total digits, <Decimals> digits after decimal dot, <EditWidth> editable width

int DEnterNumberWidth( int Width, int Decimals);
// Returns pixel width of number field

TYesNo DEnterText( char *Text, int Width, int x, int y);
// Display and edit <Text> with maximal <Width>

int DEnterTextWidth( int Width);
// Returns pixel width of text field

#ifndef DENTER_CALLBACK
   TYesNo DEnterEnum( int *InitialValue, TUniStr Base, int EnumCount, int x, int y, TCenterType Center);
   // Display and edit <InitialValue>
#else
   TYesNo DEnterEnum( int *InitialValue, TUniStr Base, int EnumCount, TAction *OnChange, int x, int y, TCenterType Center);
   // Display and edit <InitialValue>
#endif

int DEnterEnumWidth( TUniStr Base, int EnumCount);
// Returns pixel width of enum field

TYesNo DEnterProgress( int *InitialValue, int MaxValue, TAction *OnChange, int x, int y);
// Enter value by progress bar

int DEnterProgressWidth( void);
// Returns pixel width of progress field

void DEnterPassword( char *Password, int Width, int x, int y);
// Enter password into <Password> array

int DEnterPasswordWidth( int Width);
// Returns pixel width of password field

TYesNo DEnterTime( TLocalTime *Local, int x, int y);
// Enter time

int DEnterTimeWidth( void);
// Returns character width of time field

TYesNo DEnterDate( TLocalTime *Local, int x, int y);
// Enter date

int DEnterDateWidth( void);
// Returns character width of date field


#endif
