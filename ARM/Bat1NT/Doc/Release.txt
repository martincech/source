                 Upravy Bat1

Build 7.0.0
- startovaci verze

Build 7.0.1 
- do menu doplnena Vazivost 30kg/50kg

Build 7.0.2  24.1.09
- zprovoznen WatchDog
- opraveno uzavreni souboru pri vytrzeni USB kabelu
- buffer overflow u nadpisu vyber statistiky
- pridany jazyky

Build 7.0.2a  2.2.09
- doplnena finstina

Build 7.0.3  6.2.09
- doplneni crash info
- upravy tiskove sestavy
- prodlouzen tisk radku pro pomale baudy
- pridany jazyky
- zmena poctu souboru na 128
- doplnen pocet vzorku ve vyberu souboru�

Build 7.1.0 8.2.09
- zmena poctu souboru na 199
- pri smazani souboru se neodstranil ze skupin
- jazyky


Build 7.1.1 10.2.09
- Menu.c presunuto DEventDiscard 
  (v memory full msg boxu zustaval viset K_TIMEOUT)
- doplneny jazyky

Build 7.1.2 14.2.09
- upraveny presypaci hodiny v Statistics/compare files

Build 7.1.3 8.3.09
- pridana nemcina
- poznamka u souboru/skupiny muze byt prazdna
- zmena zpracovani preruseni

Build 7.1.4 27.6.09
- formatovani akumulatoru, pridana prodleva 10min pred testovani CHG
- nabijeni akumulatoru, pridano testovani napeti > 3.8V pred testovanim CHG
- animace nabijeni, doplnen test na napeti > 3.8V pred testovanim CHG

Build 7.1.5 21.8.09
- uprava jazyku turectina, finstina, nemcina
- nove fonty - pridani tureckych znaku
- vystup ukladane hmotnosti na tiskarnu
- upraven dialog vyberu souboru/skupiny doplnen PgUp/PgDn

Build 7.1.6 7.9.09 + 21.10.09
- uprava fontu v japonstine
- doplneny japonske texty
- dodatecne upraven tiskovy protokol, doplneni
  souhrnne statistiky pri vazeni podle pohlavi
  
Build 7.2.0 27.10.09
- rozsiren enum jazyku o madarstinu
- madarske texty, uprava fontu

Build 7.2.1 16.1.10
- zmeneno default ScaleName na "SCALE 1"

Build 7.3.0 9.6.10
- pridany polske texty

Build 7.3.1 16.7.10
- pridany Verze Copp a Aviagen do projektoveho souboru
- implementace pro Copp - aktualni hmotnost se vysila na tiskarnu
- implementace pro Aviagen - ustalena hmotnost se vysila na tiskarnu az do odlehceni
- 27.8.10 upraven format vystupu pro Aviagen vcetne konverze z lb/g

Build 7.3.2 19.12.10
- uprava kalibrace pro snimac se zamenenou polaritou

Build 7.3.3 22.4.11
- uprava pro Aviagen : behem zadrzeni hodnoty na displeji
  se periodicky vysila ulozena hmotnost
- v ostatnich verzich by nemelo mit vliv

-------------------------------------------------------------------
Build 7.3.4 14.11.11
- upravena struktura adresaru projektu
- doplnena verze pro HW Board 8.3
- doplnen simulator
- upraveno zobrazeni cisla verze

Build 7.3.5 30.12.11
- upravena verze HW Board 8.4
- upraveno rizeni hlasitosti pro Board 8.4
- upraveno blikani po navratu z menu
- presunuta inicializace displeje na zacatek main
  (po zapnuti pri vybite baterii displej alespon blikne
   a taktez kvuli diagnostice akumulatoru v PMan.c)
  
Build 7.3.6 9.6.12
- upraveno casovani displeje Bias=12 a default kontrast
- drobne upravy zdrojovych textu pro Rowley 2.1

Build 7.4.0  19.6.2013
- pridana jazykova verze pro italstinu,
  rozsiren enum jazyku pro italstinu
  doplneny italske texty

Build 7.4.1  18.12.2013
- zvyseni ACCU_EMPTY_VOLTAGE z 3350 mV na 3380 mV - LDO se dostaval do saturace

Build 7.4.2  31.3.2014
- pridana RGB LED pro indikaci hmotnosti lehky/OK/tezky
