- Iic.c IicWrite netestuje ACK (Rx8025 vraci NAK)

- optimalizovat prekreslovani kurzoru DEnter.c, DEnterDate, DEnterTime, DInputText
  (prekresluje se vzdy cely text)

//- vyresit formatovany vystup pro DMsg...

- Vyresit zrychlovany autorepeat

- vytvorit DString centrovani textu DStringCenterCenter( char *Text, int x, y)...
  DStringLeftBottom(...)

- zvazit : GSetFont (Color, Pattern...) by mely vracet puvodni nastaveni,
  aby byl mozny navrat : OldMode = GSetMode( XOR); GSetMode( OldMode)
  (nebo spise doplnit funkce GGetFont, GGetMode, GGetColor, GGetTextAt...)

- DOptions : bitove pole s checkboxy (checkbox + nazev bitu) jako menu
  enter prepina checkbox

//- A/D prevodnik ma podezrele dlouhou dobu ustaleni po autokalibraci
//  (nyni cca 1.s viz weighihg.c)

- DEnterText doplnit nabidku pro japonstinu

- DEnter..., DInput... konstanty DENTER_COLOR_FG/BG podle typu vstupu ?

//- DMenu : zavest konstanty pro horni okraj, dolni okraj,
//  levy okraj a pravy okraj, vysku polozky

//- Ve vsech DInput & DMenu zavest volbu fontu
//  #define DMENU_FONT SetFont( LUCIDA16)

//- DDir zavest callback volany po zmene pozice kurzoru
//  (zobrazeni doplnujicich informaci o polozce)

- DMenu zavest callback volany po zmene pozice kurzoru
  (zobrazeni doplnujicich informaci o polozce - viz bubliny)

//- fitrace indikace akumulatoru (eliminovani kratkych poklesu napeti)

//- DSamples.c pridat prostor na nadpis a status line

//- DDir prostor pro nadpis a status line

- letni cas do struktury doplnit ofset [min]
  ted je pevne 1h

- obrazkove menu

//- vyzkouset prechod do IDLE mode v SysYield (PCON=IDL),
//  pripadne timer preprogramovat na > 10ms

- MenuCountry zobrazit separatory v Date format a Time format
  (CountryParameters)

//- po navratu z menu provede automaticke zvazeni (pokud je zatez)

- dynamicke zmeny rychlosti procesoru (zmena PLL)

//- je-li Sorting mode 2 (by sex) musi se nastavit Weight sorting mode = None
//  jinak se zbytecne zobrazuje LowLimit, HighLimit v menu Weighing
//  (nebo v menu Weighing povolovat Limit jen je-li Mode1 a Automatic)

- nastaveni DCOLOR_CURSOR COLOR_DARKGRAY se chova divne
  (XOR tmavoseda / cerna)

- GLine COLOR_LIGHTGRAY se kresli tmavoseda, v miste
  pruseciku s lightgray je bila

- doplnit SerialNumber do EEPROM ?

- doplnit ContrastBase do EEPROM ?

- doplnit rezervu do TWeighingParameters ?

----------------------------------------------------------------------------

//- Default saving mode d�t Auto, nyn� je Manual. Ve v�ze i v DLL

//- Po nacteni Config kontrolovat Country a Language na pripustne hodnoty

//- Pokud uzivatel zmeni v menu jednotky na libry nebo naopak z liber na g/kg,
//  je treba prepocitat parametry automatickeho ukladani:
//    Minimum weight, Stabilisation range
//  a to v globalnim configu i v jednotlivych souborech (pokud je povoleno).

//- Doplnit do USB nastaveni data/casu + doplnit do DLL knihovny

//- Doplnit PrintProtocol do Config a zajistit nastavovani pri
//  zmene protokolu tisku (zapamatovat typ protokolu po vypnuti vahy)

- testovat hluboke vybiti aku (s nabijecem by melo jit zapnout)

- Testovat aku maitenance - zaverecny protokol

//- rozmyslet, kam vlozit ScreenWait (ukladani, ruseni,...)

//- upravit pisteni :
//    st��du pro max. hlasitost d�vej jen 35 % - vy��� st��da nezvy�uje hlasitost, zvy�uje jen odb�r
//    st��du pro min. hlasitost by to cht�lo kolem 0,006 %, to nezvl�dne�, dej tedy cca 0,1 %. Minim�ln� hlasitost kl�vesnice bude st�le vysok�, zkra� tedy d�lku p�sk�n� kl�vesnice, ��m� se hlasitost jakoby sn��. 
//    p�i p�sk�n� zhasni podsvit � sn�� se odb�r z aku

