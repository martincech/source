//******************************************************************************
//                                                                            
//   Contrast.c     Display contrast
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#include "Contrast.h"
#include "../inc/St7259.h"        // GPU contrast

//------------------------------------------------------------------------------
//  Contrast Set
//------------------------------------------------------------------------------

void ContrastSet( void)
// Set contrast
{
   ContrastTest( Config.Display.Contrast);
} // ContrastSet

//------------------------------------------------------------------------------
//  Contrast Test
//------------------------------------------------------------------------------

void ContrastTest( int Contrast)
// Test contrast
{
   GpuContrast( GPU_EC_BASE - CONTRAST_MAX / 2 + Contrast);
} // ContrastTest
