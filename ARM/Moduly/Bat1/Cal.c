//******************************************************************************
//
//   Cal.c        Calibration utilities
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifdef __WIN32__
   // Borland only
   #include <vcl.h>
   #pragma hdrstop
#endif

#include "Cal.h"
#include "../Inc/Eep.h"             // EEPROM services
#include "MemoryDef.h"              // EEPROM layout
#include <string.h>

// Remark : basic units are pounds
// kg and g are recalculated by lb/kg factor

// Factor approximation for lb/kg recalculation (1lb = 0.453592kg)
// Factor is CAL_LB_MUL / (1 << CAL_LB_SHIFT) :
#define CAL_LB_MUL      475626   // 0.453592 * (1024 * 1024) rounded
#define CAL_LB_SHIFT    20       // 1024 * 1024

static TCalibration Calibration;

// EEPROM address :
#define CAL_ADDRESS       offsetof( TEeprom, Calibration)

// Local functions :

static void CalDefault( void);
// Default values

static TCalCrc CalCrc( void);
// Calculate CRC

//------------------------------------------------------------------------------
// Load
//------------------------------------------------------------------------------

void CalLoad( void)
// Load calibration from EEPROM
{
   EepLoadData( CAL_ADDRESS, &Calibration, sizeof( TCalibration));
   if( CalCrc() != Calibration.CheckSum){
      // wrong checksum, set defaults
      CalDefault();
      CalSave();
      return;
   }
} // CalLoad

//------------------------------------------------------------------------------
// Save
//------------------------------------------------------------------------------

void CalSave( void)
// Save calibration to EEPROM
{
   Calibration.CheckSum = CalCrc();
   EepSaveData( CAL_ADDRESS, &Calibration, sizeof( TCalibration));
} // CalSave

//------------------------------------------------------------------------------
// Update
//------------------------------------------------------------------------------

void CalUpdate( TRawWeight RawZero, TRawWeight RawRange, TWeight Range)
// Update calibration : ADC value <RawZero> by zero, <RawRange> by physical <Range>
{
   Calibration.Inversion = NO;
   if( RawZero > RawRange){
      // bridge inversion
      RawZero  = -RawZero;
      RawRange = -RawRange;
      Calibration.Inversion = YES;
   }
   Calibration.Offset = RawZero;
   Calibration.Factor = RawRange - RawZero;
   if( Calibration.Factor == 0){
      Calibration.Factor = 1;          // divide by zero prevention
   }
   switch( Config.Units.Units){
      case UNITS_G :
      case UNITS_KG :
         Calibration.Range = (TWeight)(((TMulWeight)Range << CAL_LB_SHIFT) / CAL_LB_MUL);
         break;
      case UNITS_LB :
         Calibration.Range  = Range;
         break;
   }
} // CalUpdate

//------------------------------------------------------------------------------
// Get range
//------------------------------------------------------------------------------

TWeight CalGetRange( void)
// Returns physical range
{
TWeight Range2;

   switch( Config.Units.Units){
      case UNITS_G :
      case UNITS_KG :
         Range2 = (TWeight)(((TMulWeight)Calibration.Range * CAL_LB_MUL * 2) >> CAL_LB_SHIFT); // range * 2
         return( (Range2 + 1) / 2);     // round
      case UNITS_LB :
         return( Calibration.Range);
   }
   return( 0);
} // CalGetRange

//------------------------------------------------------------------------------
// Inversion
//------------------------------------------------------------------------------

TYesNo CalGetInversion( void)
// Returns bridge polarity inversion
{
   return( Calibration.Inversion);
} // CalGetInversion

//------------------------------------------------------------------------------
// Weight
//------------------------------------------------------------------------------

TWeight CalWeight( TRawWeight RawValue)
// Calculate physical weight by <RawValue>
{
TWeight Weight2;

   RawValue -= Calibration.Offset;
   switch( Config.Units.Units){
      case UNITS_G :
      case UNITS_KG :
         Weight2 = (TWeight)((((TMulWeight)RawValue * Calibration.Range * CAL_LB_MUL * 2) /
                            Calibration.Factor) >> CAL_LB_SHIFT);    // weight * 2
         if( Weight2 >= 0){
            return( (Weight2 + 1) / 2);                              // round positive
         } else {
            return( (Weight2 - 1) / 2);                              // round negative
         }
         break;
      case UNITS_LB :
         return((TWeight)(((TMulWeight)RawValue * Calibration.Range) / Calibration.Factor));
   }
   return( 0);
} // CalWeight

//------------------------------------------------------------------------------
// Raw Weight
//------------------------------------------------------------------------------

TWeight CalRawWeight( TWeight Value)
// Calculate raw weight by physical <Value>
{
TRawWeight RawValue;

   switch( Config.Units.Units){
      case UNITS_G :
      case UNITS_KG :
         Value = (TWeight)(((TMulWeight)Value << CAL_LB_SHIFT) / CAL_LB_MUL);
         break;
      case UNITS_LB :
         break;
   }
   RawValue  = (TRawWeight)(((TMulWeight)Value * Calibration.Factor) / Calibration.Range);
   RawValue += Calibration.Offset;
   return( RawValue);
} // CalRawWeight

//******************************************************************************

//------------------------------------------------------------------------------
// Default
//------------------------------------------------------------------------------

static void CalDefault( void)
// Default values
{
   memset( &Calibration, 0, sizeof( TCalibration));
   Calibration.Offset   = 0;
   Calibration.Factor   = 1000;
   Calibration.Range    = 1000;
} // CalDefault

//------------------------------------------------------------------------------
// CRC
//------------------------------------------------------------------------------

#define CAL_DATA_SIZE    (int)offsetof( TCalibration, CheckSum)

static TCalCrc CalCrc( void)
// Calculate CRC
{
byte    *p;
TCalCrc Crc;
int     i;

   p   = (byte *)&Calibration;
   Crc = 0;
   for( i = 0; i < CAL_DATA_SIZE; i++){
      Crc += *p;
      p++;
   }
   return( ~Crc);
} // CalCrc
