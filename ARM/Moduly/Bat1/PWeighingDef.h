//*****************************************************************************
//
//    PWeighingDef.h     Weighing parameters definitions
//    Version 1.0        (c) VymOs & P.Veit
//
//*****************************************************************************

#ifndef __PWeighingDef_H__
  #define __PWeighingDef_H__

#ifndef __Uni_H__
   #include "../Inc/Uni.h"
#endif

#ifndef __WeightDef_H__
   #include "WeightDef.h"
#endif

#define NUMBER_OF_BIRDS_MAX  99        // maximal number of birds

//-----------------------------------------------------------------------------
// File-specific configuration
//-----------------------------------------------------------------------------

// Saving mode
typedef enum {
   SAVING_MODE_AUTOMATIC,              // Automatic saving
   SAVING_MODE_MANUAL,                 // Manual saving (with/without weight sorting)
   SAVING_MODE_MANUAL_BY_SEX,          // Manual saving (sorting by sex)
   _SAVING_MODE_COUNT
} TSavingMode;

// Mode of weight sorting
typedef enum {
   WEIGHT_SORTING_NONE,                // Sorting not in use
   WEIGHT_SORTING_LIGHT_HEAVY,         // Only one limit used (LowLimit)
   WEIGHT_SORTING_LIGHT_OK_HEAVY,      // Both limits used
   _WEIGHT_SORTING_COUNT
} TWeightSortingMode;

// Weight sorting
typedef struct {
   byte    Mode;	               // TWeightSortingMode - Mode of limits used
   byte    _Spare[ 3];
   TWeight LowLimit;		       // Limit / Low limit
   TWeight HighLimit;		       // High limit
} TWeightSorting;

// Saving parameters limits
#define FILTER_MAX               50    // 0.1s
#define STABILISATION_TIME_MAX   50    // 0.1s
#define STABILISATION_RANGE_MAX  990   // 0.1%

// Saving parameters
typedef struct {
   byte    Mode;                       // TSavingMode Saving mode
   byte    Filter;                     // Filter averaging time [0.1s]
   byte    StabilisationTime;          // Stable duration [0.1s]
   byte    _Spare1;
   TWeight MinimumWeight;              // Charge/discharge treshold [kg]
   word    StabilisationRange;         // Stable range [0.1%]
   word    _Spare2;
} TSavingParameters;

// Weighing configuration
typedef struct {
   word                 EnableMoreBirds;       // Enable number of birds else one bird
   word                 NumberOfBirds;         // Number of birds
   TWeightSorting       WeightSorting;         // Weight limits mode and weights
   TSavingParameters    Saving;                // Saving parameters
} TWeighingParameters;

#endif
