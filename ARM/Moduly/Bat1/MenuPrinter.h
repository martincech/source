//******************************************************************************
//                                                                            
//   MenuPrinter.h   Printer configuration menu
//   Version 1.0     (c) VymOs
//
//******************************************************************************

#ifndef __MenuPrinter_H__
   #define __MenuPrinter_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuPrinter( void);
// Printer configuration menu

#endif
