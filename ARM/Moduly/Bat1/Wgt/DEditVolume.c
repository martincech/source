//******************************************************************************
//
//   DEditVolume.c  Display volume edit box
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#include "DEditVolume.h"
#include "DEdit.h"
#include "../Beep.h"         // Beep

//------------------------------------------------------------------------------
//  Spin
//------------------------------------------------------------------------------

TYesNo DEditVolume( int x, int y, int *Volume)
// Edit volume by spinner
{
int VolumeKeyboard;

   // switch keyboard beeps off :
   VolumeKeyboard = Config.Sounds.VolumeKeyboard;     // save keyboard volume
   Config.Sounds.VolumeKeyboard = 0;                  // keyboard volume off
   // edit volume :
   if( !DEditSpin( x, y, Volume, 0, VOLUME_MAX, BeepTest)){
      Config.Sounds.VolumeKeyboard = VolumeKeyboard;  // restore keyboard volume
      return( NO);
   }
   Config.Sounds.VolumeKeyboard = VolumeKeyboard;     // restore keyboard volume
   return( YES);
} // DEditVolume
