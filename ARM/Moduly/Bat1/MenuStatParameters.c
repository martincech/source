//******************************************************************************
//                                                                            
//   MenuStatParameters.c  Statistic Parameters menu
//   Version 1.0           (c) VymOs
//
//******************************************************************************

#include "MenuStatParameters.h"

#include "../Inc/Graphic.h"       // graphic
#include "../Inc/conio.h"         // Display
#include "../Inc/Wgt/DLabel.h"    // Display label

#include "Wgt/DMenu.h"            // Display menu
#include "Wgt/DEdit.h"            // Display edit value

#include "Str.h"                  // Strings
#include "Config.h"               // Configuration
#include "Wgt/DWeight.h"          // Display weight
#include "Bat1.h"                 // Limits

static DefMenu( StatisticMenu)
   STR_UNIFORMITY_RANGE,
   STR_HISTOGRAM_MODE,
   STR_HISTOGRAM_RANGE,
   STR_HISTOGRAM_STEP,
EndMenu()

typedef enum {
   SM_UNIFORMITY_RANGE,
   SM_HISTOGRAM_MODE,
   SM_HISTOGRAM_RANGE,
   SM_HISTOGRAM_STEP,
} TStatisticMenuEnum;

// Local functions :

static void StatisticParameters( int Index, int y, TStatisticParameters *Statistic);
// Display statistic parameters

//------------------------------------------------------------------------------
//  Statistic parameters menu
//------------------------------------------------------------------------------

void MenuStatisticParameters( TStatisticParameters *Statistic)
// Edit statistic parameters
{
TMenuData MData;
int       i;

   DMenuClear( MData);
   forever {
      MData.Mask = 0;
      if( Statistic->Histogram.Mode == HISTOGRAM_MODE_RANGE){
         MData.Mask |= (1 << SM_HISTOGRAM_STEP);       // hide step
      } else {
         MData.Mask |= (1 << SM_HISTOGRAM_RANGE);      // hide range
      }
      if( !DMenu( STR_STATISTICS, StatisticMenu, (TMenuItemCb *)StatisticParameters, Statistic, &MData)){
         return;
      }
      switch( MData.Item){
         case SM_UNIFORMITY_RANGE :
            i = Statistic->UniformityRange;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, 1, 99, "%")){
               break;
            }
            Statistic->UniformityRange = i;
            break;

         case SM_HISTOGRAM_MODE :
            i = Statistic->Histogram.Mode;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, STR_HISTOGRAM_MODE_RANGE, _HISTOGRAM_MODE_COUNT)){
               break;
            }
            Statistic->Histogram.Mode = i;
            break;

        case SM_HISTOGRAM_RANGE :
            i = Statistic->Histogram.Range;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, 1, 99, "%")){
               break;
            }
            Statistic->Histogram.Range = i;
            break;

        case SM_HISTOGRAM_STEP :
            i = Statistic->Histogram.Step;
            if( !DEditWeight( DMENU_EDIT_X, MData.y, &i, 1, Config.Units.Range)){
               break;
            }
            Statistic->Histogram.Step  = i;
            break;
      }
   }
} // MenuLimits

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void StatisticParameters( int Index, int y, TStatisticParameters *Statistic)
// Display statistic parameters
{
   switch( Index){
      case SM_UNIFORMITY_RANGE :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d %%", Statistic->UniformityRange);
         break;

      case SM_HISTOGRAM_MODE :
         DLabelEnum( Statistic->Histogram.Mode, STR_HISTOGRAM_MODE_RANGE, DMENU_PARAMETERS_X, y);
         break;

      case SM_HISTOGRAM_RANGE :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d %%", Statistic->Histogram.Range);
         break;

      case SM_HISTOGRAM_STEP :
         DWeightWithUnitsNarrow( DMENU_PARAMETERS_X, y, Statistic->Histogram.Step);
         break;
   }
} // StatisticParameters
