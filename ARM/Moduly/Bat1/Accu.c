//*****************************************************************************
//
//    Accu.c       Accumulator utility
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Accu.h"
#include "PAccu.h"           // Accumulator maintenance
#include "../inc/System.h"   // Operating system
#include "../Inc/SFile.h"    // Secure file
#include "../Inc/IAdc.h"     // Internal A/D convertor
#include "Backlight.h"       // Backlight
#include "ScreenAccu.h"      // Display

#define ADC_SHIFT  2
#define ADC_READS  (1 << ADC_SHIFT)

#define CALIBRATION_TRIALS   20        // calibration voltage measuring count
#define CALIBRATION_VOLTAGE  4200      // calibration voltage [mV]
#define REFRESH_TIME         20        // refresh time after discharging
#define CONNECTED_TRIALS     500       // charger connected trials
#define CONNECTED_DELAY      10        // charger connected delay

// Local functions :

static TYesNo ChargerConnected( void);
// Test charger connected

static void ChargerOn( void);
// Switch charger on

static void ChargerOff( void);
// Switch charger off

static TYesNo ChargingDone( void);
// Test charging done

static TYesNo WaitForFormat( void);
// Wait for format done. Returns NO for shutdown

static int WaitForCharging( void);
// Wait for charging done. Returns time

static TYesNo WaitForRedraw( void);
// Wait for redraw event. Returns NO for shutdown

//-----------------------------------------------------------------------------
//   Initialisation
//-----------------------------------------------------------------------------

void AccuInit( void)
// Initialisation
{
   IAdcInit();
} // AccuInit

//-----------------------------------------------------------------------------
//   Maitenance
//-----------------------------------------------------------------------------

TYesNo AccuMaitenance( void)
// Process accumulator maitenance
{
   if( !AccuFormat()){
      return( NO);
   }
   if( !AccuDischarge()){
      return( NO);
   }
   if( !AccuCharge()){
      return( NO);
   }
   return( YES);
} // AccuMaitenance

//-----------------------------------------------------------------------------
//   Raw Voltage
//-----------------------------------------------------------------------------

unsigned AccuRawVoltage( void)
// Measure accumulator voltage [LSB]
{
unsigned Value;
int      i;

   Value = 0;
   for( i = 0; i < ADC_READS; i++){
      Value += IAdcRead( ACCU_ADC_VOLTAGE);
      SysUDelay( 20);
   }
   Value >>= ADC_SHIFT;
   return( Value);
} // AccuRawVoltage

//-----------------------------------------------------------------------------
//   Format
//-----------------------------------------------------------------------------

TYesNo AccuFormat( void)
// Format (charge) accu
{
unsigned Voltage;
int      i;

   // display screen frame :
   ScreenFormat();
   // check charger :
   if( !ChargerConnected()){
      ScreenConnectCharger();
      return( NO);
   }
   // switch charger on :
   ChargerOff();                       // reset charging status
   SysDelay( 100);
   // show format display :
   BacklightFlash();
   // enable charging :
   ChargerOn();
   SysDelay( 500);
   // wait for format :
   if( !WaitForFormat()){
      return( NO);                     // shutdown
   }
   // calibrate ADC :
   ScreenFormat();                     // erase display
   SysDelay( 1000);
   Voltage = 0;
   for( i = 0; i < CALIBRATION_TRIALS; i++){
      if( !WaitForRedraw()){
         return( NO);                  // shutdown
      }
      // 1s tick
      Voltage += AccuRawVoltage();
   }
   Voltage /= CALIBRATION_TRIALS;      // average
   PAccuCalibrate( Voltage, CALIBRATION_VOLTAGE);
   PAccuSave();
   // display done :
   ScreenFormatDone();
   BacklightFlash();
   return( YES);
} // AccuFormat

//-----------------------------------------------------------------------------
//   Discharge
//-----------------------------------------------------------------------------

TYesNo AccuDischarge( void)
// Discharge accu
{
TPAccuRecord Record;
unsigned     Voltage;
unsigned     SumVoltage;
unsigned     LowVoltage;
unsigned     LastVoltage;
unsigned     Time;
int          Period;
int          i;
int          SamplesCount;
int          DeltaCount;

   // display screen frame :
   ScreenDischarge();
   BacklightFlash();
   // start discharging :
   SFileReset();                       // empty logger
   Time           = 0;
   SumVoltage     = 0;
   LowVoltage     = PAccuRawVoltage( ACCU_EMPTY_VOLTAGE);
   LastVoltage    = 0;
   Period         = ACCU_LOGGER_PERIOD;
   SamplesCount   = 1;                 // Time 00:00 record
   DeltaCount     = 0;
   // first record :
   Record.Time    = 0;
   Record.Voltage = AccuRawVoltage();
   SFileAppend( &Record);
   // wait for discharging :
   BacklightLight();                   // backlight on
   ChargerOff();                       // start discharging
   forever {
      if( !WaitForRedraw()){
         ChargerOn();                  // enable charging
         return( NO);                  // shutdown
      }
      // 1 second tick
      Time++;
      Voltage     = AccuRawVoltage();
      SumVoltage += Voltage;
      ScreenTime( Time);
      ScreenVoltage( Voltage);
      if( --Period){
         continue;
      }
      // sampling period
      Voltage    = SumVoltage / ACCU_LOGGER_PERIOD;
      Period     = ACCU_LOGGER_PERIOD;  // new cycle
      SumVoltage = 0;
      // save sample :
      Record.Time    = Time / 60;
      Record.Voltage = Voltage;
      if( !SFileAppend( &Record)){
         ScreenEof();
         ChargerOn();                  // enable charging
         return( NO);
      }
      SamplesCount++;
      ScreenSample( SamplesCount, Voltage);
      // discharging treshold :
      if( Voltage <= LowVoltage){
         Record.Voltage = 0;
         SFileAppend( &Record);        // abnormal termination marker
         break;                        // done by absolute voltage
      }
      // check for delta delay :
      if( Time < ACCU_DELTA_DELAY){
         ScreenDelta( -1);             // display inactive delta
         LastVoltage = Voltage;        // remember last voltage
         continue;                     // skip delta resolving
      }
      // resolve delta :
      if( LastVoltage - Voltage >= ACCU_DELTA){
         // high difference
         DeltaCount++;
         if( DeltaCount >= ACCU_DELTA_COUNT){
            break;                     // done by voltage difference
         }
      } else {
         // low difference
         DeltaCount = 0;               // stop counting
      }
      ScreenDelta( DeltaCount);        // display delta
      LastVoltage = Voltage;           // remember last voltage
   }
   // discharging done
   BacklightDark();                    // backlight off
   ChargerOn();                        // enable charging
   // wait some time for charging :
   for( i = 0; i < REFRESH_TIME; i++){
      if( !WaitForRedraw()){
         return( NO);
      }
      // 1s tick
   }
   // save data :
   PAccuSetDischargeTime( Time / 60);
   AccuSaveCurve( SamplesCount);
   // display done :
   ScreenDischargeDone();
   BacklightFlash();
   return( YES);
} // AccuDischarge

//-----------------------------------------------------------------------------
//   Charge
//-----------------------------------------------------------------------------

TYesNo AccuCharge( void)
// Charge accu
{
int Time;

   // check charger :
   if( !ChargerConnected()){
      ScreenConnectCharger();
      return( NO);
   }
   // switch charger on :
   ChargerOff();                       // reset charging status
   SysDelay( 100);
   // display screen frame :
   ScreenCharge();
   BacklightFlash();
   // enable charging :
   ChargerOn();
   SysDelay( 500);
   // wait for charged :
   Time = WaitForCharging();
   if( Time < 0){
      return( NO);
   }
   // save charging time :
   Time /= 60;                         // convert to [min]
   PAccuSetChargeTime( Time);
   PAccuSave();
   // display done :
   ScreenChargeDone();
   BacklightFlash();
   return( YES);
} // AccuCharge

//-----------------------------------------------------------------------------
//   Save Curve
//-----------------------------------------------------------------------------

void AccuSaveCurve( int SamplesCount)
// Save discharging curve data
{
TPAccuRecord Record;

   SFileRead( ACCU_UMAX_DELAY / ACCU_LOGGER_PERIOD, &Record);  // ACCU_UMAX_DELAY after start
   PAccuSetUMax( Record.Voltage);
   SFileRead( SamplesCount / 2, &Record);                      // in the middle of sampling
   PAccuSetUMid( Record.Voltage);
   SFileRead( SamplesCount - (ACCU_DELTA_COUNT + 1), &Record); // last sample before delta
   PAccuSetUMin( Record.Voltage);
   SFileRead( SamplesCount - (ACCU_UWARN_DELAY / ACCU_LOGGER_PERIOD + 1), &Record); // ACCU_UWARN_DELAY to end of sampling
   PAccuSetUWarn( Record.Voltage);
   PAccuSave();
} // AccuSaveCurve

//*****************************************************************************

//-----------------------------------------------------------------------------
//   Charger connected
//-----------------------------------------------------------------------------

static TYesNo ChargerConnected( void)
// Test charger connected
{
int Count;
int i;

   Count = 0;
   i = CONNECTED_TRIALS;
   do {
      if( PManGetPPR()){
         Count++;
      }
      SysDelay( CONNECTED_DELAY);
   } while( --i);
   return( Count > CONNECTED_TRIALS / 2);
} // ChargerConnected

//-----------------------------------------------------------------------------
//   Charger on
//-----------------------------------------------------------------------------

static void ChargerOn( void)
// Switch charger on
{
   PManClrENNAB();
} // ChargerOn

//-----------------------------------------------------------------------------
//   Charger off
//-----------------------------------------------------------------------------

static void ChargerOff( void)
// Switch charger off
{
   PManSetENNAB();
} // ChargerOff

//-----------------------------------------------------------------------------
//   Charging done
//-----------------------------------------------------------------------------

static TYesNo ChargingDone( void)
// Test charging done
{
   return( PManGetCHG());
} // ChargingDone

//-----------------------------------------------------------------------------
//   Wait for format
//-----------------------------------------------------------------------------

static TYesNo WaitForFormat( void)
// Wait for format done. Returns NO for shutdown
{
int Time;
int Delay;

   Time = 0;
   // precharge delay without CHG testing :
   Delay = ACCU_PRECHARGE_DELAY;
   do {
      ScreenTime( Time);
      ScreenVoltage( AccuRawVoltage());
      if( !WaitForRedraw()){
         return( NO);                  // shutdown
      }
      // 1 second tick
      Time++;
   } while( --Delay);
   // start charging with CHG testing :
   while( !ChargingDone()){
      ScreenTime( Time);
      ScreenVoltage( AccuRawVoltage());
      if( !WaitForRedraw()){
         return( NO);                  // shutdown
      }
      // 1 second tick
      Time++;
   }
   return( YES);
} // WaitForFormat

//-----------------------------------------------------------------------------
//   Wait for charging
//-----------------------------------------------------------------------------

static int WaitForCharging( void)
// Wait for charging done. Returns time
{
int      Time;
unsigned TestVoltage;
unsigned RawVoltage;

   Time = 0;
   TestVoltage = PAccuRawVoltage( ACCU_CHARGED_VOLTAGE);
   forever {
      RawVoltage = AccuRawVoltage();
      if( RawVoltage > TestVoltage){
         // voltage over treshold, test CHG status
         if( ChargingDone()){
            break;
         }
      } // voltage under treshold
      ScreenTime( Time);
      ScreenVoltage( RawVoltage);
      if( !WaitForRedraw()){
         return( -1);                  // shutdown
      }
      // 1 second tick
      Time++;
   }
   return( Time);
} // WaitForCharging

//-----------------------------------------------------------------------------
//   Wait for redraw
//-----------------------------------------------------------------------------

static TYesNo WaitForRedraw( void)
// Wait for redraw event. Returns NO for shutdown
{
int Key;

   do {
      Key = SysYield();
      if( Key == K_SHUTDOWN){
         return( NO);
      }
   } while( Key != K_REDRAW);
   return( YES);
} // WaitForRedraw
