//******************************************************************************
//                                                                            
//   MenuMemory.h    Memory functions menu
//   Version 1.0     (c) VymOs
//
//******************************************************************************

#ifndef __MenuMemory_H__
   #define __MenuMemory_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuMemory( void);
// Memory configuration menu

#endif
