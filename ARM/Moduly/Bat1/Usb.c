//*****************************************************************************
//
//    Usb.c        USB utilities
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "../Inc/System.h"  // Operating system
#include "../Inc/Cpu.h"     // Interrupt enable/disable
#include "Usb.h"
#include "UsbDef.h"
#include "ConfigDef.h"
#include "../Inc/Eep.h"     // EEPROM services
#include "PMan.h"           // Power management
#include "../Inc/File/Fd.h" // File directory
#include "../Inc/File/Fs.h" // File system
#include <string.h>

#define USB_DISCONNECT     1             // disconnect UART on USB disconnect

#ifdef USB_COM1
   #define __COM1__
      #include "../inc/Com.h"
      #include "../inc/ComPkt.h"    // packet communication

   #define PACKET_MAX_DATA PACKET1_MAX_DATA
   #define COM_BAUD    UART1_BAUD
   #define COM_FORMAT  UART1_FORMAT
   #define COM_TIMEOUT UART1_TIMEOUT

#else // USB_COM0
   #define __COM0__
      #include "../inc/Com.h"
      #include "../inc/ComPkt.h"    // packet communication

   #define PACKET_MAX_DATA PACKET0_MAX_DATA
   #define COM_BAUD    UART0_BAUD
   #define COM_FORMAT  UART0_FORMAT
   #define COM_TIMEOUT UART0_TIMEOUT
#endif

#if PACKET_MAX_DATA != USB_MAX_DATA
   #error "PACKET_MAX_DATA mismatch"
#endif

// Local data :

static byte     UsbBuffer[ PACKET_MAX_DATA];
static unsigned _Address;
static byte     _FileOpened;

// Local functions :

static void ExecuteLong( int Size);
// Execute long command

static TYesNo ReadData( int Size);
// USB_CMD_READ command reply

static TYesNo DirectoryNext( void);
// USB_CMD_DIRECTORY_NEXT command reply

static TYesNo FileRead( int Size);
// USB_CMD_READ command reply

//------------------------------------------------------------------------------
//  Initialisation
//------------------------------------------------------------------------------

void UsbInit( void)
// Initialise
{
   ComInit();
   ComSetup( COM_BAUD, COM_FORMAT, COM_TIMEOUT);
   _Address    = 0;
   _FileOpened = NO;
} // UsbInit

//------------------------------------------------------------------------------
//  Execute
//------------------------------------------------------------------------------

void UsbExecute( void)
// USB executive
{
int Cmd, Data;
int Size;

   if( !ComRxPacket( &Cmd, &Data)){
      return;                          // no data received
   }
   if( Cmd == PACKET_DATA){
      // read remainder of the data packet
      if( !ComRxData( UsbBuffer, &Size)){
         return;                       // read error
      }
      ExecuteLong( Size);
      return;
   }
   switch( Cmd){
      case USB_CMD_VERSION :
         Data = Config.Version;
         break;

      case USB_CMD_GET_STATUS :
         Data = PManPowerOn();
         break;

      case USB_CMD_POWER_OFF :
         PManSetShutdown();            // set shutdown flag
         break;

      case USB_CMD_RELOAD :
         ConfigLoad();                 // reload configuration
         break;

      case USB_CMD_GET_TIME :
         Data = SysGetClock();
         break;

      case USB_CMD_SET_TIME :
         SysSetClock( Data);
         break;

      case USB_CMD_ADDRESS :
         _Address = Data;              // remember address
         break;

      case USB_CMD_GET_ADDRESS :
         Data = _Address;              // return address
         break;

      case USB_CMD_READ :
         if( !ReadData( Data)){
            Cmd = USB_CMD_ERROR;
            break;                     // send short packet
         }
         return;                       // long packet already send

      case USB_CMD_DIRECTORY_BEGIN :
         FdSetClass( FDIR_CLASS_WILDCARD);       // any file
         Data = FdCount();                       // nuber of directory entries
         FdFindBegin();                          // start directory read
         break;

      case USB_CMD_DIRECTORY_NEXT :
         if( !DirectoryNext()){
            Cmd = USB_CMD_ERROR;
            break;                     // send short packet
         }
         return;                       // long packet already send

      case USB_CMD_FILE_OPEN :
         FsClose();                    // close previous file
         FsOpen( Data, YES);           // open for read only
         _Address    = 0;              // prepare for read
         _FileOpened = YES;            // remember opened file
         break;

      case USB_CMD_FILE_CLOSE :
         FsClose();                    // close current file
         _FileOpened = NO;             // no file opened
         break;

      case USB_CMD_FILE_READ :
         if( !FileRead( Data)){
            Cmd = USB_CMD_ERROR;
            break;                     // send short packet
         }
         return;                       // long packet already send

      case USB_CMD_FORMAT :
         if( Data != USB_FORMAT_MAGIC){
            Cmd = USB_CMD_ERROR;
            break;
         }
         FsFormat();
         break;

   }
   ComTxPacket( Cmd | USB_CMD_REPLY, Data);
} // UsbExecute

//------------------------------------------------------------------------------
//  Connect
//------------------------------------------------------------------------------

void UsbConnect( void)
// USB connect
{
#ifdef USB_DISCONNECT
   DisableInts();                      // disable interrupts before pinselect manipulation
   ComInit();                          // pinselect for UART
   EnableInts();
#endif
} // UsbConnect

//------------------------------------------------------------------------------
//  Disconnect
//------------------------------------------------------------------------------

void UsbDisconnect( void)
// USB diconnect
{
#ifdef USB_DISCONNECT
   DisableInts();                      // disable interrupts before pinselect manipulation
   ComDisconnect();                    // release pins for GPIO
   EnableInts();
#endif
   if( _FileOpened){
      FsClose();                       // close working file
   }
} // UsbDisconnect

//*****************************************************************************

//------------------------------------------------------------------------------
//  Execute long
//------------------------------------------------------------------------------

static void ExecuteLong( int Size)
// Execute long command
{
int          Data;
int          RepCmd;
TUsbCmdUnion *Cmd;
TFdirInfo    *Info;

   Cmd    = (void *)&UsbBuffer;
   Data   = 0;                         // reply data
   RepCmd = Cmd->Cmd;                  // reply command
   switch( RepCmd){
      case USB_CMD_WRITE :
         // write EEPROM
         if( Size > USB_MAX_DATA){
            RepCmd = USB_CMD_ERROR;
            break;
         }
         Size -= USB_CMD_SIZE;
         EepSaveData( _Address, Cmd->Write.Data, Size);
         _Address += Size;
         Data = _Address;
         break;

      case USB_CMD_FILE_CREATE :
         // create file
         FsClose();                              // close previous file
         if( !FsCreate( Cmd->Create.Name, Cmd->Create.Class)){
            RepCmd = USB_CMD_ERROR;
            break;
         }
         Info = FsInfo();                        // get directory entry
         strcpy( Info->Note, Cmd->Create.Note);  // update directory entry
         Data = FdSearch( Cmd->Create.Name);     // get handle
         _Address = 0;                           // prepare for write
         _FileOpened = YES;                      // remember opened file
         break;

      case USB_CMD_FILE_WRITE :
         // write at <Address> offset of the current file
         if( Size > USB_MAX_DATA){
            RepCmd = USB_CMD_ERROR;
            break;
         }
         Size -= USB_CMD_SIZE;
         if( !FsWrite( Cmd->Write.Data, Size)){
             RepCmd = USB_CMD_ERROR;
             break;
         }
         _Address += Size;
         Data = _Address;
         break;

      default :
         break;
   }
   ComTxPacket( RepCmd | USB_CMD_REPLY, Data);
} // ExecuteLong

//------------------------------------------------------------------------------
//  Read data
//------------------------------------------------------------------------------

static TYesNo ReadData( int Size)
// USB_CMD_READ command reply
{
int i;

   if( Size > USB_MAX_DATA){
      return( NO);
   }
   EepLoadData( _Address, UsbBuffer, Size);
   ComTxBlockStart( Size);
   for( i = 0; i < Size; i++){
      ComTxBlockByte( UsbBuffer[ i]);
   }
   ComTxBlockEnd();
   _Address += Size;
   return( YES);
} // ReadData

//------------------------------------------------------------------------------
//  Directory Next
//------------------------------------------------------------------------------

static TYesNo DirectoryNext( void)
// USB_CMD_DIRECTORY_NEXT command reply
{
TFdirHandle       Handle;
TUsbDirectoryItem Item;
byte              *p;
int               i;
int               Size;

   Handle = FdFindNext();
   if( Handle == FDIR_INVALID){
      return( NO);
   }
   memset( &Item, 0, sizeof( Item));
   Item.Handle = Handle;
   Item.Class  = FdGetClass( Handle);
   FdLoad( Handle, &Item.Info);
   // send packet
   p = (byte *)&Item;
   Size = sizeof( TUsbDirectoryItem);
   ComTxBlockStart( Size);
   for( i = 0; i < Size; i++){
      ComTxBlockByte( *p);
      p++;
   }
   ComTxBlockEnd();
   return( YES);
} // DirectoryNext

//------------------------------------------------------------------------------
//  Read data
//------------------------------------------------------------------------------

static TYesNo FileRead( int Size)
// USB_CMD_READ command reply
{
int i;

   if( Size > USB_MAX_DATA){
      return( NO);
   }
   if( !FsRead( UsbBuffer, Size)){
      return( NO);
   }
   ComTxBlockStart( Size);
   for( i = 0; i < Size; i++){
      ComTxBlockByte( UsbBuffer[ i]);
   }
   ComTxBlockEnd();
   _Address += Size;
   return( YES);
} // FileRead
