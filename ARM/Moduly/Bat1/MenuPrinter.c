//******************************************************************************
//
//   MenuPrinter.c   Printer configuration menu
//   Version 1.0     (c) VymOs
//
//******************************************************************************

#include "MenuPrinter.h"

#include "../Inc/Graphic.h"       // graphic
#include "../Inc/conio.h"         // Display
#include "../Inc/Wgt/DLabel.h"    // Display label

#include "Wgt/DMenu.h"            // Display menu
#include "Wgt/DEdit.h"            // Display edit value
#include "Wgt/DMsg.h"             // Display message
#include "Str.h"                  // Strings

// Baud rate enum :
typedef enum {
   BAUD_2400,
   BAUD_4800,
   BAUD_9600,
   BAUD_19200,
   BAUD_38400,
   _BAUD_COUNT
} TBaud;

// Baud rate list :
DefList( BaudList)
   "2400 Bd",
   "4800 Bd",
   "9600 Bd",
   "19200 Bd",
   "38400 Bd",
EndList()

// COM format list :
DefList( FormatList)
   "8-N-1",
   "8-E-1",
   "8-O-1",
   "8-M-1",
   "8-S-1",
   "7-N-1",
   "7-E-1",
   "7-O-1",
   "7-M-1",
   "7-S-1",
EndList()

// Printer menu :

static DefMenu( PrinterMenu)
   STR_PAPER_WIDTH,
   STR_COMMUNICATION_SPEED,
   STR_COMMUNICATION_FORMAT,
EndMenu()

typedef enum {
   PM_PAPER_WIDTH,
   PM_COMMUNICATION_SPEED,
   PM_COMMUNICATION_FORMAT,
} TPrinterMenuEnum;

// Local functions :

static void PrinterParameters(  int Index, int y, void *UserData);
// Menu Printer parameters

static int EncodeCommunicationSpeed( int Baud);
// Encode <Baud> to enum

static int DecodeCommunicationSpeed( int BaudCode);
// Decode <BaudCode> enum to speed

//------------------------------------------------------------------------------
//   Menu
//------------------------------------------------------------------------------

void MenuPrinter( void)
// Printer configuration menu
{
TMenuData MData;
int       i;

   DMenuClear( MData);
   forever {
      if( !DMenu( STR_PRINTER, PrinterMenu, PrinterParameters, 0, &MData)){
         ConfigSaveSection( Printer);
         return;
      }
      switch( MData.Item){
         case PM_PAPER_WIDTH :
            i = Config.Printer.PaperWidth;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, 1, PAPER_WIDTH_MAX, 0)){
               break;
            }
            Config.Printer.PaperWidth = i;
            break;

         case PM_COMMUNICATION_SPEED :
            i = EncodeCommunicationSpeed( Config.Printer.CommunicationSpeed);
            if( !DEditList( DMENU_EDIT_X, MData.y, &i, BaudList)){
               break;
            }
            Config.Printer.CommunicationSpeed = DecodeCommunicationSpeed( i);
            break;

         case PM_COMMUNICATION_FORMAT :
            i = Config.Printer.CommunicationFormat;
            if( !DEditList( DMENU_EDIT_X, MData.y, &i, FormatList)){
               break;
            }
            Config.Printer.CommunicationFormat = i;
            break;
      }
   }
} // MenuPrinter

//******************************************************************************

//------------------------------------------------------------------------------
//   Parameters
//------------------------------------------------------------------------------

static void PrinterParameters(  int Index, int y, void *UserData)
// Menu Printer parameters
{
   switch( Index){
      case PM_PAPER_WIDTH :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Config.Printer.PaperWidth);
         break;

      case PM_COMMUNICATION_SPEED :
         DLabelList( EncodeCommunicationSpeed( Config.Printer.CommunicationSpeed), BaudList, DMENU_PARAMETERS_X, y);
         break;

      case PM_COMMUNICATION_FORMAT :
         DLabelList( Config.Printer.CommunicationFormat, FormatList, DMENU_PARAMETERS_X, y);
         break;
   }
} // PrinterParameters

//------------------------------------------------------------------------------
//   Baud coding
//------------------------------------------------------------------------------

const word _BaudTable[ _BAUD_COUNT] = { 2400, 4800, 9600, 19200, 38400};

static int EncodeCommunicationSpeed( int Baud)
// Encode <Baud> to enum
{
int i;

   for( i = 0; i < _BAUD_COUNT; i++){
      if( _BaudTable[ i] == Baud){
         return( i);
      }
   }
   return( BAUD_2400);
} // EncodeCommunicationSpeed


static int DecodeCommunicationSpeed( int BaudCode)
// Decode <BaudCode> enum to speed
{
   return( _BaudTable[ BaudCode]);
} // DecodeCommunicationSpeed
