//******************************************************************************
//                                                                            
//   MenuCalibrate.h  Calibration menu
//   Version 1.0      (c) VymOs
//
//******************************************************************************

#ifndef __MenuCalibrate_H__
   #define __MenuCalibrate_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuCalibrate( void);
// Process calibration

#endif
