//******************************************************************************
//                                                                            
//   ScreenFiles.h    Display files comparation
//   Version 1.0      (c) VymOs
//
//******************************************************************************

#ifndef __ScreenFiles_H__
   #define __ScreenFiles_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __StrDef_H__
   #include "../Inc/Wgt/StrDef.h"
#endif

#ifndef __GroupDef_H__
   #include "GroupDef.h"
#endif

#ifndef __Calc_H__
   #include "Calc.h"
#endif


void ScreenCompareFiles( TUniStr Caption, TGroup Group, TCalcMode Mode);
// Display file comparation by <Mode>

#endif
