//******************************************************************************
//                                                                            
//   MenuSorting.h    Weighing limits menu
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __MenuSorting_H__
   #define __MenuSorting_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __PWeighingDef_H__
   #include "PWeighingDef.h"
#endif

void MenuWeightSorting( TWeightSorting *Sorting);
// Edit weight sorting limits

#endif
