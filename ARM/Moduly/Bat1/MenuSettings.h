//******************************************************************************
//                                                                            
//   MenuSettings.h  Settings menu
//   Version 1.0     (c) VymOs
//
//******************************************************************************

#ifndef __MenuSettings_H__
   #define __MenuSettings_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuSettings( void);
// Settings menu

#endif
