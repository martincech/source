//*****************************************************************************
//
//    ScreenAccu.h  Accumulator maitenance display
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __ScreenAccu_H__
   #define __ScreenAccu_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void ScreenAccumulatorData( void);
// Display accumulator data

void ScreenConnectCharger( void);
// Display error message - charger off

void ScreenFormat( void);
// Display accumulator format screen

void ScreenDischarge( void);
// Display accumulator discharging screen

void ScreenCharge( void);
// Display accumulator charging screen

void ScreenFormatDone( void);
// Display format done

void ScreenDischargeDone( void);
// Display discharging done

void ScreenChargeDone( void);
// Display charging done

void ScreenEof( void);
// Terminated by EOF

void ScreenTime( unsigned Time);
// Display time

void ScreenVoltage( unsigned Voltage);
// Display voltage

void ScreenSample( int Count, unsigned Voltage);
// Display sampled voltage

void ScreenDelta( int DeltaCount);
// Display Delta count

#endif
