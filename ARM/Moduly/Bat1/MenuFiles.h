//******************************************************************************
//                                                                            
//   MenuFiles.h    Files menu
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __MenuFiles_H__
   #define __MenuFiles_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuFiles( void);
// Files menu

#endif
