//******************************************************************************
//                                                                            
//   MenuFiles.c    Files menu
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#include "MenuFiles.h"
#include "../Inc/Graphic.h"       // graphic
#include "../Inc/conio.h"         // Display
#include "../Inc/File/Fd.h"       // File directory list

#include "Wgt/DMenu.h"            // Display menu
#include "Wgt/DInput.h"           // Display input value
#include "Wgt/DMsg.h"             // Display message
#include "Wgt/DDir.h"             // Display file directory

#include "Str.h"                  // Strings
#include "Sdb.h"                  // Samples database
#include "Beep.h"                 // Beep
#include "MenuSaving.h"           // Menu Saving Parameters
#include "Wgt/DSamples.h"         // Display samples database
#include "Weighing.h"             // Weighing
#include "Group.h"                // File groups
#include <string.h>

static DefMenu( FilesMenu)
   STR_CREATE,
   STR_EDIT_SAVING_OPTIONS,
   STR_EDIT_NOTE,
   STR_RENAME,
   STR_DELETE,
EndMenu()

typedef enum {
   FM_CREATE,
   FM_EDIT_SAVING_OPTIONS,
   FM_EDIT_NOTE,
   FM_RENAME,
   FM_DELETE,
} TFilesMenuEnum;

//------------------------------------------------------------------------------
//   Menu
//------------------------------------------------------------------------------

void MenuFiles( void)
// Files menu
{
TMenuData   MData;
TFdirHandle Handle;
char        Name[ FDIR_NAME_LENGTH + 1];
char        Note[ FDIR_NOTE_LENGTH + 1];

   // prepare menu mask :
   DMenuClear( MData);
   MData.Mask = 0;
   FdSetClass( SDB_CLASS);                       // samples database directory class
   // check for file parameters :
   if( !Config.EnableFileParameters){
      MData.Mask = (1 << FM_EDIT_SAVING_OPTIONS);// hide parameters
   }
   // run menu
   forever {
      if( !DMenu( STR_FILES, FilesMenu, 0, 0, &MData)){
         return;
      }
      if( MData.Item != FM_CREATE){
         // select file :
         Handle = DDirSelectFile( STR_SELECT_FILE, FDIR_INVALID);
         if( Handle == FDIR_INVALID){
            continue;
         }
         // update file parameters :
         SdbOpen( Handle, NO);         // open file
         WeighingSetGlobalParameters();// update file by global parameters
         SdbClose();
      }
      switch( MData.Item){
         case FM_CREATE :
            Name[ 0] = '\0';                               // empty string
            if( !DInputText( STR_CREATE, STR_ENTER_NAME, Name, FDIR_NAME_LENGTH, NO)){
               break;
            }
            if( FdExists( Name)){
               DMsgOk( STR_ERROR, STR_FILE_EXISTS, Name);
               break;
            }
            *SdbConfig() = Config.WeighingParameters;      // fill with defaults
            if( !SdbCreate( Name)){
               DMsgOk( STR_ERROR, STR_NOT_ENOUGH_SPACE, 0);
               break;
            }
            SdbClose();
            break;

         case FM_EDIT_SAVING_OPTIONS :
            SdbOpen( Handle, NO);
            MenuSavingParameters( SdbConfig(), NO);
            SdbClose();
            break;

         case FM_EDIT_NOTE :
            SdbOpen( Handle, NO);
            strcpy( Note, SdbInfo()->Note);
            if( !DInputText( STR_EDIT_NOTE, STR_ENTER_NOTE, Note, FDIR_NOTE_LENGTH, YES)){
               SdbClose();
               break;
            }
            strcpy( SdbInfo()->Note, Note);
            SdbClose();
            break;

         case FM_RENAME :
            FdGetName( Handle, Name);
            if( !DInputText( STR_RENAME, STR_ENTER_NAME, Name, FDIR_NAME_LENGTH, NO)){
               break;
            }
            if( !SdbRename( Handle, Name)){
               DMsgOk( STR_ERROR, STR_FILE_EXISTS, Name);
               break;
            }
            break;

         case FM_DELETE :
            FdGetName( Handle, Name);
            if( !DMsgYesNo( STR_DELETE, STR_REALLY_DELETE_FILE, Name)){
               break;
            }
            SdbOpen( Handle, NO);
            SdbDelete();
            if( Handle == Config.LastFile){
               // current file deleted
               Config.LastFile = FDIR_INVALID;
               ConfigSaveSection( LastFile);
            }
            GroupDeleteFile( Handle);            // remove file from all groups
            FdSetClass( SDB_CLASS);              // restore class
            break;
      }
   }
} // MenuFiles
