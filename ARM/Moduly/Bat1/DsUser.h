//******************************************************************************
//
//   DsUser.h      Data Set user definitions
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __DsUser_H__
   #define __DsUser_H__

#ifndef __Sdb_H__
   #include "Sdb.h"
#endif

#define DS_CLASS            SDB_CLASS            // dataset current class

#endif
