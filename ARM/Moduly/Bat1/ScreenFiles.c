//******************************************************************************
//                                                                            
//   ScreenFiles.c    Display files comparation
//   Version 1.0      (c) VymOs
//
//******************************************************************************

#include "ScreenFiles.h"
#include "../Inc/Cpu.h"           // WatchDog
#include "../Inc/Graphic.h"       // graphic
#include "../Inc/conio.h"         // Display
#include "../Inc/Wgt/DEvent.h"    // Display events
#include "../Inc/Wgt/Beep.h"
#include "../Inc/File/Fd.h"
#include "Str.h"                  // Strings

#include "Sdb.h"                  // Samples database
#include "Group.h"                // File groups
#include "Calc.h"                 // Statistics calculations
#include "Wgt/DWeight.h"          // Display weight
#include "Wgt/DLayout.h"          // Display layout
#include "Fonts.h"                // Project fonts
#include "Bitmap.h"               // Bitmaps
#include "Screen.h"               // ScreenWait

#define PAGE_SIZE  2              // items per page

// Local functions :

static int GetLinesCount( TGroup Group);
// Returns total nuber of lines

static void DisplayPage( TGroup Group, int FirstLine, TStatisticsType Type, TCalcMode Mode);
// Display page starting at <FirstLine>

static void DrawLine( int Row, const char *Name, TCalc *Statistics);
// Draw single file/total data

static void DisplayType( TStatisticsType Type, TCalcMode Mode);
// Display statistics type by <Type> & <Mode>

//------------------------------------------------------------------------------
//   Compare files
//------------------------------------------------------------------------------

void ScreenCompareFiles( TUniStr Caption, TGroup Group, TCalcMode Mode)
// Display file comparation by <Mode>
{
int FirstLine;
int LastFirstLine;
int Type;
int LinesCount;

   SetFont( TAHOMA16);
   FdSetClass( SDB_CLASS);                       // samples database directory class
   FirstLine     = 0;
   LastFirstLine = -1;
   Type          = STATISTICS_TOTAL;
   LinesCount = GetLinesCount( Group);
   forever {
      if( FirstLine != LastFirstLine){
         // redraw page
         ScreenWait();                          // show waiting symbol
         GClear();
         DLayoutFileTitle( Caption);
         if( Mode == CALC_TOTAL){
            DLayoutStatusMoveVertical();        // vertical arrows only
         } else {
            DLayoutStatusMove();                // display all arrows
         }
         DisplayType( Type, Mode);
         DisplayPage( Group, FirstLine, Type, Mode);
         LastFirstLine = FirstLine;
      }
      GFlush();                        // redraw
      switch( DEventWait()){
         case K_UP :
            if( FirstLine == 0){
               BeepError();
               break;
            }
            BeepKey();
            FirstLine -= PAGE_SIZE;            
            break;

         case K_DOWN :
            if( FirstLine + PAGE_SIZE >= LinesCount){
               BeepError();
               break;
            }
            BeepKey();
            FirstLine += PAGE_SIZE;
            break;

         case K_LEFT :
            if( Mode == CALC_TOTAL){
               break;                  // one statistics only
            }
            if( Type == 0){
               BeepError();
               break;
            }
            BeepKey();
            Type--;
            if( Mode == CALC_LIGHT_HEAVY &&
                Type == STATISTICS_OK){
               Type--;                 // skip OK
            }
            LastFirstLine = -1;        // force redraw
            break;

         case K_RIGHT :
            if( Mode == CALC_TOTAL){
               break;                  // one statistics only
            }
            if( Mode == CALC_BY_SEX){
               if( Type >= STATISTICS_FEMALE){
                  BeepError();
                  break;
               }
            } else { // CALC_LIGHT_(OK)_HEAVY
               if( Type >= STATISTICS_HEAVY){
                  BeepError();
                  break;
               }
            }
            BeepKey();
            Type++;
            if( Mode == CALC_LIGHT_HEAVY &&
                Type == STATISTICS_OK){
               Type++;                 // skip OK
            }
            LastFirstLine = -1;        // force redraw
            break;

         case K_ESC :
            BeepKey();
         case K_TIMEOUT :
            return;
      }     
   }
} // ScreenCompareFiles

//------------------------------------------------------------------------------
//   Lines count
//------------------------------------------------------------------------------

static int GetLinesCount( TGroup Group)
// Returns total nuber of lines
{
TFdirHandle Handle;
int         LinesCount;

   LinesCount = 0;
   // search for files
   FdFindBegin();
   while( (Handle = FdFindNext()) != FDIR_INVALID){
      if( GroupIsMember( Group, Handle)){
         LinesCount++;
      }
      WatchDog();
   }
   return( LinesCount);
} // GetLinesCount

//------------------------------------------------------------------------------
//   Display page
//------------------------------------------------------------------------------

static void DisplayPage( TGroup Group, int FirstLine, TStatisticsType Type, TCalcMode Mode)
// Display page starting at <FirstLine>
{
TFdirHandle Handle;
int         SkipCount;
int         Count;
char        Name[ FDIR_NAME_LENGTH + 1];
TGroup      FileGroup;

   // draw frame
   GSetColor( COLOR_LIGHTGRAY);
   GLine( 0, 39, G_WIDTH, 39);
   GLine( 0, 73, G_WIDTH, 73);
   GSetColor( COLOR_DARKGRAY);
   GBox(  0, 105, G_WIDTH, 33);
   GSetColor( DCOLOR_DEFAULT);
   // draw column icons
   GBitmap( 16,  21, &BmpFilesCount);
   GBitmap( 67,  23, &BmpFilesWeight);
   GBitmap( 115, 27, &BmpFilesSigma);
   GBitmap( 158, 26, &BmpFilesCV);
   GBitmap( 203, 26, &BmpFilesUNI);
   // draw files
   Count     = 0;
   SkipCount = 0;
   FdFindBegin();
   while( (Handle = FdFindNext()) != FDIR_INVALID){
      if( !GroupIsMember( Group, Handle)){
         continue;                     // file is not member
      }
      if( SkipCount < FirstLine){
         SkipCount++;
         continue;                     // skip files up to FirstLine
      }
      if( Count == PAGE_SIZE){
         break;                        // page full
      }
      // draw file data
      FdGetName( Handle, Name);        // get name
      GroupClear( FileGroup);          // empty group
      GroupAdd( FileGroup, Handle);    // add single file to group
      CalcMode( Mode);                 // statistics by mode
      GroupSet( FileGroup);            // set file as working set
      CalcStatistics();                // calculate
      DrawLine( Count, Name, &Calc[ Type]);
      Count++;
      WatchDog();
   }
   // draw total line
   CalcMode( Mode);                // statistics by mode
   GroupSet( Group);               // set whole group as working set
   CalcStatistics();               // calculate
   GSetColor( COLOR_WHITE);
   DrawLine( PAGE_SIZE, StrGet( STR_TOTAL), &Calc[ Type]);
   GSetColor( DCOLOR_DEFAULT);
} // DisplayPage

//------------------------------------------------------------------------------
//   Draw Line
//------------------------------------------------------------------------------

#define DP_ROW_H         16  // row height

static void DrawLine( int Row, const char *Name, TCalc *Statistics)
// Draw single file/total data
{
int RowY;

   RowY = Row * DP_ROW_H * 2 + 40;
   GTextAt( 4,  RowY);
   cputs( Name);                       // file name
   RowY += DP_ROW_H;                   // next line
   // count
   GTextAt( 4, RowY);
   cprintf( "%d", Statistics->Count);
   // average
   GTextAt( 52, RowY);
   DWeightLeft( Statistics->Average);
   // sigma
   GTextAt( 100, RowY);
   DWeightLeft( Statistics->Sigma);
   // variation
   GTextAt( 148, RowY);
   cprintf( "%3.1f", Statistics->Cv);
   // uniformity
   GTextAt( 196, RowY);
   cprintf( "%3.1f", Statistics->Uniformity);
} // DrawLine

//------------------------------------------------------------------------------
//   Type
//------------------------------------------------------------------------------

static void DisplayType( TStatisticsType Type, TCalcMode Mode)
// Display statistics type by <Type> & <Mode>
{
   if( Mode == CALC_TOTAL){
      return;                          // single page only, don't display type
   }
   GSetColor( COLOR_WHITE);
   if( Type == STATISTICS_TOTAL){
      // common for all <Mode>s
      GBitmap( G_WIDTH - 18, 2, &BmpIconTotal);
      GSetColor( DCOLOR_DEFAULT);
      return;
   }
   if( Mode == CALC_BY_SEX){
      // sort by sex
      if( Type == STATISTICS_MALE){
         GBitmap( G_WIDTH - 18, 2, &BmpIconMale);
      } else {
         GBitmap( G_WIDTH - 18, 2, &BmpIconFemale);
      }
      GSetColor( DCOLOR_DEFAULT);
      return;
   }
   // CALC_LIGHT_(OK)_HEAVY sort by weight
   switch( Type){
      case STATISTICS_LIGHT :
         GBitmap( G_WIDTH - 18, 2, &BmpIconLight);
         break;
      case STATISTICS_OK :
         GBitmap( G_WIDTH - 18, 2, &BmpIconOK);
         break;
      case STATISTICS_HEAVY :
         GBitmap( G_WIDTH - 18, 2, &BmpIconHeavy);
         break;
      default :
         break;
   }
   GSetColor( DCOLOR_DEFAULT);
} // DisplayType
