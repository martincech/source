//******************************************************************************
//                                                                            
//   MenuMaintenance.h  Maintenance menu
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __MenuMaintenance_H__
   #define __MenuMaintenance_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuMaintenance( void);
// Maintenance menu

#endif
