//******************************************************************************
//                                                                            
//   MenuWSorting.c    Weighing sorting menu
//   Version 1.0       (c) VymOs
//
//******************************************************************************

#include "MenuWSorting.h"

#include "../Inc/Graphic.h"       // graphic
#include "../Inc/conio.h"         // Display
#include "../Inc/Wgt/DLabel.h"    // Display label

#include "Wgt/DMenu.h"            // Display menu
#include "Wgt/DEdit.h"            // Display input value
#include "Wgt/DWeight.h"          // Display weight

#include "Str.h"                  // Strings
#include "Config.h"               // Configuration
#include "Beep.h"                 // Beep

static DefMenu( SortingMenu)
   STR_MODE,
   STR_LIMIT,
   STR_LOW_LIMIT,
   STR_HIGH_LIMIT,
EndMenu()

typedef enum {
   WS_MODE,
   WS_LIMIT,
   WS_LOW_LIMIT,
   WS_HIGH_LIMIT,
} TSortingMenuEnum;

// Local functions :

static void SortingParameters( int Index, int y, TWeightSorting *Sorting);
// Display weighing sorting parameters

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

void MenuWeightSorting( TWeightSorting *Sorting)
// Edit weight sorting limits
{
TMenuData MData;
int       i;

   DMenuClear( MData);
   forever {
      switch( Sorting->Mode){
         case WEIGHT_SORTING_NONE :
            MData.Mask = (1 << WS_LIMIT) | (1 << WS_HIGH_LIMIT) | (1 << WS_LOW_LIMIT);// mask both limits
            break;
         case WEIGHT_SORTING_LIGHT_HEAVY :
            MData.Mask = (1 << WS_LOW_LIMIT) | (1 << WS_HIGH_LIMIT);                  // mask low/high limits
            break;
         case WEIGHT_SORTING_LIGHT_OK_HEAVY :
            MData.Mask = (1 << WS_LIMIT);                                             // mask single limit
            break;
      }
      // selection :
      if( !DMenu( STR_WEIGHT_SORTING, SortingMenu, (TMenuItemCb *)SortingParameters, Sorting, &MData)){
         return;
      }
      switch( MData.Item){
         case WS_MODE :
            if( !Config.EnableFileParameters){
               BeepError();            // don't touch mode : is global
               break;
            }
            i = Sorting->Mode;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, STR_WEIGHT_SORTING_NONE, _WEIGHT_SORTING_COUNT)){
               break;
            }
            Sorting->Mode = i;
            break;

        case WS_LIMIT :
            i = Sorting->LowLimit;
            if( !DEditWeight( DMENU_EDIT_X, MData.y, &i, 1, Config.Units.Range)){
               break;
            }
            Sorting->LowLimit = i;
            break;

        case WS_LOW_LIMIT :
            i = Sorting->LowLimit;
            if( !DEditWeight( DMENU_EDIT_X, MData.y, &i, 1, Config.Units.Range)){
               break;
            }
            Sorting->LowLimit = i;
            break;

        case WS_HIGH_LIMIT :
            i = Sorting->HighLimit;
            if( !DEditWeight( DMENU_EDIT_X, MData.y, &i, 1, Config.Units.Range)){
               break;
            }
            Sorting->HighLimit = i;
            break;
      }
   }
} // MenuWeightSorting

//******************************************************************************

//------------------------------------------------------------------------------
//  Sorting menu
//------------------------------------------------------------------------------

static void SortingParameters( int Index, int y, TWeightSorting *Sorting)
// Display weighing sorting parameters
{
   switch( Index){
      case WS_MODE :
         DLabelEnum( Sorting->Mode, STR_WEIGHT_SORTING_NONE, DMENU_PARAMETERS_X, y);
         break;

      case WS_LIMIT :
         DWeightWithUnitsNarrow( DMENU_PARAMETERS_X, y, Sorting->LowLimit);
         break;

      case WS_LOW_LIMIT :
         DWeightWithUnitsNarrow( DMENU_PARAMETERS_X, y, Sorting->LowLimit);
         break;

      case WS_HIGH_LIMIT :
         DWeightWithUnitsNarrow( DMENU_PARAMETERS_X, y, Sorting->HighLimit);
         break;
   }
} // LimitParameters
