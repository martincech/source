//******************************************************************************
//                                                                            
//   MenuWUnits.h    Weighing units menu
//   Version 1.0     (c) VymOs
//
//******************************************************************************

#ifndef __MenuWUnits_H__
   #define __MenuWUnits_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuWeighingUnits( void);
// Menu weighing units

#endif
