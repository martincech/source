//*****************************************************************************
//
//    GLogo.c       Graphic EEPROM logo
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#include "GLogo.h"
#include "../Inc/Gpu.h"
#include "../Inc/Graphic.h"
#include "../Inc/Eep.h"
#include "MemoryDef.h"

#if LOGO_WIDTH != GB_WIDTH
   #error "Invalid logo width"
#endif

#if LOGO_HEIGHT / 8 != GB_HEIGHT
   #error "Invalid logo height"
#endif

#if G_PLANES != 2
   #error "Invalid number of planes"
#endif

//-----------------------------------------------------------------------------
// Logo
//-----------------------------------------------------------------------------

void GLogo( void)
// Display logo by EEPROM
{
   GBuffer.MinX = 0;
   GBuffer.MaxX = G_MAX_X;
   GBuffer.MinY = 0;
   GBuffer.MaxY = G_MAX_Y;   
   EepLoadData( offsetof( TEeprom, Logo), GBuffer.Buffer, sizeof( TLogo));
} // GLogo
