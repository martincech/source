
//                                                                            
//   MenuDisplay.c   Display configuration menu
//   Version 1.0     (c) VymOs
//
//******************************************************************************

#include "MenuDisplay.h"
#include "../Inc/Graphic.h"       // graphic
#include "../Inc/conio.h"         // Display
#include "../Inc/Wgt/DLabel.h"    // Display label
#include "Wgt/DMenu.h"            // Display menu
#include "Wgt/DEdit.h"            // Display edit value
#include "Str.h"                  // Strings
#include "MenuBacklight.h"        // Backlight menu
#include "Contrast.h"             // Contrast control

static DefMenu( DisplayMenu)
   STR_BACKLIGHT,
   STR_CONTRAST,
   STR_DISPLAY_MODE,
EndMenu()

typedef enum {
   DM_BACKLIGHT,
   DM_CONTRAST,
   DM_DISPLAY_MODE,
} TDisplayMenuEnum;

// Local functions :

static void DisplayParameters( int Index, int y, void *UserData);
// Display menu parameters

//------------------------------------------------------------------------------
//   Menu
//------------------------------------------------------------------------------

void MenuDisplay( void)
// Display configuration menu
{
TMenuData MData;
int       i;

   DMenuClear( MData);
   forever {
      if( !DMenu( STR_DISPLAY, DisplayMenu, DisplayParameters, 0, &MData)){
         return;
      }
      switch( MData.Item){
         case DM_BACKLIGHT :
            MenuBacklight();
            break;

         case DM_CONTRAST :
            i = Config.Display.Contrast; 
            if( !DEditSpin( DMENU_EDIT_X, MData.y, &i, 0, CONTRAST_MAX, ContrastTest)){
               ContrastSet();          // restore contrast
               break;
            }
            Config.Display.Contrast = i;
            break;

         case DM_DISPLAY_MODE :
            i = Config.Display.Mode;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, STR_DISPLAY_MODE_BASIC, _DISPLAY_MODE_COUNT)){
               break;
            }
            Config.Display.Mode = i;
            break;
      }
   }
} // MenuDisplay

//******************************************************************************

//------------------------------------------------------------------------------
//   Parameters
//------------------------------------------------------------------------------

static void DisplayParameters( int Index, int y, void *UserData)
// Display menu parameters
{
   switch( Index){
      case DM_CONTRAST :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Config.Display.Contrast);
         break;

      case DM_DISPLAY_MODE :
         DLabelEnum( Config.Display.Mode, STR_DISPLAY_MODE_BASIC, DMENU_PARAMETERS_X, y);
         break;
   }
} // DisplayParameters
