//******************************************************************************
//                                                                            
//   MenuSaving.c    Saving parameters menu
//   Version 1.0     (c) VymOs
//
//******************************************************************************

#include "MenuSaving.h"
#include "../Inc/Graphic.h"       // graphic
#include "../Inc/conio.h"         // Display
#include "../Inc/Cpu.h"           // WatchDog
#include "../Inc/File/Fd.h"       // File directory list
#include "../Inc/File/Ds.h"       // Data set
#include "../Inc/Wgt/DLabel.h"    // Display label

#include "Wgt/DMenu.h"            // Display menu
#include "Wgt/DEdit.h"            // Display edit value
#include "Wgt/DMsg.h"             // Display message
#include "Wgt/DDir.h"             // Display file directory
#include "Wgt/DWeight.h"          // Display weight

#include "Str.h"                  // Strings
#include "Sdb.h"                  // Samples database (class only)
#include "MenuWSorting.h"         // Weight sorting menu
#include "Bat1.h"                 // Limits
#include "Weighing.h"             // Weighing

#if FILTER_MAX_AVERAGING < FILTER_MAX
   #error "Averaging constats"
#endif

static DefMenu( SavingParametersMenu)
   STR_MODE,
   STR_WEIGHING_MORE_BIRDS,
   STR_NUMBER_OF_BIRDS,
   STR_WEIGHT_SORTING,
   STR_SORTING,
   STR_FILTER,
   STR_STABILISATION_TIME,
   STR_MINIMUM_WEIGHT,
   STR_STABILISATION_RANGE,
   STR_RESET_DEFAULTS,
   STR_ENABLE_FILE_PARAMETERS,
   STR_COPY_TO_FILE,
EndMenu()

typedef enum {
   SP_MODE,
   SP_WEIGHING_MORE_BIRDS,
   SP_NUMBER_OF_BIRDS,
   SP_WEIGHT_SORTING,
   SP_SORTING,
   SP_FILTER,
   SP_STABILISATION_TIME,
   SP_MINIMUM_WEIGHT,
   SP_STABILISATION_RANGE,
   SP_RESET_DEFAULTS,
   SP_ENABLE_FILE_PARAMETERS,
   SP_COPY_TO_FILE,
} TSavingParametersMenuEnum;

// Local functions :

static void SavingParametersParameters( int Index, int y, TWeighingParameters *Parameters);
// Draw weighing parameters

static void CopyToFile( void);
// Copy global parameters to files

//------------------------------------------------------------------------------
//  Parameters menu
//------------------------------------------------------------------------------

void MenuSavingParameters( TWeighingParameters *Parameters, TYesNo Global)
// Edit weighing parameters. <Global> == NO : file parameters
{
TMenuData MData;
int       i;

   DMenuClear( MData);
   forever {
      MData.Mask = 0;
      if( Parameters->Saving.Mode != SAVING_MODE_AUTOMATIC){
         // some kind of manual mode
         MData.Mask |= (1 << SP_STABILISATION_TIME) | (1 << SP_STABILISATION_RANGE); // hide detection parameters
         if( Parameters->Saving.Mode == SAVING_MODE_MANUAL_BY_SEX){
            MData.Mask |= (1 << SP_WEIGHT_SORTING);         // hide sorting menu
            MData.Mask |= (1 << SP_SORTING);                // disable sorting mode
         }
      }
      if( Global){
         // global weighing parameters
         MData.Mask |= (1 << SP_NUMBER_OF_BIRDS) | (1 << SP_WEIGHT_SORTING) |  (1 << SP_RESET_DEFAULTS);
         if( !Config.EnableFileParameters){
            MData.Mask |= (1 << SP_COPY_TO_FILE);           // global parameters - disable copy
         }
      } else {
         // file specific weighing parameters
         MData.Mask |= (1 << SP_SORTING) | (1 << SP_ENABLE_FILE_PARAMETERS) | (1 << SP_COPY_TO_FILE);
         if( !Parameters->EnableMoreBirds){
            MData.Mask |= (1 << SP_NUMBER_OF_BIRDS);       // hide number of birds
         }
      }
      // selection :
      if( !DMenu( STR_SAVING, SavingParametersMenu, (TMenuItemCb *)SavingParametersParameters, Parameters, &MData)){
         return;
      }
      switch( MData.Item){
         case SP_MODE :
            i = Parameters->Saving.Mode;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, STR_SAVING_MODE_AUTOMATIC, _SAVING_MODE_COUNT)){
               break;
            }
            Parameters->Saving.Mode = i;
            break;

         case SP_WEIGHING_MORE_BIRDS :
            i = Parameters->EnableMoreBirds; 
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, STR_NO, 2)){
               break;
            }
            Parameters->EnableMoreBirds = i;
            break;

         case SP_NUMBER_OF_BIRDS :
            i = Parameters->NumberOfBirds;
            if( !DEditSpin( DMENU_EDIT_X, MData.y, &i, 1, NUMBER_OF_BIRDS_MAX, 0)){
               break;
            }
            Parameters->NumberOfBirds = i;
            break;

         case SP_WEIGHT_SORTING :
            MenuWeightSorting( &Parameters->WeightSorting);
            break;

         case SP_SORTING :
            i = Config.WeighingParameters.WeightSorting.Mode;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, STR_WEIGHT_SORTING_NONE, _WEIGHT_SORTING_COUNT)){
               break;
            }
            Config.WeighingParameters.WeightSorting.Mode = i;
            ConfigSaveSection( WeighingParameters.WeightSorting);
            break;

         case SP_FILTER :
            i = Parameters->Saving.Filter;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 1, 1, FILTER_MAX, STR_SECONDS)){
               break;
            }
            Parameters->Saving.Filter = i;
            break;

        case SP_MINIMUM_WEIGHT :
            i = Parameters->Saving.MinimumWeight;
            if( !DEditWeight( DMENU_EDIT_X, MData.y, &i, 1, Config.Units.Range)){
               break;
            }
            Parameters->Saving.MinimumWeight = i;
            break;

        case SP_STABILISATION_TIME :
            i = Parameters->Saving.StabilisationTime;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 1, 1, STABILISATION_TIME_MAX, STR_SECONDS)){
               break;
            }
            Parameters->Saving.StabilisationTime = i;
            break;

        case SP_STABILISATION_RANGE :
            i = Parameters->Saving.StabilisationRange;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 1, 1, STABILISATION_RANGE_MAX, "%")){
               break;
            }
            Parameters->Saving.StabilisationRange = i;
            break;

         case SP_RESET_DEFAULTS :
            if( !DMsgYesNo( STR_RESET_DEFAULTS, STR_REALLY_RESET_DEFAULTS, 0)){
               break;
            }
            *Parameters = Config.WeighingParameters; // default parameters
            break;

         case SP_ENABLE_FILE_PARAMETERS :
            i = Config.EnableFileParameters; 
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, STR_NO, 2)){
               break;
            }
            Config.EnableFileParameters = i;
            ConfigSaveSection( EnableFileParameters);
            break;

         case SP_COPY_TO_FILE :
            CopyToFile();
            break;
      }
   }
} // MenuParameters

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void SavingParametersParameters( int Index, int y, TWeighingParameters *Parameters)
// Draw weighing parameters
{
   switch( Index){
      case SP_MODE :
         DLabelEnum( Parameters->Saving.Mode, STR_SAVING_MODE_AUTOMATIC, DMENU_PARAMETERS_X, y);
         break;

      case SP_NUMBER_OF_BIRDS :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->NumberOfBirds);
         break;

      case SP_WEIGHING_MORE_BIRDS :
         DLabelEnum( Parameters->EnableMoreBirds, STR_NO, DMENU_PARAMETERS_X, y);
         break;

      case SP_WEIGHT_SORTING :
         DLabelEnum( Parameters->WeightSorting.Mode, STR_WEIGHT_SORTING_NONE, DMENU_PARAMETERS_X, y);
         break;

      case SP_SORTING :
         DLabelEnum( Config.WeighingParameters.WeightSorting.Mode, STR_WEIGHT_SORTING_NONE, DMENU_PARAMETERS_X, y);
         break;

      case SP_FILTER :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%3.1f %s", Parameters->Saving.Filter, STR_SECONDS);
         break;

      case SP_MINIMUM_WEIGHT :
         DWeightWithUnitsNarrow( DMENU_PARAMETERS_X, y, Parameters->Saving.MinimumWeight);
         break;

      case SP_STABILISATION_TIME :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%3.1f %s", Parameters->Saving.StabilisationTime, STR_SECONDS);
         break;

      case SP_STABILISATION_RANGE :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%3.1f %%", Parameters->Saving.StabilisationRange);
         break;

      case SP_ENABLE_FILE_PARAMETERS :
         DLabelEnum( Config.EnableFileParameters, STR_NO, DMENU_PARAMETERS_X, y);
         break;
   }
} // SavingParametersParameters

//------------------------------------------------------------------------------
//  Copy to files
//------------------------------------------------------------------------------

static void CopyToFile( void)
// Copy global parameters to files
{
TFDataSet   DataSet;
TFdirHandle Handle;

   DsClear( DataSet);                  // empty file set
   FdSetClass( SDB_CLASS);             // samples database directory class
   if( !DDirSelectSet( STR_SELECT_FILES, DataSet)){    // edit file set
      return;
   }
   if( DsIsEmpty( DataSet)){
      DMsgOk( STR_ERROR, STR_NO_FILES_SELECTED, 0);
      return;                          // no selection made
   }
   if( !DMsgYesNo( STR_COPY_TO_FILE, STR_REALLY_COPY_TO_FILES, 0)){
      return;                          // don't copy
   }
   FdFindBegin();
   while( (Handle = FdFindNext()) != FDIR_INVALID){
      if( !DsContains( DataSet, Handle)){
         continue;                     // file not selected
      }
      SdbOpen( Handle, NO);
      WeighingCopyParameters();
      SdbClose();
      WatchDog();
   }
} // CopyToFiles
