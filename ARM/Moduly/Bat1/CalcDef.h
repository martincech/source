//*****************************************************************************
//
//    CalcDef.h    Statistic calculations record definition
//    Version 1.0  (c) VymOs & P.Veit
//
//*****************************************************************************

#ifndef __CalcDef_H__
   #define __CalcDef_H__

#ifndef __Uni_H__
   #include "../Inc/Uni.h"
#endif

#ifndef __WeightDef_H__
   #include "WeightDef.h"              // weight definitions
#endif

#ifndef __PWeighingDef_H__
   #include "PWeighingDef.h"           // Statistic parameters
#endif

#ifndef __Histogram_H__
   #include "../Inc/Histogram.h"
#endif

//-----------------------------------------------------------------------------
// Data
//-----------------------------------------------------------------------------

// Calculation results :

typedef struct {
   TStatCount Count;             // Samples count
   TWeight    Average;           // Average weight
   TWeight    MinWeight;         // Minimal weight
   TWeight    MaxWeight;         // Maximal weight
   TWeight    Sigma;             // Sigma weight
   word       Cv;                // CV [0.1%]
   word       Uniformity;        // Uniformity [0.1%]
   THistogram Histogram;         // Histogram
} TCalc;

#endif
