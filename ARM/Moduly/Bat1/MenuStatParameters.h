//******************************************************************************
//                                                                            
//   MenuStatParameters.h  Statistic Parameters menu
//   Version 1.0           (c) VymOs
//
//******************************************************************************

#ifndef __MenuStatistic_H__
   #define __MenuStatistic_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __PStatisticDef_H__
   #include "PStatisticDef.h"
#endif

void MenuStatisticParameters( TStatisticParameters *Statistic);
// Edit statistic parameters

#endif
