//******************************************************************************
//
//   Sdb.c        Samples database
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifdef __WIN32__
   // Borland only
   #include <vcl.h>
   #pragma hdrstop
#endif

#include "Sdb.h"
#include "../Inc/File/Db.h"
#include <stddef.h>

static TSdbConfig _Config;             // Working configuration
static TYesNo     _SaveConfig;         // Save configuration on close

//------------------------------------------------------------------------------
// Create
//------------------------------------------------------------------------------

TYesNo SdbCreate( const char *Name)
// Create new database
{
   _SaveConfig = YES;
   return( DbCreate( Name, SDB_CLASS, &_Config));
} // SdbCreate

//------------------------------------------------------------------------------
// Open
//------------------------------------------------------------------------------

void SdbOpen( TFdirHandle Handle, TYesNo ReadOnly)
// Open database
{
   _SaveConfig = !ReadOnly;
   DbOpen( Handle, ReadOnly);
   DbLoadConfig( &_Config);
} // SdbOpen

//------------------------------------------------------------------------------
// Close
//------------------------------------------------------------------------------

void SdbClose( void)
// Close current database
{
   if( _SaveConfig){
      DbSaveConfig( &_Config);
   }
   DbClose();
} // SdbClose

//------------------------------------------------------------------------------
// Config
//------------------------------------------------------------------------------

TSdbConfig *SdbConfig( void)
// Returns working configuration
{
   return( &_Config);
} // SdbConfig

//------------------------------------------------------------------------------
// Count
//------------------------------------------------------------------------------

int SdbFileSize( int Size)
// Returns samples count by file <Size> bytes
{
   return( (Size - sizeof( TSdbConfig)) / sizeof( TSdbRecord));
} // SdbFileSize
