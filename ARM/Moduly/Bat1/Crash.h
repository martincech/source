//******************************************************************************
//
//   Crash.h      Crash record utility
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __Crash_H__
   #define __Crash_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void CrashException( int Type, dword Address);
// Record exception

void CrashWatchDog( void);
// Record watchdog activity

#endif
