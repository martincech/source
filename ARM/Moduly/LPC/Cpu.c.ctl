//*****************************************************************************
//
//    Cpu.c        Philips LPC CPU functions
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "../../inc/System.h"
#include "../../inc/Cpu.h"
#include <Hardware.h>
#include <ctl_api.h>

// Local functions :

static void __irq NonvectoredIrqHandler( void);
// Non-vectored IRQ handler

//------------------------------------------------------------------------------
//   CPU initialisation
//------------------------------------------------------------------------------

void CpuInit( void)
// Cpu startup initialisation
{
   // vectored interrupt controller :  
   VICDefVectAddr = (unsigned)NonvectoredIrqHandler; // non-vectored handler
} // CpuInit

//------------------------------------------------------------------------------
//   CPU initialisation
//------------------------------------------------------------------------------

void CpuInitIap( void)
// Cpu IAP initialisation
{
   // memory mapping :
   SCB_MEMMAP = MEMMAP_FLASH;          // map vectors to flash
   // memory accelerator :
   MAM_MAMCR  = MAMCR_DISABLED;        // disable cache
   // PLL unit :
   SCB_PLLCON  = 0;                    // disable PLL
   SCB_PLLFEED = PLLFEED_1;            // launch sequence
   SCB_PLLFEED = PLLFEED_2;    
   // peripheral bus clock :
   SCB_VPBDIV  = VPBDIV_QUARTER;       // default is 1/4
} // CpuInitIap

//------------------------------------------------------------------------------
//   IRQ
//------------------------------------------------------------------------------

void CpuIrqAttach( native irq, native channel, TIrqHandler *handler)
// Install IRQ handler
{
   ctl_set_isr( channel, irq, CTL_ISR_TRIGGER_FIXED, handler, 0);
   CpuIrqEnable( channel);                           // enable input
} // CpuIrqAttach

//------------------------------------------------------------------------------
//   IRQ Enable
//------------------------------------------------------------------------------

void CpuIrqEnable( native channel)
// Enable IRQ on <channel>. Must not be used inside interrupt handler
{
   ctl_unmask_isr( channel);
} // CpuIrqEnable

//------------------------------------------------------------------------------
//   IRQ Disable
//------------------------------------------------------------------------------

void CpuIrqDisable( native channel)
// Disable IRQ on <channel>
{  
   ctl_unmask_isr( channel);
} // CpuIrqDisable

//------------------------------------------------------------------------------
//   IRQ Done
//------------------------------------------------------------------------------

void CpuIrqDone( void)
// Last operation in the interrupt handler
{
} // CpuIrqDone

//------------------------------------------------------------------------------
//   Enable iterrupts
//------------------------------------------------------------------------------

void EnableInts( void)
// Enable interrupts
{
  ctl_global_interrupts_enable();
} // EnableInts

//------------------------------------------------------------------------------
//   Disable iterrupts
//------------------------------------------------------------------------------

void DisableInts( void)
// Disable interrupts
{
  ctl_global_interrupts_disable();
} // DisableInts

//------------------------------------------------------------------------------
//   Watchdog
//------------------------------------------------------------------------------

void StartWatchDog( void)
// Start watchdog
{
   WD_WDTC  = WATCHDOG_INTERVAL * (FBUS / 1000) / 4;
   WD_WDMOD = WDMOD_ENABLE | WDMOD_RESET;
   WatchDog();                         // initial feed sequence
} // StartWatchdog


void WatchDog( void)
// Refresh watchdog
{
   DisableInts();
   WD_WDFEED = WDFEED_1;
   WD_WDFEED = WDFEED_2;
   EnableInts();
} // WatchDog

//------------------------------------------------------------------------------
//   Delay ms
//------------------------------------------------------------------------------

#define LOOP_CYCLES   4
#define MS_COUNT      FCPU / 1000 / LOOP_CYCLES

#define EmptyLoop( ms)  asm volatile (             \
		                     "L_LOOP_%=: 		\n\t" \
		                     "subs	%0, %0, #1 	\n\t" \
		                     "bne	L_LOOP_%=	\n\t" \
		                  :  /* no outputs */ : "r" (ms));

void SysDelay( native ms)
// Delay <ms>
{
native i;

   do {
      i = MS_COUNT;
      EmptyLoop( i);
   } while( --ms);
} // SysDelay

//------------------------------------------------------------------------------
//   Delay us
//------------------------------------------------------------------------------

void SysUDelay( native us)
// Delay <us>
{
native i;

   i  = FCPU / 100;
   i *= (us + 1);
   i /= 10000 * LOOP_CYCLES;
   EmptyLoop( i);
} // SysUDelay

//------------------------------------------------------------------------------
//   Non-vectored interrupt
//------------------------------------------------------------------------------

static void __irq NonvectoredIrqHandler( void)
// Non-vectored IRQ handler
{
   // no functionality
} // NonvectoredIrqHandler

