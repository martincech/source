//******************************************************************************
//                                                                            
//  SysClock.c     System timer & clock services                                                                 
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../../inc/System.h"
#include "../../inc/Rtc.h"
#include "../../inc/Dt.h"
#include "../../inc/Cpu.h"

#ifdef TIMER_SOUND
   #include "../../inc/Sound.h"
#endif
#ifdef TIMER_KBD
   #include "../../inc/Kbd.h"
#endif

#define TIMER_IR      T0_IR            // interrupt flag register
#define TIMER_TCR     T0_TCR           // timer control register
#define TIMER_CTCR    T0_CTCR          // counter/timer control register
#define TIMER_PR      T0_PR            // prescaler 
#define TIMER_MCR     T0_MCR           // match control register
#define TIMER_VIC     VIC_TIMER0       // VIC channel

#define TIMER_CHANNEL 0                // match channel
#define TIMER_MR      T0_MR0           // match value register

#define TIMER_INCREMENT ((TIMER_PERIOD * FBUS) / 1000 - 1)

#define SetTimer1s()      Counter1s      = 1000 / TIMER_PERIOD
#define SetTimerFlash()   CounterFlash   = (TIMER_FLASH1 + TIMER_FLASH2) / TIMER_PERIOD
#define SetTimerTimeout() CounterTimeout =  TIMER_TIMEOUT;

// timer flags :

#define TIMER_FLAG_1S      0x01
#define TIMER_FLAG_FLASH1  0x02
#define TIMER_FLAG_FLASH2  0x04
#define TIMER_FLAG_TIMEOUT 0x08

#define Set1s()      Flags |=  TIMER_FLAG_1S
#define SetFlash1()  Flags |=  TIMER_FLAG_FLASH1
#define SetFlash2()  Flags |=  TIMER_FLAG_FLASH2
#define SetTimeout() Flags |=  TIMER_FLAG_TIMEOUT

#define Clr1s()      Flags &= ~TIMER_FLAG_1S
#define ClrFlash1()  Flags &= ~TIMER_FLAG_FLASH1
#define ClrFlash2()  Flags &= ~TIMER_FLAG_FLASH2
#define ClrTimeout() Flags &= ~TIMER_FLAG_TIMEOUT

#define Is1s()      (Flags & TIMER_FLAG_1S)
#define IsFlash1()  (Flags & TIMER_FLAG_FLASH1)
#define IsFlash2()  (Flags & TIMER_FLAG_FLASH2)
#define IsTimeout() (Flags & TIMER_FLAG_TIMEOUT)

// Local variables :

static volatile dword      _TimerTick;           // milisecond timer
static volatile TTimestamp _Clock;               // system clock
static volatile word       Counter1s;            // 1 s countdown
static volatile word       CounterFlash;         // flash countdown
static volatile byte       Flags;                // timer flags
#ifndef TIMER_NO_TIMEOUT
static volatile byte       CounterTimeout;       // timeout countdown
#endif

// Local functions :

static void __irq SysTimerHandler( void);
// Zpracovani preruseni od casovace

//------------------------------------------------------------------------------
//  Wait Event
//------------------------------------------------------------------------------

int SysWaitEvent( void) 
// Wait for event
{
int Key;

   forever {
      Key = SysYield();
      if( Key != K_IDLE) {
          return( Key);                // nonempty event
      }
   }
} // SysWaitEvent

//------------------------------------------------------------------------------
//   Start timer
//------------------------------------------------------------------------------

void SysStartTimer( void)
// Start system timer
{
   _Clock = RtcLoad();                 // Get RTC date/time
   SetTimer1s();                       // set 1 s countdown
   SetTimerFlash();                    // set flash countdown
#ifndef TIMER_NO_TIMEOUT
   SetTimerTimeout();                  // set timeout countdown
#endif
   Flags = 0;                          // clear all flags
   // initialize timer :
   CpuIrqAttach( SYS_TIMER_IRQ, TIMER_VIC, SysTimerHandler);
   TIMER_PR    = 0;                    // Count every PCLK edge
   TIMER_MCR  |= MCR_INTERRUPT( TIMER_CHANNEL) | 
                 MCR_RESET( TIMER_CHANNEL);  // enable match interrupt & reset
   TIMER_MR    = TIMER_INCREMENT;      // match register to interval
   TIMER_TCR   = TCR_RESET;            // Reset timer
   TIMER_TCR   = TCR_ENABLE;           // Run timer
} // SysStartTimer

//------------------------------------------------------------------------------
//   Timer handler
//------------------------------------------------------------------------------

static void __irq SysTimerHandler( void)
// Timer interrupt handler
{
   TIMER_IR    = IR_MR( TIMER_CHANNEL);// clear flag
   _TimerTick += TIMER_PERIOD;         // system timer
   // second timer :
   Counter1s--;
   if( !Counter1s){             
      SetTimer1s();                    // count again
      Set1s();                         // set flag
      _Clock++;                        // actual time
#ifndef TIMER_NO_TIMEOUT
      if( CounterTimeout){
         if( !(--CounterTimeout)){
            SetTimerTimeout();         // count again
            SetTimeout();              // set flag
         }                                
      }
#endif
   }                                
   // flash timer :
   CounterFlash--;
   if( CounterFlash == TIMER_FLASH1){
      SetFlash1();                     // flash 1 reached, set flag
   }
   if( !CounterFlash){             
      SetTimerFlash();                 // count again
      SetFlash2();                     // set flag
   }                                   
   // sound trigger :
   #ifdef TIMER_SOUND
      SndTrigger();
   #endif
   // keyboard trigger :
   #ifdef TIMER_KBD
      KbdTrigger();
   #endif
   // external events :
   #ifdef TIMER_EXTERNAL
      SysTimerExecute();               // user actions
   #endif
   CpuIrqDone();
} // SysTimerHandler

#ifndef TIMER_NO_TIMEOUT
//------------------------------------------------------------------------------
//   Inactivity Timeout
//------------------------------------------------------------------------------

void SysDisableTimeout( void)
// Disable timeout
{
   DisableInts();
   CounterTimeout = 0;       // don't interrupt
   ClrTimeout();
   EnableInts();
} // SysDisableTimeout

void SysEnableTimeout( void)
// Enable timeout
{
   DisableInts();
   SetTimerTimeout();
   ClrTimeout();
   EnableInts();
} // SysEnableTimeout

void SysResetTimeout( void)
// Reset timeout
{
   DisableInts();
   if( CounterTimeout){
      // is enabled
      SetTimerTimeout();
   }
   EnableInts();
} // SysResetTimeout

TYesNo SysIsTimeout( void)
// Returns YES if timeout occured
{
TYesNo Is;

   DisableInts();
   Is = IsTimeout();                   // read flag
   ClrTimeout();                       // clear flag
   EnableInts();
   return( Is);
} // SysIsTimeout;
#endif // TIMER_NO_TIMEOUT

//------------------------------------------------------------------------------
//   Clock
//------------------------------------------------------------------------------

void SysSetClock( TTimestamp Now)
// Set system time to <Now>
{
   DisableInts();
   _Clock = Now;
   EnableInts();
   RtcSave( Now);
} // SysSetClock

TTimestamp SysGetClock( void)
// Returns system date/time
{
TTimestamp Now;

   DisableInts();
   Now = _Clock;
   EnableInts();
   return( Now);
} // SysGetClock

TTimestamp SysGetClockNaked( void)
// Returns system date/time from interrupt
{
   return( _Clock);
} // SysGetClockNaked

TYesNo Sys1sExpired( void)
// Returns YES after 1s expiration
{
TYesNo Is;

   DisableInts();
   Is = Is1s();                        // read flag
   Clr1s();                            // clear flag
   EnableInts();
   return( Is);
} // Sys1sExpired

//------------------------------------------------------------------------------
//   Flash timer
//------------------------------------------------------------------------------

TYesNo SysIsFlash1( void)
// Returns YES if flash 1 event occured
{
TYesNo Is;

   DisableInts();
   Is = IsFlash1();                    // read flag
   ClrFlash1();                        // clear flag
   EnableInts();
   return( Is);
} // SysIsFlash1

TYesNo SysIsFlash2( void)
// Returns YES if flash 2 event occured
{
TYesNo Is;

   DisableInts();
   Is = IsFlash2();                    // read flag
   ClrFlash2();                        // clear flag
   EnableInts();
   return( Is);
} // SysIsFlash1

void SysResetFlash( void)
// Reset flash events and start again
{
   DisableInts();
   ClrFlash1();                        // clear flag
   ClrFlash2();                        // clear flag
   SetTimerFlash();                    // set flash countdown
   EnableInts();
} // SysResetFlash

//------------------------------------------------------------------------------
//   Timer
//------------------------------------------------------------------------------

dword SysTime( void)
// Returns milisecond timer
{
   return( _TimerTick);
} // SysTime
