//*****************************************************************************
//
//    Sound.c      Sound creation
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "../../inc/Sound.h"
#include "../../inc/System.h"
#include "../../inc/Cpu.h"

#if   SOUND_CHANNEL == 1
   // P0.0
   #define PINSEL        PCB_PINSEL0        
   #define PINSEL_MASK   (3 << 0)                // PINSEL0, P0.0 dibit
   #define PINSEL_PWM    (2 << 0)                // PINSEL0 function 10 on P0.0
   #define SOUND_PIN      0
   #define PWM_MR        PWM_MR1
#elif SOUND_CHANNEL == 2
   // P0.7
   #define PINSEL        PCB_PINSEL0        
   #define PINSEL_MASK   (3 << 14)               // PINSEL0, P0.7 dibit
   #define PINSEL_PWM    (2 << 14)               // PINSEL0 function 10 on P0.7
   #define SOUND_PIN      7
   #define PWM_MR        PWM_MR2
#elif SOUND_CHANNEL == 3
   // P0.1
   #define PINSEL        PCB_PINSEL0        
   #define PINSEL_MASK   (3 <<  2)               // PINSEL0, P0.1 dibit
   #define PINSEL_PWM    (2 <<  2)               // PINSEL0 function 10 on P0.1
   #define SOUND_PIN      1
   #define PWM_MR        PWM_MR3
#elif SOUND_CHANNEL == 4
   // P0.8
   #define PINSEL        PCB_PINSEL0        
   #define PINSEL_MASK   (3 << 16)               // PINSEL0, P0.8 dibit
   #define PINSEL_PWM    (2 << 16)               // PINSEL0 function 10 on P0.8
   #define SOUND_PIN      8
   #define PWM_MR        PWM_MR4
#elif SOUND_CHANNEL == 5
   // P0.21
   #define PINSEL        PCB_PINSEL1             // index : 21 - 16 = 5
   #define PINSEL_MASK   (3 << 10)               // PINSEL1, P0.21 dibit
   #define PINSEL_PWM    (1 << 10)               // PINSEL1 function 01 on P0.21
   #define SOUND_PIN     21
   #define PWM_MR        PWM_MR5
#elif SOUND_CHANNEL == 6
   // P0.9
   #define PINSEL        PCB_PINSEL0        
   #define PINSEL_MASK   (3 << 18)               // PINSEL0, P0.9 dibit
   #define PINSEL_PWM    (2 << 18)               // PINSEL0 function 10 on P0.9
   #define SOUND_PIN      9
   #define PWM_MR        PWM_MR6
#else
   #error "Invalid SOUND_CHANNEL"
#endif

volatile short _snd_duration;       // citac delky

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void SndInit( void)
// Inicializace modulu
{
   PINSEL = (PINSEL & ~PINSEL_MASK) | PINSEL_PWM;// select pin fuction
   PWM_PR  = 0;                                  // prescaler
   PWM_TCR = PWMTCR_RESET;                       // reset
   PWM_MCR = PWMMCR_RESET0;                      // nulovani od PWM0
   PWM_PCR = PWMPCR_PWMENA( SOUND_CHANNEL);      // povoleni PWM vystupu
} // SndInit

//-----------------------------------------------------------------------------
// Pipnuti
//-----------------------------------------------------------------------------

void SndBeepRun( int count, int v, short duration)
// Asynchronne pipne periodou <count>, hlasitosti <v>, trvani <duration> milisekund
{
   _snd_duration = (duration) / TIMER_PERIOD; 
   SndSoundRun( count, v);
}  // SndBeepRun

//-----------------------------------------------------------------------------
// Synchronni pipnuti
//-----------------------------------------------------------------------------

void SndSBeepRun( int count, int v, short duration) 
// Synchronne pipne periodou <count>, hlasitosti <v>, trvani <duration> milisekund
{
   SndSoundRun( count, v); 
   SysDelay( duration); 
   SndNosound(); 
   SysDelay(10);
} // SndSBeep   

//-----------------------------------------------------------------------------
// Stop
//-----------------------------------------------------------------------------

void SndNosound( void)
// Zastavi zvuk
{
   PWM_TCR = PWMTCR_RESET;             // reset citace
} // SndNosound

//-----------------------------------------------------------------------------
// Run
//-----------------------------------------------------------------------------

void SndSoundRun( int count, int volume)
// Spusti zvuk s periodou <count> a  hlasitosti <volume>
{
int width;

   if( volume == 0){
      PWM_TCR = PWMTCR_RESET;          // zastavit zvuk
      return;
   }
   volume = VOL_MAX - volume;          // hlasitost na utlum
   // sirka pulsu :
   width    = 50000;                   // min utlum - perioda 50% je 50000
   width  >>= (volume >> 1);           // sudy utlum je 6dB (1/2)
   // normalizujeme na 50% == 10000 :
   if( volume & 0x01){
      width /= 7;                      // lichy utlum pridej 3dB (1/V2) + odstraneni petinasobku
   } else {
      width /= 5;                      // sudy utlum je 0dB + odstraneni petinasobku
   }
   PWM_MR0 = count;                                             // perioda
   PWM_MR  = (count * width) / 20000;                           // sirka (width 10 000 == 1/2)
   PWM_LER = PWMLER_ENABLE0 | PWMLER_ENABLE( SOUND_CHANNEL);    // povoleni latch
   PWM_TCR = PWMTCR_ENABLE  | PWMTCR_PWM_ENABLE;                // povoleni citani a PWM
} // SndSoundRun
