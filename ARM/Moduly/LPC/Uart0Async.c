//*****************************************************************************
//
//    Uart0Async.c - RS232 communication services
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#include "../../inc/Uart0.h"
#include "../../inc/Cpu.h"

#if   defined( __LPC210x__) || defined( __LPC213x__)
   // P0.0 - TxD, P0.1 - RxD
   #define PINSEL        PCB_PINSEL0
   #define PINSEL_MASK   ((3 << 0) | (3 << 2))   // PINSEL0, P0.0, P0.1 dibits
   #define PINSEL_UART   ((1 << 0) | (1 << 2))   // PINSEL0 function 01 on P0.0, P0.1

   #define GPIO_UART       (GPIO0_IODIR)
   #define GPIO_UART_MASK  ((1 << 0) | (1 << 1))
#else
   #error "Unknown processor model"
#endif

// parametry :

#define UART_RX_TIMEOUT UART0_RX_TIMEOUT
#define UART_ISIZE      UART0_RX_BUFFER_SIZE
#define UART_OSIZE      UART0_TX_BUFFER_SIZE
#ifdef UART0_MULVAL
   #define UART_MULVAL     UART0_MULVAL
   #define UART_DIVADDVAL  UART0_DIVADDVAL
#endif
#ifdef UART0_BREAK_DETECTION
   #define UART_BREAK_DETECTION
#endif

// funkce :

#define UARTInit       Uart0Init
#define UARTDisconnect Uart0Disconnect
#define UARTSetup      Uart0Setup
#define UARTTxBusy     Uart0TxBusy
#define UARTTxChar     Uart0TxChar
#define UARTRxChar     Uart0RxChar
#define UARTRxWait     Uart0RxWait
#define UARTFlushChars Uart0FlushChars
#define UARTIsBreak    Uart0IsBreak

// porty :

#define UART_RBR  UART0_RBR
#define UART_THR  UART0_THR
#define UART_IER  UART0_IER
#define UART_IIR  UART0_IIR
#define UART_FCR  UART0_FCR
#define UART_LCR  UART0_LCR
#define UART_LSR  UART0_LSR
#define UART_DLL  UART0_DLL
#define UART_DLM  UART0_DLM
#define UART_FDR  UART0_FDR

// preruseni :

#define VIC_UART  VIC_UART0
#define UART_IRQ  UART0_IRQ

// sablona modulu :

#include "UartAsync.c"
