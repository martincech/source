//*****************************************************************************
//
//    Iap.c        In application programming services
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "../../inc/Iap.h"
#include "../../inc/Cpu.h"
#include "../../inc/System.h"

#define COMMAND_SIZE 5                 // max. number of IAP command dwords
#define REPLY_SIZE   2                 // max. number of IAP reply dwords
#define IAP_ENTRY    0x7FFFFFF1        // IAP entry point address

// IAP commands :
#define CMD_PREPARE_WRITE       50     // prepare write/erase
#define CMD_COPY_RAM_TO_FLASH   51     // copy RAM to Flash
#define CMD_ERASE_SECTORS       52     // erase sector(s)
#define CMD_BLANK_CHECK         53     // blank check sectors
#define CMD_READ_PART_ID        54     // read part identification number
#define CMD_BOOTCODE_VERSION    55     // read bootcode version
#define CMD_COMPARE             56     // compare memory areas
#define CMD_REINVOKE_ISP        57     // reinvoke ISP

// IAP status :
typedef enum {
   CMD_SUCCESS         = 0,
   INVALID_COMMAND     = 1,           
   SRC_ADDR_ERROR      = 2, 
   DST_ADDR_ERROR      = 3, 
   SRC_ADDR_NOT_MAPPED = 4,
   DST_ADDR_NOT_MAPPED = 5,
   COUNT_ERROR         = 6,
   INVALID_SECTOR      = 7,
   SECTOR_NOT_BLANK    = 8,
   SECTOR_NOT_PREPARED = 9,
   COMPARE_ERROR       = 10,
   BUSY                = 11
} TIapErrors;   
   
// Sector calculation :
#define GetSector( Address)    ((dword)(Address) >> 13)

static TYesNo Prepare( dword FromSector, dword ToSector);
// Prepare sector for write/erase
 
static void CallIap( dword *Input, dword *Output);
// Call IAP entry point

//------------------------------------------------------------------------------
//  Setup
//------------------------------------------------------------------------------

void IapSetup( void)
// Prepare CPU for IAP
{
   DisableInts();
   CpuInitIap();
} // IapSetup

//------------------------------------------------------------------------------
//  Cleanup
//------------------------------------------------------------------------------

void IapCleanup( void)
// Cleanup CPU after IAP
{
   CpuInit();
   EnableInts();
   SysRestartTimer();        // long interrupt disable, restart system timer
} // IapCleanup

//------------------------------------------------------------------------------
//  Erase
//------------------------------------------------------------------------------

TYesNo IapErase( void *From, void *To)
// Erase flash <From> address <To> (inclusive) address
// If <To> == 0, erases sector containing <From> address
{
dword cmd[ COMMAND_SIZE];
dword FromSector, ToSector;

   FromSector  = GetSector( From);
   if( !To){
      ToSector = FromSector;
   } else {
      ToSector = GetSector( To);
   }
   if( !Prepare( FromSector, ToSector)){
      return( NO);
   }
   cmd[ 0] = CMD_ERASE_SECTORS;
   cmd[ 1] = FromSector;
   cmd[ 2] = ToSector;
   cmd[ 3] = FXTAL / 1000;
   CallIap( cmd, cmd);
   if( cmd[ 0] != CMD_SUCCESS){
      return( NO);
   }
   return( YES);   
} // IapErase

//------------------------------------------------------------------------------
//  Write
//------------------------------------------------------------------------------

TYesNo IapWrite( void *Destination, void *Source, native Size)
// Write <Source> data into Flash address <Destination> in <Size> length
// Warning : <Destination> align and <Size> depend on CPU model
{
dword cmd[ COMMAND_SIZE];
dword sector;

   sector = GetSector( Destination);
   if( !Prepare( sector, sector)){
      return( NO);
   }
   cmd[ 0] = CMD_COPY_RAM_TO_FLASH;
   cmd[ 1] = (dword)Destination;
   cmd[ 2] = (dword)Source;
   cmd[ 3] = Size;
   cmd[ 4] = FXTAL / 1000;
   CallIap( cmd, cmd);
   if( cmd[ 0] != CMD_SUCCESS){
      return( NO);
   }
   return( YES);   
} // IapWrite

//------------------------------------------------------------------------------
//  Compare
//------------------------------------------------------------------------------

TYesNo IapCompare( void *Destination, void *Source, native Size)
// Compare memory area <Destination>/<Source> in <Size> length
{
dword cmd[ COMMAND_SIZE];

   cmd[ 0] = CMD_COMPARE;
   cmd[ 1] = (dword)Destination;
   cmd[ 2] = (dword)Source;
   cmd[ 3] = Size;
   CallIap( cmd, cmd);
   if( cmd[ 0] != CMD_SUCCESS){
      return( NO);
   }
   return( YES);   
} // IapCompare

//------------------------------------------------------------------------------
//  Prepare write
//------------------------------------------------------------------------------

static TYesNo Prepare( dword FromSector, dword ToSector)
// Prepare sector for write/erase
{
dword cmd[ COMMAND_SIZE];

   cmd[ 0] = CMD_PREPARE_WRITE;
   cmd[ 1] = FromSector;
   cmd[ 2] = ToSector;
   CallIap( cmd, cmd);
   if( cmd[ 0] != CMD_SUCCESS){
      return( NO);
   }
   return( YES);
} // Prepare

//------------------------------------------------------------------------------
//  IAP entry
//------------------------------------------------------------------------------

typedef void (*TIap)( dword *, dword *);     // IAP entry point address

static void CallIap( dword *Input, dword *Output)
// Call IAP entry point
{
   TIap iap_entry = (TIap)IAP_ENTRY;         // entry point address
   asm volatile( " stmfd sp!, {lr}");        // save LR
   asm volatile( " mov lr, pc");             // return address
   asm volatile( " bx %0" : /*no output*/ : "r" (iap_entry)); // call IAP (thumb)
   asm volatile( " ldmfd sp,  {lr}");        // restore LR
} // CappIap
