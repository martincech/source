//*****************************************************************************
//
//    Uart1Async.c - RS232 communication services
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#include "../../inc/Uart1.h"
#include "../../inc/Cpu.h"

#if   defined( __LPC210x__) || defined( __LPC213x__)
   // P0.8 - TxD, P0.9 - RxD
   #define PINSEL        PCB_PINSEL0        
   #define PINSEL_MASK   ((3 << 16) | (3 << 18))   // PINSEL0, P0.8, P0.9 dibits
   #define PINSEL_UART   ((1 << 16) | (1 << 18))   // PINSEL0 function 01 on P0.8, P0.9
   
   #define GPIO_UART       (GPIO0_IODIR)
   #define GPIO_UART_MASK  ((1 << 8) | (1 << 9))
#else
   #error "Unknown processor model"
#endif

// parametry :

#define UART_RX_TIMEOUT UART1_RX_TIMEOUT
#define UART_ISIZE      UART1_RX_BUFFER_SIZE
#define UART_OSIZE      UART1_TX_BUFFER_SIZE
#ifdef UART1_MULVAL
   #define UART_MULVAL     UART1_MULVAL
   #define UART_DIVADDVAL  UART1_DIVADDVAL
#endif
#ifdef UART1_BREAK_DETECTION
   #define UART_BREAK_DETECTION
#endif
   
// funkce :

#define UARTInit       Uart1Init
#define UARTDisconnect Uart1Disconnect
#define UARTSetup      Uart1Setup
#define UARTTxBusy     Uart1TxBusy
#define UARTTxChar     Uart1TxChar
#define UARTRxChar     Uart1RxChar
#define UARTRxWait     Uart1RxWait
#define UARTFlushChars Uart1FlushChars
#define UARTIsBreak    Uart1IsBreak

// porty :

#define UART_RBR  UART1_RBR
#define UART_THR  UART1_THR
#define UART_IER  UART1_IER
#define UART_IIR  UART1_IIR
#define UART_FCR  UART1_FCR
#define UART_LCR  UART1_LCR
#define UART_LSR  UART1_LSR
#define UART_DLL  UART1_DLL
#define UART_DLM  UART1_DLM
#define UART_FDR  UART1_FDR

// preruseni :

#define VIC_UART  VIC_UART1
#define UART_IRQ  UART1_IRQ

// sablona modulu :

#include "UartAsync.c"
