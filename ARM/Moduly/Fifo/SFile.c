//*****************************************************************************
//
//    SFile.c       Secure file
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "..\inc\SFile.h"
#include "..\inc\Eep.h"
#include "MemoryDef.h"              // project directory EEPROM layout

static unsigned _CurrentSeek;

static TYesNo WriteMarker( word Address);
// Write marker at <Address>

static TYesNo IsMarker( word Address);
// Test marker at <Address>

//-----------------------------------------------------------------------------
// Reset
//-----------------------------------------------------------------------------

TYesNo SFileReset( void)
// Prepare file for write
{
   _CurrentSeek = 0;
   return( WriteMarker( 0));
} // SFileReset

//-----------------------------------------------------------------------------
// Append
//-----------------------------------------------------------------------------

TYesNo SFileAppend( void *Data)
// Append record with <Data>
{
byte *p;
int  i;

   if( (_CurrentSeek + 1) >= SFILE_COUNT){
      return( NO);                     // not enough space
   }
   WriteMarker( (_CurrentSeek + 1) * SFILE_RECORD_SIZE);
   if( !EepPageWriteStart( _CurrentSeek * SFILE_RECORD_SIZE + SFILE_START)){
      return( NO);                    // power failure
   }
   p = (byte *)Data;
   for( i = 0; i < (int)SFILE_RECORD_SIZE; i++){
      EepPageWriteData( *p);
      p++;
   }
   EepPageWritePerform();
   _CurrentSeek++;
   return( YES);
} // SFileAppend

//-----------------------------------------------------------------------------
// Read
//-----------------------------------------------------------------------------

TYesNo SFileRead( int Index, void *Data)
// Read <Data> from <Index>. Returns false on EOF
{
unsigned Address;
byte     *p;
int      i;

   Address = Index * SFILE_RECORD_SIZE;
   if( IsMarker( Address)){
      return( NO);                     // end of file
   }
   p = (byte *)Data;
   EepBlockReadStart( Address  + SFILE_START);
   for( i = 0; i < (int)SFILE_RECORD_SIZE; i++){
      *p = EepBlockReadData();
      p++;
   }
   EepBlockReadStop();
   return( YES);
} // SFileRead

//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Write Marker
//-----------------------------------------------------------------------------

static TYesNo WriteMarker( word Address)
// Write marker at <Address>
{
int i;

   if( !EepPageWriteStart( Address + SFILE_START)){
      return( NO);
   }
   for( i = 0; i < (int)SFILE_MARKER_SIZE; i++){
      EepPageWriteData( SFILE_MARKER >> (i << 3));
   }
   EepPageWritePerform();
   return( YES);
} // WriteMarker

//-----------------------------------------------------------------------------
// Test Marker
//-----------------------------------------------------------------------------

static TYesNo IsMarker( word Address)
// Write marker at <Address>
{
int i;

   EepBlockReadStart( Address + SFILE_START);
   for( i = 0; i < (int)SFILE_MARKER_SIZE; i++){
      if( EepBlockReadData() != ((SFILE_MARKER >> (i << 3)) & 0xFF)){
         EepBlockReadStop();
         return( NO);
      }
   }
   EepBlockReadStop();
   return( YES);
} // IsMarker
