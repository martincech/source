//******************************************************************************
//                                                                            
//  StrUtil.c      String utilities
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../../inc/wgt/StrDef.h"
#include <string.h>

//------------------------------------------------------------------------------
//  Get
//------------------------------------------------------------------------------

const char *StrGet( TUniStr Str)
// Get translated string
{
   if( Str > _STR_LIMIT){
      return( Str);                    // without translation
   }
#if LANGUAGE_COUNT > 1  
   return( AllStrings[ (int)Str][ ActiveLanguage]);
#else
   return( AllStrings[ (int)Str]);
#endif
} // StrGet

//------------------------------------------------------------------------------
//  Trim right
//------------------------------------------------------------------------------

void StrTrimRight( char *String)
// Remove right spaces
{
int i;

   i = strlen( String);
   while( --i >= 0){
      if( String[ i] != ' '){
         return;
      }
      String[ i] = '\0';
   }
} // StrTrimRight

//------------------------------------------------------------------------------
//  Set width
//------------------------------------------------------------------------------

void StrSetWidth( char *String, int Width)
// Extend text with spaces, up to <Width>
{
int i;

   i = strlen( String);
   while( i < Width){
      String[ i] = ' ';
      i++;
   }
   String[ i] = '\0';
} // StrSetWidth
