//******************************************************************************
//                                                                            
//  DMsg.c         Display text message box
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "DMsg.h"
#include "../../inc/wgt/DLabel.h"
#include "../../inc/wgt/DGrid.h"
#include "../../inc/wgt/DEvent.h"
#include "../../inc/Graphic.h"
#include "Str.h"                        // strings from project directory

// local functions :
static void FormatText( TUniStr Text, TUniStr Text2);
// Draw texts

//------------------------------------------------------------------------------
//  Info
//------------------------------------------------------------------------------

void DMsgOk( TUniStr Caption, TUniStr Text, TUniStr Text2)
// Displays <Text> with title <Caption> with OK button
{
   // draw widgets :
   GClear();
   DGridTitle( Caption);
   FormatText( Text, Text2);
   DGridButton( STR_BTN_OK);
   DGridFrame();
   GFlush();
   // wait for user response :
   DEventWaitForEnter();
} // DMsgOk

//------------------------------------------------------------------------------
//  Nowait info
//------------------------------------------------------------------------------

void DMsgCancel( TUniStr Caption, TUniStr Text, TUniStr Text2)
// Displays window with Cancel button. Warning : doesn't wait for a key
{
   // draw widgets :
   GClear();
   DGridTitle( Caption);
   FormatText( Text, Text2);
   DGridButton( STR_BTN_CANCEL);
   DGridFrame();
   GFlush();
} // DMsgCancel

//------------------------------------------------------------------------------
//  Confirmation
//------------------------------------------------------------------------------

TYesNo DMsgOkCancel( TUniStr Caption, TUniStr Text, TUniStr Text2)
// Displays <Text> with title <Caption> with OK/Cancel buttons
{
   // draw widgets :
   GClear();
   DGridTitle( Caption);
   FormatText( Text, Text2);
#ifndef DG_RIGHT_OK
   DGridTwoButtons( STR_BTN_OK, STR_BTN_CANCEL);
#else
   DGridTwoButtons( STR_BTN_CANCEL, STR_BTN_OK);
#endif
   DGridFrame();
   GFlush();
   // wait for user response :
   return( DEventWaitForEnterEsc());
} // DMsgOkCancel

//------------------------------------------------------------------------------
//  Selection
//------------------------------------------------------------------------------

TYesNo DMsgYesNo( TUniStr Caption, TUniStr Text, TUniStr Text2)
// Displays <Text>/<Text2> with title <Caption> with Yes/No buttons
{
   // draw widgets :
   GClear();
   DGridTitle( Caption);
   FormatText( Text, Text2);
#ifndef DG_RIGHT_OK
   DGridTwoButtons( STR_BTN_YES, STR_BTN_NO);
#else
   DGridTwoButtons( STR_BTN_NO, STR_BTN_YES);
#endif
   DGridFrame();
   GFlush();
   // wait for user response :
   return( DEventWaitForEnterEsc());
} // DMsgYesNo

//------------------------------------------------------------------------------
//  Format text
//------------------------------------------------------------------------------

static void FormatText( TUniStr Text, TUniStr Text2)
// Draw texts
{
   if( !Text2){
      DLabelCenter( Text, 0, DG_TITLE_HEIGHT, G_WIDTH, DGridHeight());
   } else {
      DLabelCenter( Text,  0, DG_TITLE_HEIGHT, G_WIDTH, 2 * (DGridHeight() / 3));
      DLabelCenter( Text2, 0, DG_TITLE_HEIGHT, G_WIDTH, DGridHeight());
   }
} // FormatText
