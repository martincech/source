//******************************************************************************
//                                                                            
//  DMenu.c         Display menu
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../../inc/wgt/DMenu.h"
#include "../../inc/wgt/DLabel.h"
#include "../../inc/wgt/DEvent.h"
#include "../../inc/wgt/DGrid.h"
#include "../../inc/wgt/Beep.h"
#include "../../inc/Graphic.h"

#define MENU_MAX_ROWS   32
#define DMENU_WIDTH    (G_WIDTH - DMENU_LEFT - DMENU_RIGHT)
#define DMENU_HEIGHT   (G_HEIGHT - DMENU_TOP)                // items area height
#define DMENU_STATUS_Y (G_HEIGHT - DMENU_BOTTOM)             // status line Y

// Local functions :

static void MenuDraw( const TUniStr *Menu, unsigned Mask, TMenuItemCb *MenuItemCb, void *UserData,
                      int *RowCount);
// Draw menu, returns <RowCount>

static int MenuSelect( int CursorItem, int RowCount);
// Displays menu selection, returns selected item or -1 on Esc

//------------------------------------------------------------------------------
//  Menu
//------------------------------------------------------------------------------

int DMenu( TUniStr Caption, const TUniStr *Menu, unsigned Mask, TMenuItemCb *MenuItemCb, void *UserData, int CursorItem)
// Displays <Menu>, returns selected item or -1 on Esc
{
int RowCount;                // menu rows
int Row;                     // selected (visible) row
int i;
int Index;                   // visible index

   GClear();
   DGridTitle( Caption);
#ifdef DMENU_FRAME
   GLine( 0, DMENU_TOP, 0, G_HEIGHT - DMENU_BOTTOM - 1);
   GLine( G_WIDTH - 1, DMENU_TOP, G_WIDTH - 1, G_HEIGHT - DMENU_BOTTOM - 1);
#endif
   MenuDraw( Menu, Mask, MenuItemCb, UserData, &RowCount);
   // recalculate CursorItem to visible items :
   Index = CursorItem;
   for( i = 0; i <= CursorItem; i++){
      if( Mask & (1 << i)){
         Index--;                      // remove invisible
      }
   }
   // selection :
   Row = MenuSelect( Index, RowCount);
   if( Row < 0){
      return( -1);                     // esc pressed
   }
   // search for visible items :
   Index = -1;
   for( i = 0; i < MENU_MAX_ROWS; i++){
      if( Mask & (1 << i)){
         continue;                     // invisible
      }
      Index++;                         // visible
      if( Index == Row){
         return( i);                   // index found
      }
   }
   return( -1);                        // compiler only
} // DMenu

//******************************************************************************

//------------------------------------------------------------------------------
//  MenuDraw
//------------------------------------------------------------------------------

static void MenuDraw( const TUniStr *Menu, unsigned Mask, TMenuItemCb *MenuItemCb, void *UserData,
                      int *RowCount)
// Draw menu, returns <RowCount> and <ItemHeight>
{
int i;
int Count;
int y;

   // clear data area
   GSetColor( DCOLOR_BACKGROUND);
   GBox( DMENU_LEFT, DMENU_TOP, DMENU_WIDTH, DMENU_HEIGHT);
   // draw status area
   GSetColor( DCOLOR_STATUS_BG);
   GBox( 0, DMENU_STATUS_Y, G_WIDTH, DMENU_BOTTOM);
   GSetColor( DCOLOR_DEFAULT);
   // draw items
   i     = 0;
   Count = 0;
   y     = DMENU_TOP;
   while( Menu[ i]){
      if( Mask & (1 << i)){
         i++;                          // skip invisible item
         continue;
      }
      DLabel( Menu[ i], DMENU_LEFT_TEXT, y);
      if( MenuItemCb){
         (*MenuItemCb)( i, y, UserData);
      }
      y += DMENU_ITEM_HEIGHT;
      i++;
      Count++;
   }
   *RowCount   = Count;
} // MenuDraw

//------------------------------------------------------------------------------
//  Menu Select
//------------------------------------------------------------------------------

static int MenuSelect( int CursorItem, int RowCount)
// Displays menu selection, returns selected item or -1 on Esc
{
int LastItem;

   LastItem    = -1;
   GSetMode( GMODE_XOR);
   GSetColor( DCOLOR_CURSOR);
   forever {
      if( LastItem != CursorItem){
         // status changed - redraw
         if( LastItem >= 0){
            // deselect last
            GBox( DMENU_LEFT, DMENU_TOP + LastItem * DMENU_ITEM_HEIGHT, 
                  DMENU_WIDTH, DMENU_ITEM_HEIGHT);
         }
         // select current
         GBox( DMENU_LEFT, DMENU_TOP + CursorItem * DMENU_ITEM_HEIGHT, 
               DMENU_WIDTH, DMENU_ITEM_HEIGHT);
         LastItem = CursorItem;       // remember last
         GFlush();                     // redraw
      }
      switch( DEventWait()){
         case K_UP :
         case K_UP | K_REPEAT :
            if( CursorItem == 0){
               BeepError();
               break;
            }
            BeepKey();
            CursorItem--;
            break;

         case K_DOWN :
         case K_DOWN | K_REPEAT :
            if( CursorItem == RowCount - 1){
               BeepError();
               break;
            }
            BeepKey();
            CursorItem++;
            break;

         case K_ENTER :
            BeepKey();
            GSetMode( GMODE_REPLACE);
            GSetColor( DCOLOR_DEFAULT);
            return( CursorItem);

         case K_ESC :
            BeepKey();
         case K_TIMEOUT :
            GSetMode( GMODE_REPLACE);
            GSetColor( DCOLOR_DEFAULT);
            return( -1);
      }     
   }
} // MenuSelect
