//******************************************************************************
//                                                                            
//   DInputList.c   Input dialog for string list
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../../inc/wgt/DInputList.h"
#include "../../inc/wgt/DEnterList.h"
#include "../../inc/wgt/DEvent.h"
#include "../../inc/wgt/DLabel.h"
#include "../../inc/wgt/DProgress.h"
#include "../../inc/wgt/DGrid.h"
#include "../../inc/wgt/Beep.h"
#include "../../inc/Graphic.h"
#include "Str.h"                        // strings from project directory

//------------------------------------------------------------------------------
//  Input List
//------------------------------------------------------------------------------

TYesNo DInputList( TUniStr Caption, TUniStr Text, int *Value, 
                   const TUniStr *List)
// Input list
{
int x;                    // edit field position

   x = G_WIDTH / 2 - DEnterListWidth( List) / 2;
   // draw widgets :
   GClear();
   DGridTitle( Caption);
   DLabelCenter( Text, 0, DG_CAPTION_Y, G_WIDTH, 0);
#ifndef DG_RIGHT_OK
   DGridTwoButtons( STR_OK, STR_CANCEL);
#else
   DGridTwoButtons( STR_CANCEL, STR_OK);
#endif
   DGridFrame();
   GFlush();
   // edit enum :
   return( DEnterList( Value, List, x, DG_EDIT_Y, CENTER_MIDDLE));
} // DInputList

//------------------------------------------------------------------------------
//  Spin
//------------------------------------------------------------------------------

TYesNo DInputSpin( TUniStr Caption, TUniStr Text, int *Value, 
                   int MaxValue, TAction *OnChange)
// Input value by spinner
{
int x;                    // edit field position

   x = G_WIDTH / 2 - DEnterSpinWidth( MaxValue) / 2;
   // draw widgets :
   GClear();
   DGridTitle( Caption);
   DLabelCenter( Text, 0, DG_CAPTION_Y, G_WIDTH, 0);
#ifndef DG_RIGHT_OK
   DGridTwoButtons( STR_OK, STR_CANCEL);
#else
   DGridTwoButtons( STR_CANCEL, STR_OK);
#endif
   DGridFrame();
   GFlush();
   // edit enum :
   return( DEnterSpin( Value, MaxValue, OnChange, x, DG_EDIT_Y));
} // DInputSpin
