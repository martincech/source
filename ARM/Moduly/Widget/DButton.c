//******************************************************************************
//                                                                            
//  DButton.c      Display button
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../../inc/wgt/DButton.h"
#include "../../inc/wgt/DLabel.h"
#include "../../inc/Graphic.h"

//------------------------------------------------------------------------------
//  Button
//------------------------------------------------------------------------------

void DButton( TUniStr Text, int x, int y, int width, int height)
// Display button with <Text> at position <x,y> with <width,height>
{
   DLabelCenter( Text, x, y, width, height);
   GRectangle( x, y, width, height);
#ifdef DG_SHADOW
   GSetColor( DCOLOR_SHADOW);
   GBox( x + DG_BUTTON_SHADOW, y + height, width, DG_BUTTON_SHADOW);
   GBox( x + width, y + DG_BUTTON_SHADOW, DG_BUTTON_SHADOW, height);
   GSetColor( DCOLOR_DEFAULT);
#endif
} // DButton
