//******************************************************************************
//                                                                            
//   DHistogram.c   Display histogram
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../../inc/wgt/DHistogram.h"
#include "../../inc/wgt/DEvent.h"
#include "../../inc/wgt/DLabel.h"
#include "../../inc/wgt/DProgress.h"
#include "../../inc/wgt/Beep.h"
#include "../../inc/Graphic.h"
#include "../../inc/conio.h"
#include "DWeight.h"

// Icon constants :
#define DHISTI_W   (DHISTI_COLUMN_W * HISTOGRAM_SLOTS)

// Local functions :

static void DrawHistogram( THistogram *Histogram, int LowLimit, int HighLimit);
// Draw <Histogram>

static void DrawColumn( int Value, int SlotIndex);
// Draw column at <SlotIndex> with <Value>

static int GetSlotX( int SlotIndex);
// Returns <SlotIndex> x position

//------------------------------------------------------------------------------
//  Histogram
//------------------------------------------------------------------------------

TYesNo DHistogram( THistogram *Histogram,  int LowLimit, int HighLimit)
// Display histogram. Returns YES no Enter
{
int Cursor;
int OldCursor;

   Cursor    = HISTOGRAM_SLOTS / 2;
   OldCursor = -1;
   DrawHistogram( Histogram, LowLimit, HighLimit);
   forever {
      if( Cursor != OldCursor){
         // clear area
         GSetColor( COLOR_WHITE);
         GBox( 0, DHIST_BASE_Y + 1, G_WIDTH, DHIST_INFO_H);
         GSetColor( COLOR_BLACK);
         // draw labels
         GTextAt( DHIST_SAMPLES_X, DHIST_BASE_Y + 1);
         cprintf( "%d", Histogram->Slot[ Cursor]);         // samples count
         GTextAt( DHIST_WEIGHT_X - GCharWidth(),  DHIST_BASE_Y + 1);
         if( Cursor == 0){
            cputch( '<');                                  // samples under minimum
         } else if( Cursor == HISTOGRAM_SLOTS - 1){
            cputch( '>');                                  // samples above maximum
         } // else no prefix
         GTextAt( DHIST_WEIGHT_X,  DHIST_BASE_Y + 1);
         DWeightWithUnits( HistogramGetValue( Histogram, Cursor));  // weight at cursor
         // draw cursor marker
         GSetMode( GMODE_XOR);
         GLine( GetSlotX( Cursor), DHIST_BASE_Y + 1, GetSlotX( Cursor), G_HEIGHT - 1);
         GSetMode( GMODE_REPLACE);
         OldCursor = Cursor;           // remember value
         GFlush();                     // redraw
      }
      switch( DEventWait()){
         case K_RIGHT | K_REPEAT :
         case K_RIGHT :
            if( Cursor == HISTOGRAM_SLOTS - 1){
               BeepError();
               break;
            }
            BeepKey();
            Cursor++;
            break;

         case K_LEFT | K_REPEAT :
         case K_LEFT :
            if( Cursor == 0){
               BeepError();
               break;
            }
            BeepKey();
            Cursor--;
            break;

         case K_ENTER :
            BeepKey();
            return( YES);

         case K_ESC :
            BeepKey();
         case K_TIMEOUT :
            return( NO);
      }
   }
} // DHistogram

//------------------------------------------------------------------------------
//  Histogram Icon
//------------------------------------------------------------------------------

void DHistogramIcon( THistogram *Histogram, int x, int y)
// Display histogram icon at <x,y>
{
int i;
int MaxValue;
int Height;

   GLine( x, y + DHISTI_BASE_Y, x + DHISTI_W, y + DHISTI_BASE_Y);     // base line
   MaxValue = HistogramMaximum( Histogram);
   for( i = 0; i < HISTOGRAM_SLOTS; i++){
      Height = (HistogramNormalize( Histogram, MaxValue, i) * DHISTI_COLUMN_H) / HISTOGRAM_NORM;
      GBox( x + i * DHISTI_COLUMN_W, y + DHISTI_BASE_Y - Height, DHISTI_COLUMN_W, Height);      
   }
} // DHistogramIcon

//------------------------------------------------------------------------------
//  Draw histogram
//------------------------------------------------------------------------------

static void DrawHistogram( THistogram *Histogram, int LowLimit, int HighLimit)
// Draw <Histogram>
{
int i;
int Value;
int MaxValue;

   GLine( 0, DHIST_BASE_Y, G_WIDTH - 1, DHIST_BASE_Y);     // base line
   MaxValue = HistogramMaximum( Histogram);
   for( i = 0; i < HISTOGRAM_SLOTS; i++){
      Value = HistogramNormalize( Histogram, MaxValue, i);
      DrawColumn( Value, i);
   }
   // marker settings
   GSetMode( GMODE_XOR);
   GSetPattern( PATTERN_DOTTED);
   // draw low limit marker
   if( LowLimit > 0){
      i = HistogramGetSlot( Histogram, LowLimit);
      if( i > 0 && i < HISTOGRAM_SLOTS - 1){
         GLine( GetSlotX( i), DHIST_CEIL_Y, GetSlotX( i), DHIST_BASE_Y);
      } // else out of visible area
   }
   // draw high limit marker
   if( HighLimit > 0){
      i = HistogramGetSlot( Histogram, HighLimit);
      if( i > 0 && i < HISTOGRAM_SLOTS - 1){
         GLine( GetSlotX( i), DHIST_CEIL_Y, GetSlotX( i), DHIST_BASE_Y);
      } // else out of visible area
   }
   // return to standard settings
   GSetMode( GMODE_REPLACE);
   GSetPattern( PATTERN_SOLID);
} // DrawHistogram

//------------------------------------------------------------------------------
//  Draw Column
//------------------------------------------------------------------------------

static void DrawColumn( int Value, int SlotIndex)
// Draw column at <SlotIndex> with <Value>
{
int Height;

   Height = (Value * (DHIST_BASE_Y - DHIST_CEIL_Y)) / HISTOGRAM_NORM;
   if( Value > 0 && Height == 0){
      Height = 1;                      // nonzero value - set 1pixel height
   }
   GBox( GetSlotX( SlotIndex) - DHIST_COLUMN_W / 2, DHIST_BASE_Y - Height, DHIST_COLUMN_W, Height);
} // Draw column

//------------------------------------------------------------------------------
//  Slot X
//------------------------------------------------------------------------------

static int GetSlotX( int SlotIndex)
// Returns <SlotIndex> x position
{
   return( SlotIndex * DHIST_COLUMN_X + G_WIDTH / 2 - HISTOGRAM_SLOTS / 2 * DHIST_COLUMN_X);
} // GetSlotX
