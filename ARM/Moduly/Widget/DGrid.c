//******************************************************************************
//                                                                            
//  DGrid.c        Display grid
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../../inc/wgt/DGrid.h"
#include "../../inc/wgt/DLabel.h"
#include "../../inc/wgt/DButton.h"
#include "../Inc/Graphic.h"

//------------------------------------------------------------------------------
//  Title
//------------------------------------------------------------------------------

void DGridTitle( TUniStr Title)
// Display window <Title>
{
   if( !Title){
      return;
   }
   // clear background
   GSetColor( DCOLOR_TITLE_BG);
   GBox( 0, 0, G_WIDTH, DG_TITLE_HEIGHT);
   // draw title
   GSetColor( DCOLOR_TITLE);  
   DLabelCenter( Title, 0, 0, G_WIDTH, DG_TITLE_HEIGHT);
   GSetColor( DCOLOR_DEFAULT);
} // DGridTitle

//------------------------------------------------------------------------------
//  Status line
//------------------------------------------------------------------------------

void DGridStatus( void)
// Clear status area
{
   GSetColor( DCOLOR_STATUS_BG);
   GBox( 0, G_HEIGHT - DG_STATUS_HEIGHT, G_WIDTH, DG_STATUS_HEIGHT);
   GSetColor( DCOLOR_DEFAULT);
} // DGridStatus

//------------------------------------------------------------------------------
//  Frame
//------------------------------------------------------------------------------

void DGridFrame( void)
// Display left & right line
{
#ifdef DG_FRAME
   GLine( 0, DG_TITLE_HEIGHT, 0, G_HEIGHT - 1);
   GLine( G_WIDTH - 1, DG_TITLE_HEIGHT, G_WIDTH - 1, G_HEIGHT - 1);
   GLine( 0, G_HEIGHT - 1, G_WIDTH - 1, G_HEIGHT - 1);
#endif
} // DGridFrame

//------------------------------------------------------------------------------
//  Button
//------------------------------------------------------------------------------

void DGridButton( TUniStr Button)
// Display one <Button>
{
   DButton( Button, 
            G_WIDTH / 2 - DG_BUTTON_WIDTH / 2, 
            G_HEIGHT - DG_BUTTON_MARGIN - DG_BUTTON_HEIGHT,
            DG_BUTTON_WIDTH,
            DG_BUTTON_HEIGHT);
} // DGridButton

//------------------------------------------------------------------------------
//  Two buttons
//------------------------------------------------------------------------------

void DGridTwoButtons( TUniStr LButton, TUniStr RButton)
// Display two buttons
{
   DButton( LButton, 
            DG_BUTTON_MARGIN, 
            G_HEIGHT - DG_BUTTON_MARGIN - DG_BUTTON_HEIGHT,
            DG_BUTTON_WIDTH,
            DG_BUTTON_HEIGHT);
   DButton( RButton, 
            G_WIDTH  - DG_BUTTON_MARGIN - DG_BUTTON_WIDTH, 
            G_HEIGHT - DG_BUTTON_MARGIN - DG_BUTTON_HEIGHT,
            DG_BUTTON_WIDTH,
            DG_BUTTON_HEIGHT);
} // DGridTwoButtons

//------------------------------------------------------------------------------
//  Width
//------------------------------------------------------------------------------

int DGridWidth( void)
// Returns width of main area
{
   return( G_WIDTH - 2 * DG_BUTTON_MARGIN);
} // DGridWidth

//------------------------------------------------------------------------------
//  Height
//------------------------------------------------------------------------------

int DGridHeight( void)
// Returns height of main area
{
   return( G_HEIGHT - DG_TITLE_HEIGHT - DG_BUTTON_MARGIN - DG_BUTTON_HEIGHT);
} // DGridHeight
