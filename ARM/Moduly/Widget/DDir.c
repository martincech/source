//******************************************************************************
//                                                                            
//  DDir.c         Display file directory
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../../Inc/Wgt/DDir.h"
#include "../../Inc/Wgt/DEvent.h"
#include "../../Inc/Wgt/DMsg.h"
#include "../../Inc/Wgt/DGrid.h"
#include "../../Inc/Wgt/Beep.h"
#include "../../Inc/File/Fd.h"
#include "../../Inc/File/Ds.h"
#include "../../Inc/Graphic.h"
#include "../../Inc/conio.h"
#include "Str.h"                       // project directory strings

// Constants :
#define DDIR_COUNT  ((G_HEIGHT - DDIR_TOP - DDIR_BOTTOM) / DDIR_ITEM_HEIGHT)   // items per page
#define DDIR_WIDTH   (G_WIDTH - DDIR_LEFT - DDIR_RIGHT)                        // cursor width
#define DDIR_HEIGHT  (G_HEIGHT - DDIR_TOP - DDIR_BOTTOM)                       // items area height
#define DDIR_INFO_Y  (G_HEIGHT - DDIR_BOTTOM)     // info line Y

// Local functions :

static int DisplaySelection( TFDataSet CheckSet, TFdirHandle Handle);
// Display and edit, retuns index

static void DisplayPage( TFDataSet CheckSet, int CurrentPage, int *RowCount);
// Display directory set page

static void SetCursorPosition( TFdirHandle Handle, int *CurrentPage, int *CurrentItem);
// Searches for the <Handle>, returns <CurrentPage>, <CurrentItem>

static TYesNo NextPage( int *CurrentPage, int *RowCount);
// Move at next page

static TYesNo PreviousPage( int *CurrentPage, int *RowCount);
// Move at previous page

static void HandleToIndex( TFDataSet CheckSet, TFDataSet DataSet);
// recalculate data set to directory indexes

static void IndexToHandle( TFDataSet DataSet, TFDataSet CheckSet);
// recalculate data set to directory indexes

static void ItemInfo( int Index);
// Display item info

//------------------------------------------------------------------------------
//  Select
//------------------------------------------------------------------------------

TFdirHandle DDirSelect( TUniStr Caption, TFdirHandle Handle)
// Select file from directory
{
int Index;

   GClear();
   DGridTitle( Caption);
#ifdef DDIR_FRAME
   GLine( 0, DDIR_TOP, 0, G_HEIGHT - DDIR_BOTTOM - 1);
   GLine( G_WIDTH - 1, DDIR_TOP, G_WIDTH - 1, G_HEIGHT - DDIR_BOTTOM - 1);
#endif
   Index = DisplaySelection( 0, Handle);
   if( Index < 0){
      return( FDIR_INVALID);           // escape
   }
   return( FdGet( Index));
} // DDirSelect

//------------------------------------------------------------------------------
//  Select set
//------------------------------------------------------------------------------

void DDirSelectSet( TUniStr Caption, TFDataSet DataSet)
// Select file set from directory
{
TFDataSet CheckSet;          // directory checkboxes

   GClear();
   DGridTitle( Caption);
#ifdef DDIR_FRAME
   GLine( 0, DDIR_TOP, 0, G_HEIGHT - DDIR_BOTTOM - 1);
   GLine( G_WIDTH - 1, DDIR_TOP, G_WIDTH - 1, G_HEIGHT - DDIR_BOTTOM - 1);
#endif
   HandleToIndex( CheckSet, DataSet);  // recalculate data set to directory indexes
   DisplaySelection( CheckSet, FDIR_INVALID);
   IndexToHandle( DataSet, CheckSet);  // recalculate to handle set
} // DDirSelectSet

//------------------------------------------------------------------------------
//  Select set
//------------------------------------------------------------------------------

static int DisplaySelection( TFDataSet CheckSet, TFdirHandle Handle)
// Display and edit
{
int RowCount;                // rows count
int CurrentItem;             // active row
int LastItem;                // last active row
int CurrentPage;             // start of page offset
int LastPage;                // last start of page offset
int Index;                   // temporary index

   if( !FdCount()){
      DMsgOk( STR_INFO, STR_DIRECTORY_EMPTY);
      return( -1);
   }
   CurrentItem =  0;                   // first row
   CurrentPage =  0;                   // first page
   if( Handle != FDIR_INVALID){
      // update cursor position
      SetCursorPosition( Handle, &CurrentPage, &CurrentItem);
   }
   LastItem    = -1;                   // force redraw
   LastPage    = -1;                   // force redraw
   forever {
      if( CurrentPage != LastPage){
         // redraw page
         DisplayPage( CheckSet, CurrentPage, &RowCount);
         LastPage = CurrentPage;
         LastItem = -1;                // force cursor redraw
      }
      if( CurrentItem != LastItem){
         GSetMode( GMODE_XOR);
         GSetColor( DCOLOR_CURSOR);
         if( LastItem >= 0){
            // deselect last
            GBox( DDIR_LEFT, DDIR_TOP + LastItem * DDIR_ITEM_HEIGHT, DDIR_WIDTH, DDIR_ITEM_HEIGHT);
         }
         // select current
         GBox( DDIR_LEFT, DDIR_TOP + CurrentItem * DDIR_ITEM_HEIGHT, DDIR_WIDTH, DDIR_ITEM_HEIGHT);
         LastItem = CurrentItem;       // remember last
         GSetMode( GMODE_REPLACE);
         GSetColor( DCOLOR_DEFAULT);
         ItemInfo( CurrentPage + CurrentItem);
         GFlush();                     // redraw
      }
      switch( DEventWait()){
         case K_UP :
         case K_UP | K_REPEAT :
            if( CurrentItem > 0){
               BeepKey();
               CurrentItem--;
               break;
            }
            if( !PreviousPage( &CurrentPage, &RowCount)){
               BeepError();
               break;
            }
            BeepKey();
            CurrentItem = RowCount - 1;
            break;

         case K_DOWN :
         case K_DOWN | K_REPEAT :
            if( CurrentItem < RowCount - 1){
               BeepKey();
               CurrentItem++;
               break;
            }
            if( !NextPage( &CurrentPage, &RowCount)){
               BeepError();
               break;
            }
            BeepKey();
            CurrentItem = 0;
            break;

         case K_LEFT :
         case K_LEFT | K_REPEAT :
            if( !PreviousPage( &CurrentPage, &RowCount)){
               BeepError();
               break;
            }
            BeepKey();
            break;

         case K_RIGHT :
         case K_RIGHT | K_REPEAT :
            if( !NextPage( &CurrentPage, &RowCount)){
               BeepError();
               break;
            }
            BeepKey();
            if( CurrentItem >= RowCount){
               CurrentItem = RowCount - 1;       // short page - move cursor at last item               
            }
            break;

         case K_ENTER :
            BeepKey();
            Index = CurrentPage + CurrentItem;   // actual index
            if( !CheckSet){
               return( Index);                   // file selection
            }
            // file set - toggle selection
            if( DsContains( CheckSet, Index)){
               DsRemove( CheckSet, Index);
            } else {
               DsAdd(    CheckSet, Index);
            }
            LastPage = -1;                       // force redraw
            break;

         case K_ESC :
            BeepKey();
         case K_TIMEOUT :
            return( -1);
      }     
   }
} // DisplaySelection

//------------------------------------------------------------------------------
//  Display page
//------------------------------------------------------------------------------

static void DisplayPage( TFDataSet CheckSet, int CurrentPage, int *RowCount)
// Display directory page
{
int  Count;
TFdirHandle Fd;
char Name[ FDIR_NAME_LENGTH + 1];
int  i;

   // clear data area
   GSetColor( DCOLOR_BACKGROUND);
   GBox( DDIR_LEFT, DDIR_TOP, DDIR_WIDTH, DDIR_HEIGHT);
   GSetColor( DCOLOR_DEFAULT);
   // draw files
   Count      = 0;
   FdMoveAt( CurrentPage);
   i = CurrentPage;
   while( (Fd = FdFindNext()) != FDIR_INVALID){
      if( Count >= DDIR_COUNT){
         // end of page
         *RowCount = Count;
         return;
      }
      GTextAt( DDIR_LEFT_TEXT, DDIR_TOP + Count * DDIR_ITEM_HEIGHT);
      if( CheckSet){
         // display checkbox
         if( DsContains( CheckSet, i)){
            cputch( '*');              // checked
         } else {
            cputch( ' ');              // unchecked
         }
         GTextAt( DDIR_LEFT_TEXT + DDIR_CBOX_WIDTH, DDIR_TOP + Count * DDIR_ITEM_HEIGHT);
      }
      FdGetName( Fd, Name);
      cputs( Name);
      Count++;
      i++;
   }
   *RowCount = Count;
} // DisplayPage

//------------------------------------------------------------------------------
//  Cursor position
//------------------------------------------------------------------------------

static void SetCursorPosition( TFdirHandle Handle, int *CurrentPage, int *CurrentItem)
// Searches for the <Handle>, returns <CurrentPage>, <CurrentItem>
{
int         Position;
TFdirHandle Fd;

   if( !FdCount()){
      return;                          // directory empty
   }   
   Position = 0;
   FdFindBegin();
   while( (Fd = FdFindNext()) != FDIR_INVALID){
      if( Fd == Handle){
         break;
      }
      Position++;
   }
   if( Fd == FDIR_INVALID){
      return;                          // not found
   }
   // find page & cursor offset :
   *CurrentPage = Position / DDIR_COUNT;
   *CurrentItem = Position % DDIR_COUNT;
} // SetCursorPosition

//------------------------------------------------------------------------------
//  Next page
//------------------------------------------------------------------------------

static TYesNo NextPage( int *CurrentPage, int *RowCount)
// Move at next page
{
int TotalCount;
int NextCount;

   TotalCount = FdCount();
   if( *CurrentPage + DDIR_COUNT >= TotalCount){
      return( NO);                     // last visible page
   }
   *CurrentPage += DDIR_COUNT;         // move to next page
   NextCount    = TotalCount - *CurrentPage;
   if( NextCount >= DDIR_COUNT){
      *RowCount = DDIR_COUNT;          // whole page
   } else {
      *RowCount = NextCount;           // short page
   }
   return( YES);
} // NextPage

//------------------------------------------------------------------------------
//  Previous page
//------------------------------------------------------------------------------

static TYesNo PreviousPage( int *CurrentPage, int *RowCount)
// Move at previous page
{
int TotalCount;

   if( *CurrentPage == 0){
      return( NO);
   }
   TotalCount = FdCount();
   if( *CurrentPage < DDIR_COUNT){
      *CurrentPage  = 0;
   } else {
      *CurrentPage -= DDIR_COUNT;
   }
   if( TotalCount < DDIR_COUNT){
      *RowCount = TotalCount;
   } else {
      *RowCount = DDIR_COUNT;
   }
   return( YES);
} // PreviousPage

//------------------------------------------------------------------------------
//  Handle to index
//------------------------------------------------------------------------------

static void HandleToIndex( TFDataSet CheckSet, TFDataSet DataSet)
// recalculate data set to directory indexes
{
int         i;
TFdirHandle Handle;

   DsClear( CheckSet);
   i = 0;
   FdFindBegin();
   while( (Handle = FdFindNext()) != FDIR_INVALID){
      if( DsContains( DataSet, Handle)){
         DsAdd( CheckSet, i);
      }
      i++;
   }
} // HandleToIndex

//------------------------------------------------------------------------------
//  Index to handle
//------------------------------------------------------------------------------

static void IndexToHandle( TFDataSet DataSet, TFDataSet CheckSet)
// recalculate data set to directory indexes
{
int         i;
TFdirHandle Handle;

   DsClear( DataSet);
   i = 0;
   FdFindBegin();
   while( (Handle = FdFindNext()) != FDIR_INVALID){
      if( DsContains( CheckSet, i)){
         DsAdd( DataSet, Handle);
      }
      i++;
   }
} // IndexToHandle

//------------------------------------------------------------------------------
//  Item info
//------------------------------------------------------------------------------

static void ItemInfo( int Index)
// Display item info
{
TFdirHandle Fd;
TFdirInfo   Info;

   // clear info area
   GSetColor( DCOLOR_STATUS_BG);
   GBox( 0, DDIR_INFO_Y, G_WIDTH, DDIR_BOTTOM);
   // display info
   GSetColor( DCOLOR_STATUS);
   GTextAt( DDIR_INFO_X, DDIR_INFO_Y);
   FdMoveAt( Index);
   Fd = FdFindNext();
   FdLoad( Fd, &Info);
   cputs( Info.Note);
   GSetColor( DCOLOR_DEFAULT);
} // ItemInfo
