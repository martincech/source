//******************************************************************************
//                                                                            
//   DInputText.c   Display input text box
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../../inc/wgt/DInput.h"
#include "../../inc/wgt/DLabel.h"
#include "../../inc/wgt/DGrid.h"
#include "../../inc/wgt/DEnter.h"
#include "../../inc/wgt/DMsg.h"
#include "../../inc/Graphic.h"
#include "Str.h"                        // strings from project directory
#include <string.h>

//------------------------------------------------------------------------------
//  Text
//------------------------------------------------------------------------------

TYesNo DInputText( TUniStr Caption, TUniStr Text, char *String, int Width)
// Input text up to <Width> letters
{
int x;                    // edit field position

   x = G_WIDTH / 2 - DEnterTextWidth( Width) / 2;
   forever {
      // draw widgets :
      GClear();
      DGridTitle( Caption);
      DLabelCenter( Text, 0, DG_CAPTION_Y, G_WIDTH, 0);
#ifndef DG_RIGHT_OK
      DGridTwoButtons( STR_OK, STR_CANCEL);
#else
      DGridTwoButtons( STR_CANCEL, STR_OK);
#endif
      DGridFrame();
      GFlush();
      // edit text :
      if( !DEnterText( String, Width, x, DG_EDIT_Y)){
         return( NO);                  // escape
      }
      StrTrimRight( String);
      if( strlen( String) > 0){
         return( YES);         
      }
      if( Width == 1){
         // single character - accept space
         String[ 0] = ' ';
         String[ 1] = '\0';
         return( YES);
      }
      DMsgOk( STR_ERROR, STR_STRING_EMPTY, 0);
   }
} // DInputText
