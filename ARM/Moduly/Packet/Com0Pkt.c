//*****************************************************************************
//
//   Com0Pkt.c    COM packet interface
//   Version 1.0 (c) VymOs
//
//*****************************************************************************

#include "../../inc/Com0Pkt.h"
#include "../../inc/CPktDef.h"            // format paketu

#ifdef PACKET0_TX_BLOCK
   #define PACKET_TX_BLOCK
#endif
#ifdef PACKET0_RX_BLOCK
   #define PACKET_RX_BLOCK
#endif
#define PACKET_RX_TIMEOUT   PACKET0_RX_TIMEOUT
#define PACKET_MAX_DATA     PACKET0_MAX_DATA

#define __COM0__
#include "../../inc/Com.h"

#define COMRxPacket     Com0RxPacket
#define COMTxPacket     Com0TxPacket
#define COMTxBlockStart Com0TxBlockStart
#define COMTxBlockByte  Com0TxBlockByte
#define COMTxBlockEnd   Com0TxBlockEnd
#define COMRxData       Com0RxData

#include "ComPkt.c"
