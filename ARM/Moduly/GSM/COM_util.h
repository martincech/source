//******************************************************************************
//
//   COM_util.h   Serial communication utility
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __COM_util_H__
   #define __COM_util_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // zakladni datove typy
#endif

void ComTxDigit( byte n);
// vyslani cislice 0..9

void ComTxString( const char *Text);
// Vysle retezec

void ComSecureTxString( const char *Text);
// Flush Rx a vysle retezec

void ComTxHex( byte Number);
// Vysle hexadecimalni reprezentaci bytu

void ComTxDec( byte Number);
// Vysle dekadickou reprezentaci bytu

byte ComRxDec( void);
// Cte cislo po prvni nenumericky znak

byte ComRxHex( void);
// Cte dva znaky, jako hexa reprezentaci

TYesNo ComWaitChar( const char ch);
// Ceka na znak <ch>

void ComSkipChars( native Count);
// Preskoci pocet znaku <Count>

TYesNo ComRxMatch( const char *Text, native Timeout);
// Cte ocekavany retezec, vraci YES souhlasi-li

TYesNo ComRxString( char *Text, native Length);
// Cte retezec v uvozovkach

#endif




