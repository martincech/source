//******************************************************************************
//
//   GsmBase.c    GSM basic services
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include "../../inc/Gsm.h"
#include "COM_util.h"

#if defined( GSM_COM0)
   #define __COM0__
#elif defined( GSM_COM1)   
   #define __COM1__
#else
   #error "Unknown COM number"
#endif   
#include "../../inc/Com.h"

#define REPLY_TIMEOUT 100             // odezva na prikaz [ms]

// Preddefinovane retezce :

static const char At[]         = "At\r";                // vyzva
static const char Ok[]         = "\r\nOK\r\n";          // ok reply
static const char EchoOff[]    = "ATE0\r";              // echo off
static const char ErrorMode[]  = "ATV1+CMEE=1\r";       // Extended error mode
static const char PduMode[]    = "AT+CMGF=0\r";         // SMS PDU mode

#define TxString( s) ComSecureTxString( s)    // pred vyslanim zrus Rx

//-----------------------------------------------------------------------------
// Reset
//-----------------------------------------------------------------------------

TYesNo GsmReset( void)
// Inicializace modemu
{
   // Echo off :
   TxString( EchoOff);
   if( !ComRxWait( REPLY_TIMEOUT)){    // cekani na odpoved
      return( NO);
   }
   if( !ComWaitChar( '\n')){           // pred OK
      return( NO);
   }
   if( !ComWaitChar( '\n')){           // za OK
      return( NO);
   }
   // SMS mode :
   ComTxString( ErrorMode);
   if( !ComRxMatch( Ok, REPLY_TIMEOUT)){
      return( NO);
   }
   ComTxString( PduMode);
   if( !ComRxMatch( Ok, REPLY_TIMEOUT)){
      return( NO);
   }
   return( YES);
} // GsmReset

//******************************************************************************
// Registrace
//******************************************************************************

static const char Registered[] = "AT+CREG?\r";           // Registration
static const char RegHeader[]  = "\r\n+CREG: ";          // Header of registration


TYesNo GsmRegistered( void)
// Vraci YES, je-li modem registrovan v siti
{
byte Value;

   TxString( Registered);
   if( !ComRxMatch( RegHeader, REPLY_TIMEOUT)){
      return( NO);
   }
   if( !ComWaitChar( ',')){            // preskoc pole <n>
      return( NO);
   }
   Value = ComRxDec();                 // pole <stat>
   if( Value == 1 || Value == 5){
      return( YES);                    // registered or roaming
   }
   return( NO);
} // GsmRegistered

//******************************************************************************
// Operator
//******************************************************************************

static const char Operator[]   = "AT+COPS?\r";           // Get operator
static const char OperHeader[] = "\r\n+COPS: ";          // operator header

TYesNo GsmOperator( char *Name)
// Vrati retezec s nazvem operatora,
// POZOR delka <Name> musi byt min SMS_MAX_OPERATOR+1
{
   TxString( Operator);
   if( !ComRxMatch( OperHeader, REPLY_TIMEOUT)){
      return( NO);
   }
   if( !ComWaitChar( ',')){            // preskoc pole <mode>
      return( NO);
   }
   if( !ComWaitChar( ',')){            // preskoc pole <format>
      return( NO);
   }
   if( !ComRxString( Name, SMS_MAX_OPERATOR)){
      return( NO);
   }
   return( YES);
} // GsmOperator

//******************************************************************************
// Sila signalu
//******************************************************************************

static const char Rssi[]       = "AT+CSQ\r";             // Get Received Signal Strength
static const char RssiHeader[] = "\r\n+CSQ: ";           // Signal strength header

native GsmSignalStrength( void)
// Vrati relativni silu signalu 0..31 nebo 99 nejde-li zjistit
{
byte Value;

   TxString( Rssi);
   if( !ComRxMatch( RssiHeader, REPLY_TIMEOUT)){
      return( 99);
   }
   Value = ComRxDec();                 // relativni sila
   return( Value);
} // GsmSignalStrength
