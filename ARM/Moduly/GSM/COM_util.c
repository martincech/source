//******************************************************************************
//
//   COM_util.c   Serial communication utility
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include "com_util.h"
#include "../../inc/System.h"
#include "../../inc/Bcd.h"
#include <string.h>

#if defined( GSM_COM0)
   #define __COM0__
#elif defined( GSM_COM1)   
   #define __COM1__
#else
   #error "Unknown COM number"
#endif   
#include "../../inc/Com.h"

//-----------------------------------------------------------------------------
// Tx Digit
//-----------------------------------------------------------------------------

void ComTxDigit( byte n)
// vyslani cislice 0..9
{
   ComTxChar( n + '0');
} // ComTxDigit

//-----------------------------------------------------------------------------
// Tx string
//-----------------------------------------------------------------------------

void ComTxString( const char *Text)
// Vysle retezec
{
native Length;
native i;

   Length = strlen( Text);
   for( i = 0; i < Length; i++){
      WatchDog();
      ComTxChar( *Text);
      Text++;
   }
   while( ComTxBusy());      // cekej na vyslani
} // ComTxString

//-----------------------------------------------------------------------------
// Secure Tx string
//-----------------------------------------------------------------------------

void ComSecureTxString( const char *Text)
// Flush Rx a vysle retezec
{
   ComFlushChars();
   ComTxString( Text);
} // ComTxString

//-----------------------------------------------------------------------------
// Tx hexa
//-----------------------------------------------------------------------------

void ComTxHex( byte Number)
// Vysle hexadecimalni reprezentaci bytu
{
   ComTxChar( nibble2hex( hnibble( Number)));
   ComTxChar( nibble2hex( lnibble( Number)));
} // ComTxHex

//-----------------------------------------------------------------------------
// Tx dekadicky
//-----------------------------------------------------------------------------

void ComTxDec( byte Number)
// Vysle dekadickou reprezentaci bytu
{
word w;

   w = bbin2bcd( Number);
   ComTxDigit( w >> 8);                // stovky
   ComTxDigit( hnibble( w));           // desitky
   ComTxDigit( lnibble( w));           // jednotky
} // ComTxDec

//-----------------------------------------------------------------------------
// Rx dekadicky
//-----------------------------------------------------------------------------

byte ComRxDec( void)
// Cte cislo po prvni nenumericky znak
{
byte Value;
char ch;

   Value = 0;
   while(1){
      if( !ComRxChar( &ch)){
         return( 0);    // timeout
      }
      if( ch < '0' || ch > '9'){
         return( Value);
      }
      Value *= 10;
      Value += char2dec( ch);
   }
} // ComRxDec

//-----------------------------------------------------------------------------
// Rx hexadecimalne
//-----------------------------------------------------------------------------

byte ComRxHex( void)
// Cte dva znaky, jako hexa reprezentaci
{
byte Value;
char ch;

   if( !ComRxChar( &ch)){
      return( 0);                      // timeout
   }
   Value  = char2hex( ch) << 4;        // MSB
   if( !ComRxChar( &ch)){
      return( 0);                      // timeout
   }
   Value |= char2hex( ch);             // LSB
   return( Value);
} // ComRxHex

//-----------------------------------------------------------------------------
// Rx wait char
//-----------------------------------------------------------------------------

TYesNo ComWaitChar( const char ch)
// Ceka na znak <ch>
{
char x;

   while(1){
      WatchDog();
      if( !ComRxChar( &x)){
         return( NO);              // timeout
      }
      if( ch == x){
         return( YES);             // prijat
      }
   }
} // ComWaitChar

//-----------------------------------------------------------------------------
// Rx skip chars
//-----------------------------------------------------------------------------

void ComSkipChars( native Count)
// Preskoci pocet znaku <Count>
{
char x;

   do {
      WatchDog();
      if( !ComRxChar( &x)){
         return;                       // timeout
      }
   } while( --Count);
} // ComSkipChars

//-----------------------------------------------------------------------------
// Rx match
//-----------------------------------------------------------------------------

TYesNo ComRxMatch( const char *Text, native Timeout)
// Cte ocekavany retezec, vraci YES souhlasi-li
{
native Length;
native i;
const  char *p;
char   ch;

   Length = strlen( Text);
   p      = Text;
   // cekani na prvni znak :
   if( Timeout){
      if( !ComRxWait( Timeout)){
         return( NO);
      }
   } // else bez cekani na prvni znak
   // cteni zpravy :
   for( i = 0; i < Length; i++){
      WatchDog();
      if( !ComRxChar( &ch)){
         return( NO);    // timeout
      }
      if( ch != *p){
         return( NO);
      }
      p++;
   }
   return( YES);
} // ComRxMatch

//******************************************************************************
// Rx string
//******************************************************************************

TYesNo ComRxString( char *Text, native Length)
// Cte retezec v uvozovkach
{
native i;
char   ch;

   // uvodni uvozovka :
   if( !ComWaitChar( '"')){
      return( NO);
   }
   for( i = 0; i < Length; i++){
      if( !ComRxChar( &ch)){
         return( NO);    // timeout
      }
      if( ch == '"'){
         Text[ i] = '\0';
         return( YES);
      }
      Text[ i] = ch;
   }
   Text[ Length] = '\0';
   return( YES);
} // ComRxString
