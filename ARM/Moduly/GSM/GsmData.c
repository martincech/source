//******************************************************************************
//
//   GsmData.c    GSM data modem services
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include "../../inc/Gsm.h"
#include "../../inc/System.h"   // SysDelay
#include "COM_util.h"

#if defined( GSM_COM0)
   #define __COM0__
#elif defined( GSM_COM1)   
   #define __COM1__
#else
   #error "Unknown COM number"
#endif   
#include "../../inc/Com.h"

// Preddefinovane retezce :

static const char Ring[]       = "\r\nRING\r\n";         // incomming call
static const char OffHook[]    = "ATA\r";                // zvednuti
static const char HangUp[]     = "ATH\r";                // zaveseni
static const char CommandEsc[] = "+++";                  // escape sekvence pro prikazovy mod
//static const char NoCarrier[]  = "\r\nNO CARRIER\r\n";   // preruseni spojeni
static const char Connect[]    = "\r\nCONNECT";          // navazani spojeni


#define TxString( s) ComSecureTxString( s)    // pred vyslanim zrus Rx

//-----------------------------------------------------------------------------
// Zvoneni
//-----------------------------------------------------------------------------

TYesNo GsmIsRinging( void)
// Kontrola prichoziho hovoru
{
char ch;
native Index;

   Index = 0;
   while( ComRxChar( &ch)){
      if( ch != Ring[ Index]){
         Index = 0;                    // znovu od zacatku retezce
         // zopakuj test pro prvni znak :
         if( ch != Ring[ Index]){
            continue;                  // ani prvni nesouhlasi
         }
      }
      Index++;
      if( Index < sizeof( Ring) - 1){
         continue;                     // cekej dalsi znak
      }
      return( YES);                    // uspesny prijem
   }
   return( NO);                        // zadny znak
} // GsmIsRinging

//-----------------------------------------------------------------------------
// Zvednuti
//-----------------------------------------------------------------------------

void GsmOffHook( void)
// Zvedne telefon
{
   TxString( OffHook);                 // zvedni
} // GsmOffHook

//-----------------------------------------------------------------------------
// Navazani spojeni
//-----------------------------------------------------------------------------

TYesNo GsmConnect( void)
// Kontrola navazani spojeni
{
   if( !ComRxMatch( Connect, 0)){
      return( NO);                     // nepodarilo se navazat
   }
   ComWaitChar( '\n');                 // preskoc zbytek zpravy Connect
   return( YES);
} // GsmConnect

//-----------------------------------------------------------------------------
// Command mode
//-----------------------------------------------------------------------------

void GsmCommandMode( void)
// Prepne na prikazovy rezim (vysle ESC)
{
   TxString( CommandEsc);              // ESC
} // GsmCommandMode

//-----------------------------------------------------------------------------
// Zaveseni
//-----------------------------------------------------------------------------

void GsmHangUp( void)
// Vysle prikaz zaveseni
{
   TxString( HangUp);                  // zaves
} // GsmHangUp
