//*****************************************************************************
//
//    Bcd.c - BCD utility
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "../../inc/Bcd.h"

//-----------------------------------------------------------------------------
// Get/Set digit
//-----------------------------------------------------------------------------

byte BcdGetDigit( int Value, int Order)
// returns BCD digit from <Order>
{
   Order <<= 2;         // Order * 4
   return((Value >> Order) & 0x0F);
} // BcdGetDigit

int BcdSetDigit( int Value, int Order, byte Digit)
// set BCD <Digit> at <Order>
{
   Order <<= 2;         // Order * 4
   return(( Value & ~(0x0F << Order)) | (Digit << Order));
} // BcdSetDigit

//-----------------------------------------------------------------------------
// Width binary
//-----------------------------------------------------------------------------

int BcdGetWidth( int Number)
// Returns order of the binary <Number>
{
   if( Number < 10){
      return( 1);
   }
   if( Number < 100){
      return( 2);
   }
   if( Number < 1000){
      return( 3);
   }
   if( Number < 10000){
      return( 4);
   }
   if( Number < 100000){
      return( 5);
   }
   if( Number < 1000000){
      return( 6);
   }
   if( Number < 10000000){
      return( 7);
   }
   return( 8);
} // BcdGetWidth

//-----------------------------------------------------------------------------
// Width hexadecimal
//-----------------------------------------------------------------------------

int BcdGetXWidth( int Number)
// Returns order of the hexadecimal <Number>
{
unsigned Mask;
int      i;

   Mask   = 0xF0000000;
   for( i = 8; i > 0; i--){
      if( Mask & Number){    // nonzero position
         return( i);
         break;
      }
      Mask >>= 4;      
   }
   return( 1);               // zero only
} // BcdGetXWidth

//-----------------------------------------------------------------------------
// Binary->BCD
//-----------------------------------------------------------------------------

dword dbin2bcd( dword x)
// converts 0..99 999 999 to bcd
{
int    i;
dword  y;

   y = 0;
   for( i = 0; i < 8; i++){
      y >>= 4;
      y |= (x % 10) << 28;
      x /= 10;
   }
   return( y);
} // dbin2bcd

//-----------------------------------------------------------------------------
// BCD->Binary
//-----------------------------------------------------------------------------

dword dbcd2bin( dword x)
// converts 0..99 999 999 to bin
{
int    i;
dword  y;

   y = 0;
   for( i = 0; i < 8; i++){
      y *= 10;
      y += (x & 0xF0000000) >> 28;
      x <<= 4;
   }
   return( y);
} // dbcd2bin

//-----------------------------------------------------------------------------
// To char
//-----------------------------------------------------------------------------

byte nibble2dec( byte x)
// converts low nibble to '0'..'9'
{
   return( ((x) & 0x0F) + '0');
} // nibble2dec

//-----------------------------------------------------------------------------
// To x char
//-----------------------------------------------------------------------------

byte nibble2hex( byte x)
// converts low nibble to '0'..'F'
{
   x &= 0x0F;
   if( (x) < 10){
      return( x + '0');
   } else {
      return( x - 10 + 'A');
   }
} // nibble2hex

//-----------------------------------------------------------------------------
// To digit
//-----------------------------------------------------------------------------

byte char2dec( byte ch)
// converts ASCII '0'..'9' to digit
{
   return( ch - '0');
} // char2dec

//-----------------------------------------------------------------------------
// To x digit
//-----------------------------------------------------------------------------

byte char2hex( byte ch)
// converts ASCII '0'..'9' and 'A'..'F' to digit
{
   return( (ch) <= '9' ? (ch) - '0' : (ch) - 'A' + 10);
} // char2hex
