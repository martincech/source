//******************************************************************************
//
//   TestDef.h    Test SW definitions
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __TestDef_H__
#define __TestDef_H__

// Test commands :
typedef enum {
   TEST_ADC            = 'A',                 // test external ADC
   TEST_CHARGER_ON     = 'C',                 // switch charger on
   TEST_DISPLAY        = 'D',                 // run display test
   TEST_OFF            = 'F',                 // switch power off
   TEST_IADC           = 'I',                 // test internal ADC
   TEST_MEMORY         = 'M',                 // run memory test
   TEST_CHARGER_OFF    = 'O',                 // switch charger off
   TEST_POWER          = 'P',                 // test power conditions
   TEST_RTC            = 'R',                 // run RTC test
   TEST_SOUND          = 'S',                 // run Sound test
   TEST_USB            = 'U',                 // run USB test
   TEST_BACKLIGHT_ON   = 'B',                 // switch backlight off
   TEST_BACKLIGHT_OFF  = 'G',                 // switch backlight on
   TEST_KEYBOARD       = 'K',                 // test keyboard
} TTestCommand;

// Test reply :
#define TEST_OK  'Y'
#define TEST_ERR 'N'

// Test power reply :
// PPR   : L/H
// CHG   : L/H
// USBON : L/H
// ENNAB : L/H
// TEST_OK

#define TEST_POWER_REPLY_SIZE 5

// Test ADC reply :
// 8 hexa digits
// TEST_OK

#define TEST_HEX_REPLY_SIZE 9

// Test keyboard reply :
// Key ON : L/H
// Key K0 : L/H
// Key K1 : L/H
// Key K2 : L/H
// TEST_OK

#define TEST_KEYBOARD_REPLY_SIZE 5

#endif
