//*****************************************************************************
//
//    Ads123x.c  -  A/D convertor ADS1230/ADS1232 services
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#include "../Inc/Ads123xf.h"
#include "../Inc/Cpu.h"
#include "../Inc/System.h"    // SysDelay

#if defined( __ADS1232__)
   // Parametry dat prevodniku ADS 1232 :
   #define ADC_DATA_WIDTH         24
   #define ADC_CALIBRATION_CLOCK  26
#elif defined ( __ADS1230__)
   // Parametry dat prevodniku ADS 1230 :
   #define ADC_DATA_WIDTH         20
   #define ADC_CALIBRATION_CLOCK  26
#else
   #error "Unknown ADS123x convertor type"
#endif

static void __irq EintHandler( void);
// External interrupt handler

#define EINT_PORT       ADC_DRDY_PORT // EINT port number (P0.EINT_PORT)
#define EINT_FALL_EDGE  1             // EINT fall edge sensitivity

#include "../Moduly/LPC/Eint.c"       // include template

#if ADC_DRDY == ADC_DOUT                             // common DRDY/DOUT input
   #define EintAttachPin()     EintSelectPin()       // use EINT
   #define EintReleasePin()    EintDeselectPin()     // use EINT as I/O
   #ifdef ADC_PINSEL_INTERRUPT
      #define AdcDisableInts() DisableInts()
      #define AdcEnableInts()  EnableInts()
   #else
      #define AdcDisableInts()
      #define AdcEnableInts()
   #endif
#else // ADC_DRDY != ADC_DOUT separate DRDY/DOUT inputs
   #define EintAttachPin()
   #define EintReleasePin()
   #define AdcDisableInts()
   #define AdcEnableInts()
#endif

// Local functions :

static void AdcSetup( void);
// Nastaveni prevodu, autokalibrace

static int32 AdcReadValue( void);
// Precteni namerene hodnoty

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void AdcInit( void)
// Inicializace prevodniku
{
   AdcInitPorts();                     // port init
   EintSetup();                        // EINT init
   AdcClrSCLK();
   AdcClrPDWN();                       // power down
   SysUDelay( 100);                    // min 26us
   AdcSetPDWN();                       // power up
   AdcSetup();                         // reset/autocalibration
   FilterStop();                       // default filter status
} // AdcInit

//-----------------------------------------------------------------------------
// Kalibrace
//-----------------------------------------------------------------------------

static void AdcSetup( void)
// Nastaveni prevodu, autokalibrace
{
native i;

   AdcDisableInts();                   // PINSEL access
   EintReleasePin();                   // use DRDY/DOUT as standard I/O
   AdcEnableInts();
   // DRDY may be configured as EINT, use DOUT instead :
   while( !AdcGetDOUT());              // wait for DRDY = H
   while(  AdcGetDOUT());              // wait for DRDY = L - data ready
   i = ADC_CALIBRATION_CLOCK;
   do {
      AdcSetSCLK();
      SysUDelay( 1);
      AdcClrSCLK();
      SysUDelay( 1);
   } while( --i);
   AdcDisableInts();                   // PINSEL access
   EintAttachPin();                    // use DRDY/DOUT as EINT
   AdcEnableInts();
} // AdcSetup

//-----------------------------------------------------------------------------
// Cteni dat
//-----------------------------------------------------------------------------

static int32 AdcReadValue( void)
// Precteni namerene hodnoty
{
int32  Value;
native i;

   EintReleasePin();                   // use DRDY/DOUT as standard I/O
   // znamenkovy bit :
   AdcSetSCLK();
   SysUDelay( 1);
   if( AdcGetDOUT()){
      Value = -1;                      // rozsireni zaporneho znamenka
   } else {
      Value =  0;                      // kladne znamenko
   }
   AdcClrSCLK();
   SysUDelay( 1);
   // vyznamove bity :
   i = ADC_DATA_WIDTH - 1;
   do {
      AdcSetSCLK();
      SysUDelay( 1);
      Value <<= 1;
      if( AdcGetDOUT()){
         Value |= 1;
      }
      AdcClrSCLK();
      SysUDelay( 1);
   } while( --i);
   // nastavit DRDY do H :
   AdcSetSCLK();
   SysUDelay( 1);
   AdcClrSCLK();
   SysUDelay( 1);
   EintAttachPin();                    // use DRDY/DOUT as EINT
   return( Value);
} // AdcReadValue


//-----------------------------------------------------------------------------
// Start
//-----------------------------------------------------------------------------

void AdcStart( void)
// Zahajeni periodickeho mereni
{
   EintDisableIrq();
   AdcSetup();                         // inicializace
   FilterStart();                      // start filtering
   EintEnableIrq();                    // enable EINT
} // AdcStart

//-----------------------------------------------------------------------------
// Stop
//-----------------------------------------------------------------------------

void AdcStop( void)
// Zastaveni periodickeho mereni
{
   EintDisableIrq();
   FilterStop();
} // AdcStop

//-----------------------------------------------------------------------------
// Stop
//-----------------------------------------------------------------------------

void AdcRestart( void)
// Restart filtrace
{
   EintDisableIrq();
   FilterRestart();                    // restart filtering
   EintEnableIrq();                    // enable EINT
} // AdcRestart

//-----------------------------------------------------------------------------
// Cteni raw
//-----------------------------------------------------------------------------

TRawWeight AdcRawRead( void)
// Cteni okamzite hodnoty prevodu
{
TRawWeight Value;

   DisableInts();                     // lze volat i ve stavu Stop - EintDisable nelze
   Value = FilterRecord.RawWeight;    // okamzita hmotnost
   EnableInts();
   return( Value);
} // AdcRawRead

//-----------------------------------------------------------------------------
// Cteni low pass
//-----------------------------------------------------------------------------

TRawWeight AdcLowPassRead( void)
// Cteni filtrovane hodnoty prevodu
{
TRawWeight Value;

   DisableInts();                     // lze volat i ve stavu Stop - EintDisable nelze
   Value = FilterRecord.LowPass;      // dolni propust
   EnableInts();
   return( Value);
} // AdcRawRead

//-----------------------------------------------------------------------------
// Cteni
//-----------------------------------------------------------------------------

TYesNo AdcRead( TRawWeight *Weight)
// Cteni ustalene hodnoty
{
TYesNo Result;

   DisableInts();
   Result = FilterRead( Weight);
   EnableInts();
   return( Result);
} // AdcRead

//-----------------------------------------------------------------------------
// Prerusovaci rutina externiho vstupu
//-----------------------------------------------------------------------------

static void __irq EintHandler( void)
// Preruseni od externiho vstupu
{
TRawWeight  AdcValue;

   AdcValue  = AdcReadValue();         // precti prevod
   FilterNextSample( AdcValue);        // aktualizuj filtraci
   EintClearFlag();                    // zrus priznak nastaveny ctenim hodnoty
   CpuIrqDone();
} // EintHandler
