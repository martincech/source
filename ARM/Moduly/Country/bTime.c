//******************************************************************************
//                                                                            
//  bTime.c        Print time to buffer
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../../inc/bTime.h"
#include "../../inc/conio.h"         // putchar
#include "../../inc/xTime.h"         // date & time
#include "../../inc/sputchar.h"

//------------------------------------------------------------------------------
//  Time
//------------------------------------------------------------------------------

void bTime( char *Buffer, TTimestamp Now)
// Display time only
{
   sputcharbuffer( Buffer);
   xTime( sputchar, Now);   
} // DTime

//------------------------------------------------------------------------------
//  Date
//------------------------------------------------------------------------------

void bDate( char *Buffer, TTimestamp Now)
// Display date only
{
   sputcharbuffer( Buffer);
   xDate( sputchar, Now);
} // DDate

//------------------------------------------------------------------------------
//  Date & Time
//------------------------------------------------------------------------------

void bDateTime( char *Buffer, TTimestamp Now)
// Display date and time
{
   sputcharbuffer( Buffer);
   xDateTime( sputchar, Now);
} // DDateTime
